.PHONY: d3m server client clean

all: d3m server client

d3m:
	cd d3m/ && npm install && npm run build && cd -

server:
	cd server/ && npm install && npm run build && cd -

client:
	cd client/ && npm install && npm run build && cd -

clean:
	rm -rf d3m/node_modules && rm -f d3m/package-lock.json
	rm -rf server/node_modules && rm -f server/package-lock.json
	rm -rf client/node_modules && rm -f client/package-lock.json