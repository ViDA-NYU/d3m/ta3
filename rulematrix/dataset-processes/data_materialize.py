#!/bin/env python3
import os
import csv
import sys
import json

from datamart_materialize.d3m import D3mWriter
import shutil

def materialize(path_data_csv, path_d3m_destination, metadata_):
    jsonMetadata = json.loads(metadata_)
    datasetID = jsonMetadata['filename']
    metadata = jsonMetadata['metadata']

    writer = D3mWriter(
        dataset_id=datasetID,
        destination= path_d3m_destination,
        metadata=metadata,
        format_options=dict(
            # Can provide D3M version here, example:
            # version='3.2.0',
            # This will generate a d3mIndex if there isn't one
            need_d3mindex=True,
        ),
    )
    path = path_data_csv
    with open(path, 'rb') as source:
        with writer.open_file('wb') as dest:
            shutil.copyfileobj(source, dest)
    writer.finish()

if __name__ == "__main__":
    cargs = sys.argv[1:]
    path_current_data = cargs.pop(0) # we don"t want to pass this argument
    metadata_ = cargs.pop(0)
    args = cargs[:]
    config = None
    while args:
        arg = args.pop(0)
        print(arg)
        if arg == "--config":
            config = args.pop(0)
    if config is None:
        raise ValueError("must specify config file!")

    path_d3m_destination = cargs[1]
    materialize(path_current_data, path_d3m_destination, metadata_)
    print("executing: materialize.main({0})".format(cargs))
