#!/bin/env python3
import os
import csv
import sys
import json

from json import dumps
import datamart_profiler
from datamart_materialize.d3m import D3mWriter

def profiler(path_csv, path_directory, datasetID):
    metadata = datamart_profiler.process_dataset(path_csv, include_sample=True)
    pathJson = path_directory + '/' + datasetID + '.json'

    with open(pathJson, "w", encoding='utf-8') as fp:
        json.dump(metadata, fp, ensure_ascii=False, indent=4)

if __name__ == "__main__":
    cargs = sys.argv[1:]
    path_directory = cargs.pop(0) # we don"t want to pass this argument
    datasetID = cargs.pop(0)
    args = cargs[:]
    config = None
    while args:
        arg = args.pop(0)
        print(arg)
        if arg == "--config":
            config = args.pop(0)
    if config is None:
        raise ValueError("must specify config file!")

    path_csv = cargs[1]
    profiler(cargs[1], path_directory, datasetID)
    print("executing: profiler.main({0})".format(cargs))
