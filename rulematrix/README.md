# Rule Matrix

## Setup

First, install dependencies in the `requirements.txt` through pip:

```
pip install -r requirements.txt
```

Install the pyfim package, which is available at http://www.borgelt.net/pyfim.html

You can run the `install_pyfim.sh` to install the package.


## Running Scripts

First, export the `PYTHONPATH` to include the local rulematrix module

```
export PYTHONPATH=$PYTHONPATH:$HOME/workspace/ta3/rulematrix
```

### sample.py

Run sample.py to sample training data for the rule list. For example:

```
python rulematrix/sample.py --config test_problem.json
```

### extract_rules.py

After the targets of sampled data are filled (store in the `samplePath`), run the following scripts to extract a rule list:

```
python rulematrix/extract_rules.py --config test_problem2.json -v
```

## Helpers

There are plenty of useful functions that create json objects that can be used for front-end