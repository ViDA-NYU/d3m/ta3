# Visus

Visus is an interactive system for automatic machine learning model building and curation.

# Installation

### Datasets

To use the system, you need to first download D3M datasets avaiable at https://gitlab.datadrivendiscovery.org/d3m/datasets. Follow the instructions on that repository to clone the datasets using Git LFS.

Alternatively, you can download the datasets from https://datadrivendiscovery.org/data/seed_datasets_current/ using the following command:
```
wget -r -np -R "index.html*" -nH --header Authorization:$TOKEN https://datadrivendiscovery.org/data/seed_datasets_current/
```

where `$TOKEN` is available at: https://api-token.datadrivendiscovery.org/#!/login

### D3M Common Typings and Declarations (at /d3m)

This module contains shared code between both server and client modules. 

To install the dependencies and build this package, run:
```
npm install && npm run build
```

After this, the ``d3m/dist`` folder will contain importable shared typings and declarations common to client and server modules.
To use those: ``import * as d3m from 'd3m/dist'``.
If you change any files inside ``/d3m``, you must rebuild the dist files.
To automatically rebuild on active development, use:
 ```
npm run watch
```

### Web Client (at /)

This is a React app that runs on the browser.

Install client module dependencies:

```
npm install
```

Run in development mode:

```
npm start
```
- This enables watch and automatic rebuilds while you edit the source.
- If you need HTTP APIs served for your dev web view, you must run the dev server alongside this.

Build production files: 
```
npm run build
```
- This creates production files at `/build/`.

Local view of production files:
```
npm install -g serve
serve -s build
```

To run unit tests (which use Jest):

```
npm test
```


### Express Server (at server/)

This module is the backend server built using Express and Node.js.

Install npm dependencies:

```bash
npm install
```

To use RuleMatrix:
```bash
cd ../rulematrix/
# install dependencies in the `requirements.txt` through pip  (at rulematrix/):
pip install -r requirements.txt
# Install the pyfim package  (at rulematrix/):
bash install_pyfim.sh
# export the `PYTHONPATH` and `RULEMATRIX_ROOT` to include the local rulematrix module (at server/)
cd ../server/
export PYTHONPATH=$HOME/workspace/ta3/rulematrix
export RULEMATRIX_ROOT=$HOME/workspace/ta3/rulematrix
```
Build the server module (this creates the server files at `/server/dist/`):
```bash
npm run build
```

Create an ``.env`` file with the fields defined in ``server/.env.template``:
```bash
cp server/.env.template server/.env
# then, edit server/.env with necessities of your dev environment
```


Run the dev server (you must build the server before starting it):  
```
npm start
```


Alternatively, to automatically rebuild and run when the source code changes, use:
```bash
npm run watch
```

To execute the tests (Jest), use:

```
npm test
```


### Backend TA2 Server

The TA3 system depends on TA2 systems that implement the common [gRPC TA3TA2-API](https://gitlab.com/datadrivendiscovery/ta3ta2-api). To run a TA2 server you can either run a full-fledged TA2 system or run the TA2 stubs (mock API implementation) during development.

To run a full TA2 system, you can use the docker-compose file available at `docker-compose/docker-compose-ta2-dev.yml`:

```bash
# Start docker-compose with environment variables configured with your local machine paths
D3MOUTPUTDIR=/Users/user/workspace/d3m/D3MData/ \
D3MOUTPUTDIR=/Users/user/workspace/d3m/output/ \
docker-compose -f docker-compose-ta2-dev.yml up
```

To run the TA2 stubs for development, follow instructions on the [ta2ta3-stubs](https://gitlab.com/ViDA-NYU/d3m/ta2ta3-stubs) project.

### Build & Run Using Docker

Build the docker image:

```
docker build -t ta3 .
```

Make sure that the environment variable ``$D3MINPUTDIR``  points to a directory that contains the d3m dataset folders.
In order to do so, you can create a `.env` file, at the `./docker-compose/` directory, containing all the environment variables needed, e.g:
```
D3MINPUTDIR=/path/to/datasets/folder
```

Finally, start the docker containers using:

```
cd docker-compose
docker-compose up
```
The TA3 system will be available at [``http://localhost:80``](http://localhost:80).

Here is a sequence of commands that start the project's docker-compose from scratch:

```bash
# Clone and enter the app directory
git clone git@gitlab.com:ViDA-NYU/d3m/ta3.git ta3
cd ta3/

# Build the docker images named "ta3"
docker build . -t ta3

# Start docker containers. Make sure that the 
# environment variable $D3MINPUTDIR points to
# a folder that contains datasets in D3M format
# and that it is a directory accessible by Docker.
cd docker-compose
D3MINPUTDIR=../D3MData docker-compose up

# Finally, access TA3 at http://localhost.
```
