# Visus Change Log

## Version 2020.x.x

- Sort partial dependence plots by feature importance
- Extracted ta3ta2-api .proto files to a new package named `ta3ta2-api-ts`
  which includes all necessary TypesScript types compiled automatically using
  the `protoc` compiler with the plugins `ts-protoc-gen` and `protoc-gen-grpc`.
  The package is published at https://www.npmjs.com/package/ta3ta2-api-ts and
  its source code is hosted at https://gitlab.com/ViDA-NYU/d3m/ta3ta2-api-ts.
- Refactored the TA2TA3-API implementation to use the new `ta3ta2-api-ts` package,
  which improves maintainability and fixes issue longstanding issues with
  deserialization of protocol buffer messages (see issue #48).
- Updated React dependency to version 16.8.6
- Updated dependencies, including `react-hot-loader`, `styled-components`, and `jest`.
- Data ingestion
  - Ingesting data from CSV files.
  - Auto-profiling (`datamart-profiler 0.5.3`) and allow users to define/update/fix the features datatype.
  - Save the new data in a D3M format using `datamart-materialize 0.5.4`.
- Updated to use problem and dataset schemas v4.0.0
- Updated to use ta3ta2-api v2020.1.28
- Added time series forecasting explanations: forecasting timeline (line chart)
  and confusion scatterplot.
- Added a new method for image regression problem explanation. It shows an
  image gallery that contains a sample of predicted images. The predicted and
  ground-truth values are shown beside the images.
- Implemented data set splitting for time series forecasting problems
- Implemented data set splitting for image datasets
- Disable RuleMatrix and PDPs for non-tabular data
- Remove resource name from target selection step of problem definition stepper
  to make it more readable (issue #151)
- Updated to use ta3ta2-api v2020.2.11
- Disable data set splitting for object detection problems.
- Added a new method for object detection problem explanation: Drawing bounding boxes to check if a model is accurate or not.
  - Define a new data resource object based on the CSV file that contains the predicted values and extra information such as d3mIndex and confidence.
  - Get the intersection of multiple indexes between ground-truth and predicted values. For object detection problems, the d3mIndex attribute is a multiIndex data type as it can have non-unique values.
  - Get predicted bounding boxes for each image.
  - Scaling bounding boxes according to the scaled image's size.
  - Display bounding boxes over the images.
- Refactored ExplainSolutionsView component
- Implemented screen splitting to allow comparison of pipelines side-by-side (issue #156)
- Make time-series plots on Explain Solutions page responsive to screen width
- Fixed synchronization issues in data generation necessary for the partial
  dependence plots. This issue was leading TA2 failures due to mal-formed
  CSV files being generated (issue #165).
- Update datamart libs (`datamart-profiler 0.5.6` and `datamart-materialize 0.5.7`) to generate d3mIndex column and explicitly request sample from datamart-profiler.
- Display a message if the file is too large. The maximum file size is capped at 100MB.
- Display a message while profiling the loaded dataset.
- Handling errors while profiling data and saving the CSV file in the D3M format.
- Fixed issue: Partial dependence plots did not show up for regression problems after uploading a new CSV.
- Save uploaded datasets under `D3MOUTPUTDIR` directory
- Clean-up legacy code
- Display an alert message if the dataset has already been uploaded
- Disable the 'Save Dataset' button after clicked

## Version 2020.1.24

- Update configure search view
  - Remove 'configure search' step from the main workflow. The
    'configure search' step was added as one of the sub-steps required to
    define a problem.
  - Include the variables: maxTime, maxTimeUnit, and metrics as part of the
    problem definition.
  - Add two modes of configuring search: 'Default Configuration' and
    'Custom configuration' modes. Just the second one can be customized by the
    user.
  - Highlight the selected tab using a colored border-bottom.
  - Make actions reversible while defining the problem: The user should always
    be able to quickly go back step-by-step through changes that were made.
    Visus ensures that users never lose their work as a result of backtracking.
    In this way, users are not forced to repeat data they have previously
    entered.
  - Add a taxonomy of problems (types and sub-types) to filter available
    sub-types after selecting a problem type based on the D3M problem schema
    standard.
  - Minor fixed issues:
    - Incorrect target's label, the resource id was missing, when a user uses
      an existing setting to define a problem.
    - The height of the vertical line in the main workflow is too long.

- Disable "View Details" button for data augmentation search results
- Update Docker in GitLab CI script to version 19.03.1
- Split the dataset (training and testing data) using stratified sampling
  for classification problems
- Limit datasets shown to datasets from the problem configured via
  `D3MPROBLEMPATH` environment variable (when it is set)
- Disable "Maximum solutions" option for configure search.
- Update 'Select Dataset' and 'Select Task' views:
  - Use ReactMarkDown to convert text written in Markdown to HTML to display
    dataset description.
  - The whole dataset description is shown.
  - The CardShadow component, used in Select Dataset view to display dataset
    info, is not any more clickable, users must use the button 'Select' to go
    the next step.
  - The CardShadow component, used in Select Task view to display problem info,
    is not any more clickable, users must use the button 'Use this setting' to
    go the next step.
  - Change the background color of the entire CardShadow (div) on hover.
- Handle WebSocket disconnection errors (issue #131)
- Change evaluate solution API to use WebSocket instead of REST to avoid
  HTTP timeouts when pipeline training takes a long time (issue #101)
- Update data profiling view:
  - Add spreadsheet view. Retrieve multiple rows from the dataset specifying
    a start point (row index) in the dataset.
  - Add detail view. This view displays a spreadsheet with a small
    visualization in the column's header which summarizes the information of
    each column.
  - Add column view. It displays visualizations to summarize the information
    for each column. It also show some statistics for each data column such
    mean and total of distinct values.
- Update the version of react-feather package from v1.1.4 to v2.0.3.
- Update the version of grpc package from v1.17.0v to v1.24.0. It was done to
  avoid conflict after updating Node to v12.11.1
- Fix RuleMatrix issues:
  - Use the target defined by the user as target while creating ruleMatrix.
  - Support 'string' and 'boolean' datatypes while building RuleMatrix
  visualization. String and boolean datatypes are being handled as categorical
  datatypes, otherwise, the system crashes because of errors while the rule
  induction process performs.
- Remove DARPA and NYU logos
- Update usage logs.
  - Update usage logs related to the problem definition step.
  - Update usage logs related to the exploring data step.
  - Update usage logs related to model comparison and explanation steps.
  - Log GRPC requests and responses.
  - Refactoring logs.
- Avoid crashing Visus whether there are any errors while building partial
  dependence plots.
- Split datasets only once for SearchSolution call, and re-use the previously
  build dataset splits later for ScoreSolution, FitSolution and ProduceSolution
  calls.
- Refactor of dataset splitting code and fix minor bugs
- Add new environment variable to configure whether the system should use the
  scores returned from the SearchSolution call or allways compute score using
  the ScoreSolution API call.
- Change compute partial dependence plots API to use WebSocket instead of REST
  to avoid HTTP timeouts when prediction generation takes a long time.
- Fix bug that caused the system to trigger the model search before the data
  splits were completely writen to disk.
- Upgrade docker image and Gitlab CI to use Node 12 (LTS)
- Fix integration tests (closes issue #141)
- Fix bug that caused the system to fail to render all partial dependence plots
  because the input file necessary for generating the plots was not completely
  written to disk before asking TA2 to generate predictions for the input file.
- Fix problem in partial dependence plots that caused the confidence interval
  shaded area be rendered outside of the chart area
- Fix a bug in the server-side code that causes reloading the partial dependence
  plot page to fail

## Version 2019.7.19

- Updated rule matrix code to handle and do not break the system when the rule
  induction process throws an error because the training data is not a singular
  matrix. A matrix is singular when it is not in full rank (its determinant is
  equal to zero and does not have an inverse). It can happen when two or more
  columns/rows in the dataset are equal or have the same value (issue #107).
- Fix issue with vertical scrolling when a code block is included in the text
  with Markdown syntax (issue #105).
- Fixed issue where a stale rest/index.js file was being imported due to an
  implicit import. Using the explicit import fixes the issue.
- Fixed issue #106: categorical attributes histograms not being shown.
- Display only metrics supported for each task type and sub-type (issue #109)
- Support D3M environment variables `D3MINPUTDIR` and `D3MOUTPUTDIR` (issue #77)
- Set task sub-type to `NONE` when there is no sub-type. Setting it to
  `TASK_SUBTYPE_UNDEFINED` is considered invalid by TA2s.
- Bugfix: remove file URI protocol only if it exists in Rule Matrix
- Change layouts to use the new system name: Visus
- Corrected visualization title from "Partial Plots" to "Partial Dependence Plots"
- Refactor and fix race condition in solution's sequential ID generation
- Disable the filter of solutions with internal_score equal to 0
- Update TA2-TA3 API to version v2019.4.11. The following changes were made:
  - Fields in problem description deprecated in the previous version has now
    been removed.
  - Updated to new changes in the d3m v2019.4.4 core package release:
    - `random_seed` field was added to `Score` message.
    - `TaskType` enumeration have been updated and renumbered.
    - `PerformanceMetric` enumeration have been updated and renumbered.
    - `ProblemInput` now has `privileged_data` field.
    - `ProblemDescription` now has `other_names` field.
  - Changed `time_bound` name to `time_bound_search` in
    `SearchSolutionsRequest`. Added `time_bound_run` to
    `SearchSolutionsRequest` as place to specify anticipated limit on time
    to one pass of the pipeline run.
- Implemented integration tests using Cypress (issue #110)
- Add support for time series forecasting problems.
- Add support for text problems. The text is summarized as a keyword summary in which frequency is presented as a horizontal bar chart.
- Automatically build integration tests Docker image in CI server (issue #115)
- Add a wait script that waits until the Visus server is ready to receive
  requests. This script is used to run the tests on D3M TA2-TA3 CI server.
- Specify ScoringConfiguration when sending a ScoreSolutionRequest (issue #116)
- Implemented a new visual design and several improvements in layout
- Fix explain solution when confusion matrix and confusion scatterplot are created for time series, image and text problems (fixes issues #123, #124 and # 126).
- Fix error when splitting training and test datasets during search solution. Instead of retrieving training data to train the model, it was using the test dataset.
- Add home page.
- Add Select Dataset step as part of the workflow in Visus.
- Update Select Task View. Now it is using card components as content containers instead of lists.
- Update Select Dataset View. Now it is using card components as content containers.
- Include NYU, Darpa, and Visus logos to the new interface.
- Update names of environment variables which are used to start TA2 through docker-compose.
- Fix start search solutions when the API takes long to return (fixes issue #127)
- Update TA2-TA3 API to version v2019.6.11. The following changes were made:
 - Renamed type `SolutionDescription` to match the API type `PipelineDescription`
 - Added the optional `rank_solutions_limit` field to `SearchSolutionsRequest`
-  Update `typescript` to version ^3.4.5
-  Update `immer` to version ^3.1.1
-  Send generated pipelines to browser even before scoring and describe solutions are finished for faster response times and better user experience
-  Remove all uses of internal_score field
-  Disable preemptive training of the 5 top solutions
-  Rename solutionSearchStatus to solutionSearchState in DataStore.ts
-  Display handcrafted metric names instead of using startCase() function to format them
-  Handle errors during search solution and display to the user (issue #79)
-  Move translate.ts from server to the shared d3m module
-  Remove legacy docker-compose files and script to change dataset (issue #120)
-  Update docker-compose folder documentation
-  Correctly detect the main primitive name (issue #114)
- Implement 'Define Problem' view main navigation through a step-by-step layout. It allows navigation between the three required steps to define a problem (select: target, type, and subtype).
 - Disabling and enabling buttons while users complete each step of define problem step-by-step interface.
 - Create a problem only if the user completes all the steps required to define a problem.
 - Add textual messages to guide users to define a problem from scratch or edit an existing problem.
 - The data object that contains info about the target, type, and subtype of the problem, is now used to determinate whether a column is a target or not (in the profile data table view).
 - Handle problems with no subtype property such as linkPrediction, graphMatching, etc.
 - Add Datamart Augmentation tab, which allows data search and data augmentation
 - Add a recorder feature that allows recording all API responses for demonstration purposes
 - Setup D3MSTATICDIR environment variable to be able to test locally Object Detection problems
 - Update metrics:
  - Add metrics available for Vertex Classification problems (accuracy, F1 Micro, and F1 Macro)
  - Add metrics available for Semi Supervised Classification and Regression problems. Furthermore, a camel case error was fixed (from semiSupervisedClassification to semiSupervisedClassification, and from semiSupervisedRegression to semiSupervisedRegression)
- Handling error in Explain Solution view when training failed in TA2.
- Fix issue for RuleMatrix (empty column).
- Fix issue related to Explain Solution View: Show a message to let users know that Visus doesn't provide any visualization to explain Vertex Nomination, Vertex Classification and TimeSeriesForeCasting problem instead of crashing the system.
- Fix issue: The status of the defined problem which contains the target, type and subtype was incorrectly changed after clicking on search solution button. So, when the user comes back to Define Problem View, the defined problem was not shown correctly.
- Minor change related to image problems (display a sample of four images at the beginning).
- Configure Datamart base URL based on server environment variables
- Update TA2-TA3 API to version v2019.7.9.
- Add usage logs

## Version 2019.2.7

Stable TA3:
- Upgraded all npm vulnerable dependencies in both server and client modules
- Improved support for timeseries datasets
- Updates Docker image to use Node 11.
- Automatically builds and pushes docker images to the registry. It only pushes
  images for the branches master, devel, and any tag/branch with the pattern
  x.y.z (this will generate images for released tagged versions). Fixes #35.
- Rewrite of Dockerfile to avoid adding unnecessary files to the image and
  increasing its size. Total image size was decreased to 1.17GB.
- Improvement in configuration error messages

New TA3 Version:
- Implemented new TA3 layout navigation
- Implemented all new-layout react components using mock data
- Implemented data exploration for video, audio, image, text, timeseries resources
- Implemented a custom Bootstrap 4 theme for a more uniform color design
- Fixed missing dependencies in client/package.json file which caused build to
  fail when node_modules was not cached.
- Implemented error handling
- Improved column profiling plots with human-readable ticks
- Implemented SolutionTable that supports user pruning of solutions: favorite
  solutions, discard solutions
- Implemented solution score distribution with highlighting
- Implemented confusion matrix and confusion scatterplot over FitSolution and
  ProduceSolution API
- Implemented waveform display in audio viewer
- Changed solution score distribution to thumbnail plots in SolutionTable header
- Moved confusion matrix and confusion plots to Explain Solutions page
- Added evaluation metrics to solution table in Explore Solutions page
- Added an animated spinner component to indicate that the system is working on
  the background
- Updated rule matrix python code and moved rule matrix to Explain Solutions page
- Added user control of server solution evaluation cache
- Supported favorite solutions export in the ExplainSolutionsView
- Implemented Rank Up/Down buttons to reorder solutions.
- Implemented automatic discovery of datasets by traversing data directory
- Implemented metric parallel coordinates
