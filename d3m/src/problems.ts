// D3M problem schema
// https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/blob/shared/schemas/problemSchema.json

export interface Problem {
  inputs: Inputs;
  expectedOutputs: ExpectedOutputs;
  about: ProblemAbout;
  dataAugmentation?: DataAugmentation[];
}

export enum TaskKeyword {
  // task types
  classification = "classification",
  regression = "regression",
  clustering = "clustering",
  linkPrediction = "linkPrediction",
  vertexNomination = "vertexNomination",
  vertexClassification = "vertexClassification",
  communityDetection = "communityDetection",
  graphMatching = "graphMatching",
  forecasting = "forecasting",
  collaborativeFiltering = "collaborativeFiltering",
  objectDetection = "objectDetection",
  // task sub-types
  semiSupervised = "semiSupervised",
  binary = "binary",
  multiClass = "multiClass",
  multiLabel = "multiLabel",
  univariate = "univariate",
  multivariate = "multivariate",
  overlapping = "overlapping",
  nonOverlapping = "nonOverlapping",
  // data types
  tabular = "tabular",
  relational = "relational",
  image = "image",
  audio = "audio",
  video = "video",
  speech = "speech",
  text = "text",
  graph = "graph",
  multiGraph = "multiGraph",
  timeSeries = "timeSeries",
  grouped = "grouped",
  geospatial = "geospatial",
  remoteSensing = "remoteSensing",
  lupi = "lupi",
  missingMetadata = "missingMetadata",
}

export interface ProblemAbout {
  problemID: string;
  problemName: string;
  problemDescription?: string;
  problemURI?: string;
  taskKeywords: TaskKeyword[];
  problemVersion: string;
  problemSchemaVersion: string;
}

export interface Inputs {
  data: InputsData[];
  dataSplits?: DataSplits;
  performanceMetrics: Metrics[];
}

export interface InputsData {
  datasetID: string;
  targets: InputsTarget[];
  forecastingHorizon?: ForecastingHorizon;
}

export interface Feature  {
  resID: string;
  colIndex: number;
  colName: string;
}

export interface InputsTarget extends Feature {
  targetIndex: number;
  resID: string;
  colIndex: number;
  colName: string;
  numClusters?: number;
}

export interface ForecastingHorizon {
  resID: string;
  colIndex: number;
  colName: string;
  horizonValue: number;
}

export enum SplitMethod {
  holdOut = 'holdOut',
  kFold = 'kFold',
}

export interface DataSplits {
  method: SplitMethod;
  testSize?: number;
  numFolds: number;
  stratified: boolean;
  numRepeats: number;
  randomSeed?: number;
  splitsFile: string;
}

export interface Metrics {
  metric: MetricValue;
  applicabilityToTarget?: ApplicabilityToTarget;
  K?: number;
  posLabel?: string;
}

export enum ApplicabilityToTarget {
  singleTarget = 'singleTarget',
  allTargets = 'allTargets',
}

export enum MetricValue {
  accuracy = 'accuracy',
  precision = 'precision',
  recall = 'recall',
  f1 = 'f1',
  f1Micro = 'f1Micro',
  f1Macro = 'f1Macro',
  rocAuc = 'rocAuc',
  rocAucMacro = 'rocAucMacro',
  rocAucMicro = 'rocAucMicro',
  meanSquaredError = 'meanSquaredError',
  rootMeanSquaredError = 'rootMeanSquaredError',
  meanAbsoluteError = 'meanAbsoluteError',
  rSquared = 'rSquared',
  normalizedMutualInformation = 'normalizedMutualInformation',
  jaccardSimilarityScore = 'jaccardSimilarityScore',
  precisionAtTopK = 'precisionAtTopK',
  objectDetectionAP = 'objectDetectionAP',
  hammingLoss = 'hammingLoss',
  averageMeanReciprocalRank = 'averageMeanReciprocalRank',
}

export interface ExpectedOutputs {
  predictionsFile: string;
  scoresFile?: string;
}

export interface DataAugmentation {
  domain?: string[];
  keywords?: string[];
}