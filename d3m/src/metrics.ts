import * as problems from './problems';
import { TaskKeyword, MetricValue } from './problems';

export enum PerformanceMetric {
  METRIC_UNDEFINED = 0,
  ACCURACY = 1,
  PRECISION = 2,
  RECALL = 3,
  F1 = 4,
  F1_MICRO = 5,
  F1_MACRO = 6,
  ROC_AUC = 7,
  ROC_AUC_MICRO = 8,
  ROC_AUC_MACRO = 9,
  MEAN_SQUARED_ERROR = 10,
  ROOT_MEAN_SQUARED_ERROR = 11,
  MEAN_ABSOLUTE_ERROR = 12,
  R_SQUARED = 13,
  NORMALIZED_MUTUAL_INFORMATION = 14,
  JACCARD_SIMILARITY_SCORE = 15,
  PRECISION_AT_TOP_K = 16,
  OBJECT_DETECTION_AVERAGE_PRECISION = 17,
  HAMMING_LOSS = 18,
  RANK = 99,
  LOSS = 100,
}

export const PerformanceMetricEnumTranslator = {
  [MetricValue.accuracy.toString()]: PerformanceMetric.ACCURACY,
  [MetricValue.f1.toString()]: PerformanceMetric.F1,
  [MetricValue.f1Macro.toString()]: PerformanceMetric.F1_MACRO,
  [MetricValue.f1Micro.toString()]: PerformanceMetric.F1_MICRO,
  [MetricValue.jaccardSimilarityScore.toString()]: PerformanceMetric.JACCARD_SIMILARITY_SCORE,
  [MetricValue.meanAbsoluteError.toString()]: PerformanceMetric.MEAN_ABSOLUTE_ERROR,
  [MetricValue.meanSquaredError.toString()]: PerformanceMetric.MEAN_SQUARED_ERROR,
  [MetricValue.normalizedMutualInformation.toString()]: PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION,
  [MetricValue.objectDetectionAP.toString()]: PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION,
  [MetricValue.precision.toString()]: PerformanceMetric.PRECISION,
  [MetricValue.precisionAtTopK.toString()]: PerformanceMetric.PRECISION_AT_TOP_K,
  [MetricValue.rSquared.toString()]: PerformanceMetric.R_SQUARED,
  [MetricValue.recall.toString()]: PerformanceMetric.RECALL,
  [MetricValue.rocAuc.toString()]: PerformanceMetric.ROC_AUC,
  [MetricValue.rocAucMacro.toString()]: PerformanceMetric.ROC_AUC_MACRO,
  [MetricValue.rocAucMicro.toString()]: PerformanceMetric.ROC_AUC_MICRO,
  [MetricValue.rootMeanSquaredError.toString()]: PerformanceMetric.ROOT_MEAN_SQUARED_ERROR,
  [MetricValue.hammingLoss.toString()]: PerformanceMetric.HAMMING_LOSS,
};

export const metricNamesMapping: { [key: number]: string } = {
  [PerformanceMetric.METRIC_UNDEFINED]: 'Undefined',
  [PerformanceMetric.ACCURACY]: 'Accuracy',
  [PerformanceMetric.PRECISION]: 'Precision',
  [PerformanceMetric.RECALL]: 'Recall',
  [PerformanceMetric.F1]: 'F1',
  [PerformanceMetric.F1_MACRO]: 'F1 Macro',
  [PerformanceMetric.F1_MICRO]: 'F1 Micro',
  [PerformanceMetric.ROC_AUC]: 'ROC AUC',
  [PerformanceMetric.ROC_AUC_MACRO]: 'ROC AUC Macro',
  [PerformanceMetric.ROC_AUC_MICRO]: 'ROC AUC Micro',
  [PerformanceMetric.MEAN_SQUARED_ERROR]: 'Mean Squared Error',
  [PerformanceMetric.ROOT_MEAN_SQUARED_ERROR]: 'Root Mean Squared Error',
  [PerformanceMetric.MEAN_ABSOLUTE_ERROR]: 'Mean Absolute Error',
  [PerformanceMetric.R_SQUARED]: 'R Squared',
  [PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION]: 'Normalized Mutual Information',
  [PerformanceMetric.JACCARD_SIMILARITY_SCORE]: 'Jaccard Similarity Score',
  [PerformanceMetric.PRECISION_AT_TOP_K]: 'Precision at top K',
  [PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION]: 'Object Detection Average Precision',
  [PerformanceMetric.HAMMING_LOSS]: 'Hamming Loss',
  [PerformanceMetric.RANK]: 'Rank',
  [PerformanceMetric.LOSS]: 'Loss',
};

export function metricName(metricValue: problems.MetricValue): string {
  return ta2MetricName(PerformanceMetricEnumTranslator[metricValue]);
}

export function toMetricValueAsString(metric: problems.MetricValue): string {
  const ta2Metric = PerformanceMetricEnumTranslator[metric];
  const metricAsString: string = PerformanceMetric[ta2Metric];
  return metricAsString;
}

export function ta2MetricName(ta2Metric: PerformanceMetric): string {
  return metricNamesMapping[ta2Metric];
}

/**
 * List of metrics that should be regarged as error metrics,
 * i.e., the smaller the score value, the better the score.
 */
const errorMetrics = [
  PerformanceMetric.MEAN_SQUARED_ERROR,
  PerformanceMetric.ROOT_MEAN_SQUARED_ERROR,
  PerformanceMetric.MEAN_ABSOLUTE_ERROR,
  PerformanceMetric.HAMMING_LOSS,
];

export const isErrorMetric = (metric: PerformanceMetric) => {
  return errorMetrics.findIndex((m) => m === metric) >= 0;
}

/**
 * Checks if the lowerbound of the metric is zero.
 * An error metric, and most of the scores have zero lowerbounds.
 * The only exception right now is R-Squared.
 */
export const isZeroMinMetric = (metric: PerformanceMetric) => {
  return metric !== PerformanceMetric.R_SQUARED;
};

/**
 * Checks if a metric has a value between zero and one.
 */
const zeroOneMetrics = [
  PerformanceMetric.ACCURACY,
  PerformanceMetric.PRECISION,
  PerformanceMetric.RECALL,
  PerformanceMetric.F1,
  PerformanceMetric.F1_MACRO,
  PerformanceMetric.F1_MICRO,
  PerformanceMetric.ROC_AUC,
  PerformanceMetric.ROC_AUC_MACRO,
  PerformanceMetric.ROC_AUC_MICRO,
  PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION,
  PerformanceMetric.JACCARD_SIMILARITY_SCORE,
  PerformanceMetric.PRECISION_AT_TOP_K,
  PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION,
]

export const isZeroOneMetric = (metric: PerformanceMetric) => {
  return zeroOneMetrics.findIndex(m => m == metric) >= 0;
};

const ACC = { metric: MetricValue.accuracy, label: 'Accuracy' };
const PREC = { metric: MetricValue.precision, label: 'Precision' };
const REC = { metric: MetricValue.recall, label: 'Recall' };
const F1 = { metric: MetricValue.f1, label: 'F1' };
const F1_MICRO = { metric: MetricValue.f1Micro, label: 'Micro F1' };
const F1_MACRO = { metric: MetricValue.f1Macro, label: 'Macro F1' };
const MSE = { metric: MetricValue.meanSquaredError, label: 'Mean Squared Error' };
const RMSE = { metric: MetricValue.rootMeanSquaredError, label: 'Root Mean Squared Error' };
const MAE = { metric: MetricValue.meanAbsoluteError, label: 'Mean Absolute Error' };
const R2 = { metric: MetricValue.rSquared, label: 'R-Squared' };
const JACCARD = { metric: MetricValue.jaccardSimilarityScore, label: 'Jaccard Similarity' };
const NMI = { metric: MetricValue.normalizedMutualInformation, label: 'Normalized Mutual Information' };
const OBJ_DET_AP = { metric: MetricValue.objectDetectionAP, label: 'Average Precision' };

// TODO: Identify which problem types use HammingLoss
// const HAMMING = { metric: MetricValue.hammingLoss, label: 'Hamming Loss' };

// ROC metrics are not yet supported
// const ROC_AUC = { metric: d3m.MetricValue.rocAuc, label: 'ROC AUC' };
// const ROC_AUC_MICRO = { metric: d3m.MetricValue.rocAucMicro, label: 'ROC AUC Micro' };
// const ROC_AUC_MACRO = { metric: d3m.MetricValue.rocAucMacro, label: 'ROC AUC Macro' };

export const getMetricsForTask = (taskType?: TaskKeyword, taskSubType?: TaskKeyword) => {
  if (taskType === TaskKeyword.classification) {
    const commonClassificationMetrics = [ACC];
    if (taskSubType === TaskKeyword.binary) {
      // TODO: add ROC_AUC
      return [
        ...commonClassificationMetrics,
        PREC,
        REC,
        F1,
      ];
    } else if (
      taskSubType === TaskKeyword.multiClass ||
      taskSubType === TaskKeyword.multiLabel) {
      return [
        ...commonClassificationMetrics,
        F1_MICRO,
        F1_MACRO,
        // TODO: Add Hamming Loss when it becomes supported
        // HAMMING,
        // TODO: Add ROC_AUC_* when they become supported
        // ROC_AUC_MICRO,
        // ROC_AUC_MACRO,
      ];
    }
    return commonClassificationMetrics;
  } else if (taskType === TaskKeyword.regression) {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else if (taskType === TaskKeyword.linkPrediction) {
    return [
      ACC,
      JACCARD,
    ];
  } else if (taskType === TaskKeyword.vertexNomination) {
    // TODO: return Mean Reciprocal Rank when it becomes available in TA3TA2 API
    // return [
    //   MRR,
    // ];
    return [];
  } else if (taskType === TaskKeyword.vertexClassification) {
    return [
      ACC,
      F1_MICRO,
      F1_MACRO,
    ];
  }
  else if (taskType === TaskKeyword.communityDetection) {
    return [
      NMI,
    ];
  } else if (taskType === TaskKeyword.graphMatching) {
    return [
      ACC,
      JACCARD,
    ];
  } else if (taskType === TaskKeyword.forecasting) {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else if (taskType === TaskKeyword.collaborativeFiltering) {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else if (taskType === TaskKeyword.objectDetection) {
    return [
      OBJ_DET_AP,
    ];
  } else {
    console.warn(`No metrics available for the requested task type
      (${taskType}) and sub-type(${taskSubType}).`);
    return [];
  }
}
