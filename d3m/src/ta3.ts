import { Problem } from './problems';
import { PartialPlotData } from './visualization';
import { RestRequestState } from './core';
/**
 * @fileoverview Provides socket message definitions for TA3-only communications between TA3 client and server.
 */

export enum Rpc {
  GET_SEARCH_RESULTS = 'ta3/getSearchResults',
  EVALUATE_SOLUTION = "ta3/evaluateSolution",
  COMPUTE_PARTIAL_DEPENDENCE_PLOTS = "ta3/computePartialDependencePlots",
  WRITE_NEW_DATASET = "ta3/writeNewDataset",
  WRITE_DATASET_D3MFORMAT = "ta3/writeDatasetD3MFormat",
}

export interface GetSearchResultsRequest {
  searchID: string;
  problem: Problem;
}

export interface EvaluateSolutionRequest {
  solutionID: string;
  problem: Problem;
}

export interface EvaluateSolutionResponse {
  fittedSolutionID: string;
  error: boolean;
}

export interface ComputePartialDependencePlotsRequest {
  fittedSolutionID: string;
  problem: Problem;
}

export interface ComputePartialDependencePlotsResponse {
  partialPlotsResults: PartialPlotData;
  error?: string;
}
export interface WriteNewDataRequest {
  dataID: string;
  data: string;
}
export interface WriteNewDataResponse {
  statusWriteNewData: RestRequestState;
  dataTypes: string;
  error?: string;
}
export interface WriteDataD3MFormatRequest {
  metadata: string;
}
export interface WriteDataD3MFormatResponse {
  statusSaveNewData: RestRequestState;
  error?: string;
}

// WebSocket definitions

export interface WSMessage {
  rid: string;
  fname: string;
  object: object;
}

export enum StatusCode {
  UNKNOWN = 0,
  OK = 1,
  CANCELLED = 2,
  SESSION_UNKNOWN = 3,
  SESSION_ENDED = 4,
  SESSION_EXPIRED = 5,
  INVALID_ARGUMENT = 6,
  RESOURCE_EXHAUSTED = 7,
  UNAVAILABLE = 8,
  FAILED_PRECONDITION = 9,
  OUT_OF_RANGE = 10,
  UNIMPLEMENTED = 11,
  INTERNAL = 12,
  ABORTED = 13,
}

export interface Status {
  code: StatusCode;
  details: string;
}

export interface Response {
  status: Status;
}
