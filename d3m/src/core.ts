import * as problems from './problems';
import { Column } from './datasets';

export interface ClientConfig {
  env_vars: {
    DATAMART_URL: string;
  }
}

export enum NumericType {
  integer = 'integer',
  real = 'real',
}

export enum TimeUnits {
  Minutes = 1,
  Hours = 60,
}

export enum ConfigurationType {
  DEFAULT = '1',
  CUSTOM = '2',
}

export interface SolutionSearchRequest {
  problem: problems.Problem;
  timeLimit: number;
  priority: number;
  maxSolutions?: number;
}

export interface SearchSolutionsResponse {
  search_id: string;
}

export type PrimitiveValue = string | number | Date;
export interface ColumnValueStat {
  name: PrimitiveValue;
  count: number;
}

export interface ColumnProfile {
  column: Column;
  values: ColumnValueStat[];
  isText: boolean;
  stats?: {
    min: number;
    max: number;
    mean: number;
    stdDev: number;
  };
}

export interface ResourceFileData {
  name: string;
  content: string;
  src: string;
}

export interface TimeSeriesData {
  name: string;
  minValue: number;
  maxValue: number;
  seriesKey: string;
  dataKey: string;
  data: Array<{ [anyKey: string]: number } | { [anyKey: number]: number }>;
}

export type DataForResource = ColumnProfile[] | ResourceFileData[];

export enum DescribeSolutionStatus {
  REQUESTED,
  ERRORED,
  COMPLETED,
}

export interface PipelineDescription {
  inputs: PipelineDescriptionInput[]
  outputs: PipelineDescriptionOutput[]
  steps: PipelineDescriptionStep[];
}

export interface PipelineDescriptionInput {
  name: string;
}

export interface PipelineDescriptionOutput {
  name: string;
  data: string;
}

export interface PipelineDescriptionStep {
  primitive: PrimitivePipelineDescriptionStep;
}

export interface PrimitivePipelineDescriptionStep {
  primitive: Primitive;
  arguments: {
    [aug: string]: SolutionDescriptionContainerArgument,
  };
  outputs: { id: string }[];
}

export interface SolutionDescriptionContainerArgument {
  argument: "container",
  container: {
    data: string
  }
}

export interface Primitive {
  id: string;
  name: string;
  python_path: string;
}

export interface DescribeSolutionProgress {
  status: DescribeSolutionStatus
  modelName?: string;
  pipeline?: PipelineDescription;
}

export interface SolutionScore {
  metric: string;
  score: number;
}

export enum ScoringStatus {
  REQUESTED,
  ERRORED,
  COMPLETED,
}

export interface SolutionScoreProgress {
  status: ScoringStatus;
  scores?: {
    [metric: string]: SolutionScore,
  };
}

export interface Solution {
  id: string;
  name: string;
  internal_score: number;
  scores?: SolutionScoreProgress;
  description?: DescribeSolutionProgress;
}

export interface SimplifiedProblem {
  searchParams: {
    maxSolution: number;
    maxTime: number;
    timeUnit: string;
    evaluationMetrics: { [metric: string]: boolean };
  };
}

export enum ProgressState {
  PROGRESS_UNKNOWN = 0,
  // The process has been scheduled but is pending execution.
  PENDING = 1,
  // The process is currently running. There can be multiple messages with this state
  // (while the process is running).
  RUNNING = 2,
  // The process completed and final results are available.
  COMPLETED = 3,
  // The process failed.
  ERRORED = 4,
}

export const ProgressStateNameMap = {
  0: 'PROGRESS_UNKNOWN',
  1: 'PENDING',
  2: 'RUNNING',
  3: 'COMPLETED',
  4: 'ERRORED',
};

export interface SolutionSearchProgress {
  state: string; // stringified version of ProgressState enum
  status: string; // TA2 custom status string
  percentage: number; // [0, 1] percentage based on ticks
}

/**
 * Formatted solution search update sent to TA3 client.
 */
export interface SolutionSearchUpdate {
  progress?: SolutionSearchProgress;
  solution?: Solution;
}

/**
 * Solution search status maintained locally in the TA3.
 */
export interface SolutionSearchState {
  searchID: string;
  params: SolutionSearchRequest;
  progress: SolutionSearchProgress;
  solutions: Solution[]; // maintained list of solutions on TA3 client
}

export interface UserProblemDefinition {
  target?: string;
  taskType?: problems.TaskKeyword;
  taskSubType?: problems.TaskKeyword;
  maxSolutions?: string;
  maxTime?: string;
  maxTimeUnit?: TimeUnits.Minutes;
  metrics?: problems.Metrics[];
}

/*
 * Mapping of TaskKeyword to actual task types.
 */
export const taskTypes = [
  problems.TaskKeyword.classification,
  problems.TaskKeyword.regression,
  problems.TaskKeyword.clustering,
  problems.TaskKeyword.linkPrediction,
  problems.TaskKeyword.vertexNomination,
  problems.TaskKeyword.vertexClassification,
  problems.TaskKeyword.communityDetection,
  problems.TaskKeyword.graphMatching,
  problems.TaskKeyword.forecasting,
  problems.TaskKeyword.collaborativeFiltering,
  problems.TaskKeyword.objectDetection,
]

export const taskSubtypes = [
  problems.TaskKeyword.semiSupervised,
  problems.TaskKeyword.binary,
  problems.TaskKeyword.multiClass,
  problems.TaskKeyword.multiLabel,
  problems.TaskKeyword.univariate,
  problems.TaskKeyword.multivariate,
  problems.TaskKeyword.overlapping,
  problems.TaskKeyword.nonOverlapping,
]

export function getTaskTypeFromKeywords(taskKeywords?: problems.TaskKeyword[]) {
  if (taskKeywords) {
    return taskTypes.find(t => taskKeywords.includes(t));
  } else {
    return undefined;
  }
}

export function getTaskSubtypeFromKeywords(taskKeywords?: problems.TaskKeyword[]) {
  if (taskKeywords) {
    return taskSubtypes.find(t => taskKeywords.includes(t));
  } else {
    return undefined;
  }
}

// Based on https://datadrivendiscovery.org/wiki/display/work/Taxonomy+of+problems and
// https://datadrivendiscovery.org/wiki/display/work/Matrix+of+metrics
export const problemType = {
  'classification': ['binary', 'multiClass', 'multiLabel'],
  'regression': ['univariate', 'multivariate'],
  'communityDetection': ['overlapping', 'nonOverlapping'],
  'clustering': [],
  'linkPrediction': [],
  'vertexNomination': [],
  'vertexClassification': ['binary', 'multiClass', 'multiLabel'],
  'graphMatching': [],
  'forecasting': [],
  'collaborativeFiltering': [],
  'objectDetection': [],
};

export enum RestRequestState {
  ERROR,
  SUCCESS,
}

export interface RestResponse {
  status: RestRequestState;
  message: string;
}
