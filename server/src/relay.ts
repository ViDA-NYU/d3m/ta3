import * as WebSocket from 'ws';
import * as d3m from 'd3m/dist';
import { Server } from 'http';
import { processWS } from './rest/index';

interface SocketError extends Error {
  code: string;
}

export let createRelayServer = (wspath: string, server: Server): WebSocket.Server => {
  // Websocket defs
  const wss = new WebSocket.Server(
    {
      server: server,
      path: wspath,
    },
  );

  // Websocket relay server
  wss.on('connection', (ws: WebSocket) => {

    ws.on('message', (strMessage: string) => {
      const message: d3m.ta3.WSMessage = JSON.parse(strMessage);
      // NOTE(bowen): When there are multiple types of resolvers, shall we define them in separate files and merge them?
      if (message.fname.startsWith('ta3/')) {
        console.log('Received WebSocket message:', message.fname);
        processWS(ws, message);
        return;
      }
    });

    ws.on('error', (err: SocketError) => {
      switch (err.code) {
        case 'ECONNRESET':
          console.log('TA3 resets socket connection');
          break;
        default:
          console.error('Unknown socket error');
      }
    });

    ws.on('close', () => {
      console.log('socket closed');
    });
  });
  return wss;
};
