import client from './client';
import * as d3m from 'd3m/dist';
import { flatten } from 'lodash';
import * as json2csv from 'json2csv';
import * as fs from 'fs';
import * as path from 'path';
import { mkDirByPathSync } from '../api/utils';
import { getDataSource, getDatasetSplitter, getProblemsDirectory } from '../data/config';
import { SeqIdCreator } from '../utils';
import { DEFAULT_TEST_SPLIT_PROPORTION } from '../data/DatasetSplitter';
import { USE_SCORES_FROM_SEARCH_SOLUTIONS } from '../env';
import {
  SearchSolutionsRequest,
  ScoreSolutionRequest,
  ScoringConfiguration,
  EvaluationMethod,
  FitSolutionResponse,
  ProduceSolutionResponse,
  GetSearchSolutionsResultsResponse,
  ProgressState,
  GetFitSolutionResultsResponse,
  GetProduceSolutionResultsResponse,
} from 'ta3ta2-api-ts/core_pb';
import { toProtobufProblemDescription, toCorePipelineDescription } from './translate';
import { Value, ValueType } from 'ta3ta2-api-ts/value_pb';
import { ProblemDescription, PerformanceMetric } from 'ta3ta2-api-ts/problem_pb';
import { PipelineDescription } from 'ta3ta2-api-ts/pipeline_pb';

const USER_AGENT = 'TA3-NYU';
const VERSION = '2020.2.11';

const userProblems: Array<{
  problem_id: string;
  system: string;
  meaningful: string;
}> = [];

export async function startSolutionSearch(
  params: d3m.SolutionSearchRequest,
): Promise<d3m.SearchSolutionsResponse> {

  const dataSource = getDataSource();

  const pd = toProtobufProblemDescription(params.problem);

  const ssr = new SearchSolutionsRequest();
  ssr.setUserAgent(USER_AGENT);
  ssr.setVersion(VERSION);
  ssr.setAllowedValueTypesList([ValueType.RAW]);
  ssr.setProblem(pd);
  ssr.setTimeBoundSearch(params.timeLimit);
  ssr.setPriority(params.priority);
  ssr.setTimeBoundRun(0);
  for (const input of pd.getInputsList()) {
    const dataset = dataSource.getDatasetById(input.getDatasetId());
    const datasetSplits = await getDatasetSplitter().getSplitsForDataset(dataset, pd);
    const value = new Value();
    value.setDatasetUri(datasetSplits.training_dataset_uri);
    ssr.addInputs(value);
  }

  saveProblem(params.problem, ssr.toObject());

  return {
    search_id: (await client.searchSolutions(ssr)).getSearchId(),
  };
}

export async function saveProblem(
  problem: d3m.Problem, searchSolutionCall: object,
): Promise<string> {

  const problemSchema = problem;
  const pathProblemSchema = getProblemsDirectory();
  const problemId = problemSchema.about.problemID;
  const meaningful = 'yes'; // req.body.meaningful; // it could be 'no' if the user did not select the problem as a meaningful problem.
  const system = 'user'; // req.body.system; // it could be 'auto' if the user did not select the problem as a meaningful problem.
  userProblems.push({
    problem_id: problemId,
    system: system,
    meaningful: meaningful,
  });
  const csv = json2csv.parse(userProblems);
  const csvOutput = path.join(pathProblemSchema, 'labels.csv');
  mkDirByPathSync(path.join(pathProblemSchema, problemId));
  if (fs.existsSync(csvOutput)) {
    fs.unlinkSync(csvOutput);
  }
  fs.writeFileSync(csvOutput, csv, { encoding: 'utf8', flag: 'w' });
  const schemaOutputFilename = path.join(pathProblemSchema, problemId, 'problem_schema.json');
  fs.writeFile(schemaOutputFilename, JSON.stringify(problemSchema), () => {});
  const callOutputFilename = path.join(pathProblemSchema, problemId,  'ss_api.json');
  fs.writeFile(callOutputFilename, JSON.stringify(searchSolutionCall), () => {});

  return 'Ok';
}

export async function describeSolution(solutionID: string) {
  return client.describeSolution(solutionID);
}

export function getSearchResults(
  searchID: string,
  problem: d3m.Problem,
  onUpdate: (update: d3m.SolutionSearchUpdate) => void,
) {
  const seqIdCreator = new SeqIdCreator();
  return client.getSearchSolutionsResults(
    searchID,
    async (data: GetSearchSolutionsResultsResponse) => {

      // Send progress update
      onUpdate({
        progress: {
          percentage: data.getDoneTicks() / (data.getAllTicks() || 1), // avoid NaN on divide-by-zero
          state: d3m.ProgressStateNameMap[data.getProgress().getState()],
          status: data.getProgress().getStatus(),
        },
      });

      // When there is an solution_id, we return it and trigger
      // the loading of the pipeline description and the scores
      if (data.getSolutionId()) {

        const name: string = seqIdCreator.getOrCreateSeqId(data.getSolutionId(), 3);

        const solution: d3m.Solution = {
          id: data.getSolutionId(),
          internal_score: data.getInternalScore(),
          name: name,
        };
        // Send a partial solution to browser so that it can be displayed immediately
        onUpdate({ solution });

        // Dispatch async loading of solution description (for pipeline steps graph)
        try {
          const description: d3m.DescribeSolutionProgress = await loadSolutionDescription(data.getSolutionId());
          onUpdate({
            solution: {
              ...solution,
              description,
            },
          });
        } catch {
          onUpdate({
            solution: {
              ...solution,
              description: {
                status: d3m.DescribeSolutionStatus.ERRORED,
              },
            },
          });
        }
        const problemDescription = toProtobufProblemDescription(problem);
        const scoreValues = tryToReadScoresFromSolution(data, problemDescription);
        if (USE_SCORES_FROM_SEARCH_SOLUTIONS && scoreValues) {
          onUpdate({
            solution: {
              ...solution,
              scores: {
                status: d3m.ScoringStatus.COMPLETED,
                scores: scoreValues,
              },
            },
          });
        } else {
          // Dispatch async computation of scores
          try {
            const scores: d3m.SolutionScoreProgress = await scoreSolution(data.getSolutionId(), problemDescription);
            onUpdate({
              solution: {
                ...solution,
                scores,
              },
            });
          } catch (error) {
            console.error('Failed to call ScoreSolutions: ', error);
            onUpdate({
              solution: {
                ...solution,
                scores: {
                  status: d3m.ScoringStatus.ERRORED,
                },
              },
            });
          }
        }
      }
    },
  );
}

/**
 * Tries to read the metrics requested in the problem description from the
 * solution search result. If all metrics requested were returned, then they
 * are returned, otherwise, no metric is returned.
 */
function tryToReadScoresFromSolution(
  raw: GetSearchSolutionsResultsResponse,
  problemDescription: ProblemDescription,
) {
  try {
    const scoreValues = flatten(
      raw.getScoresList().map((s) =>
        s.getScoresList().map(
          (d) => ({
            metric: d.getMetric().getMetric(),
            score: d.getValue().getRaw().getDouble(),
          }),
        ),
      ),
    );

    const scores: {
        [metric: string]: d3m.SolutionScore;
    } = {};
    const requestedMetrics = problemDescription
      .getProblem()
      .getPerformanceMetricsList()
      .map(m => m.getMetric());
    // FIXME: Double check if this logic is correct
    // const metricNames = requestedMetrics;
    console.log('requestedMetrics: ', requestedMetrics);
    requestedMetrics.forEach(requestedMetric => {
      console.log('Compared metric: ' + requestedMetric);
      scoreValues.forEach(it => console.log(' ? === ' + JSON.stringify(it)));
      const i = scoreValues.findIndex(v => v.metric === requestedMetric);
      if (i >= 0) {
        scores[requestedMetric] = {
          metric: Object.keys(PerformanceMetric).find(key => PerformanceMetric[key] === scoreValues[i]),
          score: scoreValues[i].score,
        };
      }
    });

    const receivedMetrics: number = Object.keys(scores).length;
    console.log(`Received ${receivedMetrics} out of ${requestedMetrics.length} ` +
      `requested metrics in search solution response.`);

      if (Object.keys(scores).length === requestedMetrics.length) {
      return scores;
    } else {
      return undefined;
    }
  } catch {
    return undefined;
  }
}

async function loadSolutionDescription(solutionId: string): Promise<d3m.DescribeSolutionProgress> {
  try {
    const solutionDetail = await client.describeSolution(solutionId);
    const pipeline = solutionDetail.getPipeline();
    const modelName = findMainPrimitiveName(pipeline);
    return {
      status: d3m.DescribeSolutionStatus.COMPLETED,
      modelName: modelName,
      pipeline: toCorePipelineDescription(pipeline),
    };
  } catch (e) {
    console.error(`Couldn't get details of solution ${solutionId}`, e);
    return {
      status: d3m.DescribeSolutionStatus.ERRORED,
    };
  }
}

function findMainPrimitiveName(pipeline: PipelineDescription) {
  const prefixes = [
    'd3m.primitives.classification',
    'd3m.primitives.regression',
    'd3m.primitives.object_detection',
    'd3m.primitives.graph_matching',
    'd3m.primitives.clustering',
    'd3m.primitives.time_series_forecasting',
    'd3m.primitives.time_series_classification',
    'd3m.primitives.vertex_nomination',
    'd3m.primitives.semisupervised_classification',
    'd3m.primitives.link_prediction',
  ];
  for (const step of pipeline.getStepsList()) {
    for (const prefix of prefixes) {
      console.log({
        step,
        prefix,
        python_path: step.getPrimitive().getPrimitive().getPythonPath(),
        name: step.getPrimitive().getPrimitive().getName(),
      });
      if (step.getPrimitive().getPrimitive().getPythonPath().startsWith(prefix)) {
        return step.getPrimitive().getPrimitive().getName().split('.').pop();
      }
    }
  }
  return undefined;
}

export async function scoreSolution(
  solutionID: string,
  problemDescription: ProblemDescription,
): Promise<d3m.SolutionScoreProgress> {

  const dataSource = getDataSource();
  const inputs: Value[] = await Promise.all(
    problemDescription.getInputsList().map(
      async (input): Promise<Value> => {
        const datasetSplits = await getDatasetSplitter().getSplitsForDataset(
          dataSource.getDatasetById(input.getDatasetId()),
          problemDescription,
        );
        const value = new Value();
        value.setDatasetUri(datasetSplits.testing_dataset_uri);
        return value;
      },
    ),
  );

  const scoringConfig = new ScoringConfiguration();
  scoringConfig.setMethod(EvaluationMethod.HOLDOUT);
  scoringConfig.setRandomSeed(1);
  scoringConfig.setTrainTestRatio(1.0 - DEFAULT_TEST_SPLIT_PROPORTION);
  scoringConfig.setShuffle(false);

  const scoresolutionRequest = new ScoreSolutionRequest();
  scoresolutionRequest.setSolutionId(solutionID);
  scoresolutionRequest.setInputsList(inputs);
  scoresolutionRequest.setPerformanceMetricsList(problemDescription.getProblem().getPerformanceMetricsList());
  scoresolutionRequest.setConfiguration(scoringConfig);

  try {
    const scoreSolutionResponse = await client.scoreSolution(scoresolutionRequest);
    return await getScoreSolutionResults(solutionID, scoreSolutionResponse.getRequestId());
  } catch {
    return {
      status: d3m.ScoringStatus.ERRORED,
    };
  }
}

export async function getScoreSolutionResults(solutionId: string, requestId: string) {
  return new Promise<d3m.SolutionScoreProgress>(resolve => {

    const returnedScores: {[metric: string]: d3m.SolutionScore} = {};

    client.getScoreSolutionsResults(requestId, update => {
      // we first accumulate all received scores locally
      const scores = update.getScoresList();
      if (scores && scores.length > 0) {
        scores.forEach((s) => {
          const metric = Object
            .keys(PerformanceMetric)
            .find(k => PerformanceMetric[k] === s.getMetric().getMetric());
          returnedScores[metric] = {
            metric: metric,
            score: s.getValue().getRaw().getDouble(),
          };
        });
      }

      const state = update.getProgress().getState();

      // when the progress is completed, resolve all scores
      if (state === ProgressState.COMPLETED) {
        // return scores in the same order that they were returned from TA2
        resolve({
          status: d3m.ScoringStatus.COMPLETED,
          scores: returnedScores,
        });
      }

      // if scoring errored, resolve with errored state
      if (state === ProgressState.ERRORED) {
        console.error(`Score solution for solutionID=[${solutionId}] and ` +
          `request_id=[${requestId}] errored.`);
        resolve({
          status: d3m.ScoringStatus.ERRORED,
        });
      }
    });
  });
}

export async function fitSolution(
  req: {
    solutionID: string,
    problem: d3m.Problem,
}): Promise<FitSolutionResponse> {
  const { solutionID, problem } = req;
  const dataset = getDataSource().getDatasetById(problem.inputs.data[0].datasetID);
  const datasetSplits = await getDatasetSplitter()
    .getSplitsForDataset(dataset, toProtobufProblemDescription(problem));

  return client.fitSolutionRequest(solutionID, datasetSplits.training_dataset_uri);
}

export async function getFitSolutionResults(
  requestID: string,
  onUpdate: (update: GetFitSolutionResultsResponse) => void,
) {
  return client.getFitSolutionResults(requestID, onUpdate);
}

/**
 * Gets the final fitted solution ID given a FitSolution request.
 */
export async function getFitSolutionFittedSolutionID(requestID: string) {
  return new Promise<{ fittedSolutionID: string }>(resolve => {
    getFitSolutionResults(
      requestID,
      async (fitUpdate: GetFitSolutionResultsResponse) => {
        const state = fitUpdate.getProgress().getState();
        // Wait for FitSolution to complete
        if (state === ProgressState.COMPLETED) {
          const fittedSolutionID = fitUpdate.getFittedSolutionId();
          resolve({ fittedSolutionID });
        }
        if (state === ProgressState.ERRORED) {
          // TODO: Implement a better error handling
          console.error(`Fit solution results for requestID=[${requestID}] errored.`);
        }
      },
    );
  });
}

/**
 * Produces the solution using the given inputs.
 */
export async function produceSolution(req: {
  fittedSolutionID: string;
  inputs: Array<{ dataset_uri: string }>;
}): Promise<ProduceSolutionResponse> {
  const { fittedSolutionID, inputs } = req;
  return client.produceSolution(fittedSolutionID, inputs);
}

export function stopSearchSolutions(searchID) {
  return client.stopSolutionSearch(searchID);
}

export async function getProduceSolutionResults(
  requestID: string,
  onUpdate: (update: GetProduceSolutionResultsResponse) => void,
) {
  return client.getProduceSolutionResults(requestID, onUpdate);
}

/**
 * Returns the final produced CSV URI for a ProduceSolution request.
 */
export async function getProduceSolutionCsvUri(requestID: string) {
  return new Promise<string>((resolve, reject) => {
    getProduceSolutionResults(requestID, update => {
      const state = update.getProgress().getState();
      if (state === ProgressState.COMPLETED) {
        const value: Value = update.getExposedOutputsMap().get('outputs.0');
        resolve(value.getCsvUri());
      } else if (state === ProgressState.ERRORED) {
        reject(new Error(`ProduceSolution request for requestID=[${requestID}] errored.`));
      }
    });
  });
}

export async function solutionExport(solutionId: string, rank: number) {
  return await client.solutionExport(solutionId, rank);
}
