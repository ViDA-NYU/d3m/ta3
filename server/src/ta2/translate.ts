import * as d3m from 'd3m/dist/';
import * as problems from 'd3m/dist/problems';

import {
  Problem,
  ProblemPerformanceMetric,
  ProblemDescription,
  ProblemInput,
  ProblemTarget,
  DataAugmentation,
  TaskKeyword,
} from 'ta3ta2-api-ts/problem_pb';

import * as problem_pb from 'ta3ta2-api-ts/problem_pb';
import { PipelineDescription, PipelineDescriptionStep } from 'ta3ta2-api-ts/pipeline_pb';
import { Primitive } from 'ta3ta2-api-ts/primitive_pb';

const TranslateTaskKeywords = {
  [problems.TaskKeyword.classification]: TaskKeyword.CLASSIFICATION,
  [problems.TaskKeyword.regression]: TaskKeyword.REGRESSION,
  [problems.TaskKeyword.clustering]: TaskKeyword.CLUSTERING,
  [problems.TaskKeyword.linkPrediction]: TaskKeyword.LINK_PREDICTION,
  [problems.TaskKeyword.vertexNomination]: TaskKeyword.VERTEX_NOMINATION,
  [problems.TaskKeyword.vertexClassification]: TaskKeyword.VERTEX_CLASSIFICATION,
  [problems.TaskKeyword.communityDetection]: TaskKeyword.COMMUNITY_DETECTION,
  [problems.TaskKeyword.graphMatching]: TaskKeyword.GRAPH_MATCHING,
  [problems.TaskKeyword.forecasting]: TaskKeyword.FORECASTING,
  [problems.TaskKeyword.collaborativeFiltering]: TaskKeyword.COLLABORATIVE_FILTERING,
  [problems.TaskKeyword.objectDetection]: TaskKeyword.OBJECT_DETECTION,
  [problems.TaskKeyword.semiSupervised]: TaskKeyword.SEMISUPERVISED,
  [problems.TaskKeyword.binary]: TaskKeyword.BINARY,
  [problems.TaskKeyword.multiClass]: TaskKeyword.MULTICLASS,
  [problems.TaskKeyword.multiLabel]: TaskKeyword.MULTILABEL,
  [problems.TaskKeyword.univariate]: TaskKeyword.UNIVARIATE,
  [problems.TaskKeyword.multivariate]: TaskKeyword.MULTIVARIATE,
  [problems.TaskKeyword.overlapping]: TaskKeyword.OVERLAPPING,
  [problems.TaskKeyword.nonOverlapping]: TaskKeyword.NONOVERLAPPING,
  [problems.TaskKeyword.tabular]: TaskKeyword.TABULAR,
  [problems.TaskKeyword.relational]: TaskKeyword.RELATIONAL,
  [problems.TaskKeyword.image]: TaskKeyword.IMAGE,
  [problems.TaskKeyword.audio]: TaskKeyword.AUDIO,
  [problems.TaskKeyword.video]: TaskKeyword.VIDEO,
  [problems.TaskKeyword.speech]: TaskKeyword.SPEECH,
  [problems.TaskKeyword.text]: TaskKeyword.TEXT,
  [problems.TaskKeyword.graph]: TaskKeyword.GRAPH,
  [problems.TaskKeyword.multiGraph]: TaskKeyword.MULTIGRAPH,
  [problems.TaskKeyword.timeSeries]: TaskKeyword.TIME_SERIES,
  [problems.TaskKeyword.grouped]: TaskKeyword.GROUPED,
  [problems.TaskKeyword.geospatial]: TaskKeyword.GEOSPATIAL,
  [problems.TaskKeyword.remoteSensing]: TaskKeyword.REMOTE_SENSING,
  [problems.TaskKeyword.lupi]: TaskKeyword.LUPI,
  [problems.TaskKeyword.missingMetadata]: TaskKeyword.MISSING_METADATA,
};

const PerformanceMetricToProtobufTranslator = {
  [problem_pb.PerformanceMetric.ACCURACY]: problems.MetricValue.accuracy.toString(),
  [problem_pb.PerformanceMetric.F1]: problems.MetricValue.f1.toString(),
  [problem_pb.PerformanceMetric.F1_MACRO]: problems.MetricValue.f1Macro.toString(),
  [problem_pb.PerformanceMetric.F1_MICRO]: problems.MetricValue.f1Micro.toString(),
  [problem_pb.PerformanceMetric.JACCARD_SIMILARITY_SCORE]: problems.MetricValue.jaccardSimilarityScore.toString(),
  [problem_pb.PerformanceMetric.MEAN_ABSOLUTE_ERROR]: problems.MetricValue.meanAbsoluteError.toString(),
  [problem_pb.PerformanceMetric.MEAN_SQUARED_ERROR]: problems.MetricValue.meanSquaredError.toString(),
  [problem_pb.PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION]: problems.MetricValue.normalizedMutualInformation.toString(),
  [problem_pb.PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION]: problems.MetricValue.objectDetectionAP.toString(),
  [problem_pb.PerformanceMetric.PRECISION]: problems.MetricValue.precision.toString(),
  [problem_pb.PerformanceMetric.PRECISION_AT_TOP_K]: problems.MetricValue.precisionAtTopK.toString(),
  [problem_pb.PerformanceMetric.R_SQUARED]: problems.MetricValue.rSquared.toString(),
  [problem_pb.PerformanceMetric.RECALL]: problems.MetricValue.recall.toString(),
  [problem_pb.PerformanceMetric.ROC_AUC]: problems.MetricValue.rocAuc.toString(),
  [problem_pb.PerformanceMetric.ROC_AUC_MACRO]: problems.MetricValue.rocAucMacro.toString(),
  [problem_pb.PerformanceMetric.ROC_AUC_MICRO]: problems.MetricValue.rocAucMicro.toString(),
  [problem_pb.PerformanceMetric.ROOT_MEAN_SQUARED_ERROR]: problems.MetricValue.rootMeanSquaredError.toString(),
  [problem_pb.PerformanceMetric.HAMMING_LOSS]: problems.MetricValue.hammingLoss.toString(),
};

export function toCorePerformanceMetric(p: keyof problem_pb.PerformanceMetricMap): problems.MetricValue {
  return PerformanceMetricToProtobufTranslator[p];
}

export function toProtobufPerformanceMetric(p: problems.Metrics): ProblemPerformanceMetric {
  // FIXME: Some metrics have required parameters that should either be read
  // from the problemDoc schema or supplied by the user, e.g., posLabel is
  // required by accuracy, recall, and F1.
  // For details, see:
  // https://gitlab.com/datadrivendiscovery/data-supply/issues/138
  if (
    p.metric === problems.MetricValue.precision ||
    p.metric === problems.MetricValue.recall ||
    p.metric === problems.MetricValue.f1
  ) {
    p.posLabel = '1'; // F1 metric requires pos_label argument
  }
  const ppm = new ProblemPerformanceMetric();
  ppm.setK(p.K);
  ppm.setMetric(d3m.metrics.PerformanceMetricEnumTranslator[p.metric]);
  ppm.setPosLabel(p.posLabel);
  return ppm;
}

export function toProtobufProblemInput(input: problems.InputsData): ProblemInput {
  const pi = new ProblemInput();
  pi.setDatasetId(input.datasetID);
  pi.setTargetsList(
    input.targets.map(
      (t): ProblemTarget => {
        const pt = new ProblemTarget();
        pt.setClustersNumber(t.numClusters);
        pt.setColumnIndex(t.colIndex);
        pt.setColumnName(t.colName);
        pt.setResourceId(t.resID);
        pt.setTargetIndex(t.targetIndex);
        return pt;
      },
    ),
  );
  return pi;
}

export function toProtobufProblemDescription(problem: problems.Problem) {
  const p = new Problem();
  p.setTaskKeywordsList(problem.about.taskKeywords.map(k => TranslateTaskKeywords[k]));
  p.setPerformanceMetricsList(problem.inputs.performanceMetrics.map(toProtobufPerformanceMetric));

  const pd = new ProblemDescription();
  pd.setProblem(p);
  pd.setInputsList(problem.inputs.data.map(toProtobufProblemInput));
  pd.setId(problem.about.problemID);
  pd.setName(problem.about.problemName);
  pd.setVersion(problem.about.problemVersion);
  pd.setDescription(problem.about.problemDescription);
  if (problem.dataAugmentation) {
    pd.setDataAugmentationList(
      problem.dataAugmentation.map(d => {
        const da = new DataAugmentation();
        da.setDomainList(d.domain);
        da.setKeywordsList(d.keywords);
        return da;
      }),
    );
  }

  return pd;
}

function toCorePrimitive(primitive: Primitive): d3m.Primitive {
  return {
    id: primitive.getId(),
    name: primitive.getName(),
    python_path: primitive.getPythonPath(),
  };
}

function toCorePipelineDescriptionStep(step: PipelineDescriptionStep): d3m.PipelineDescriptionStep {
  const primitiveArgs: { [aug: string]: d3m.SolutionDescriptionContainerArgument } = {};
  step
    .getPrimitive()
    .getArgumentsMap()
    .forEach((argument, key) => {
      primitiveArgs[key] = {
        argument: 'container',
        container: {
          data: argument.getContainer().getData(),
        },
      };
    });

  const primitiveOutputs: { id: string }[] = step
    .getPrimitive()
    .getOutputsList()
    .map(o => {
      return { id: o.getId() };
    });

  return {
    primitive: {
      primitive: toCorePrimitive(step.getPrimitive().getPrimitive()),
      arguments: primitiveArgs,
      outputs: primitiveOutputs,
    },
  };
}

export function toCorePipelineDescription(pipelineDescription: PipelineDescription): d3m.PipelineDescription {
  const steps: d3m.PipelineDescriptionStep[] = pipelineDescription
    .getStepsList()
    .map(toCorePipelineDescriptionStep);

  const inputs = pipelineDescription
    .getInputsList()
    .map(i => {
      return { name: i.getName() };
    });

  const outputs = pipelineDescription
    .getOutputsList()
    .map(o => {
      return {
        name: o.getName(),
        data: o.getData(),
      };
    });

  return {
    steps: steps,
    inputs: inputs,
    outputs: outputs,
  };
}
