import * as NodeCache from 'node-cache';
import * as d3m from 'd3m/dist';
import { toDebugString } from '../utils';
import { withCache } from '../cache';
import { usageLogger } from '../usage-log';
import { GRPC_CONN_URL } from '../env';
import { CoreClient } from 'ta3ta2-api-ts/core_grpc_pb';
import { grpc } from 'ta3ta2-api-ts/grpc-lib';
import { Value, ValueType } from 'ta3ta2-api-ts/value_pb';
import {
  SearchSolutionsRequest,
  DescribeSolutionResponse,
  SolutionExportRequest,
  SolutionExportResponse,
  StopSearchSolutionsRequest,
  StopSearchSolutionsResponse,
  FitSolutionRequest,
  FitSolutionResponse,
  ProduceSolutionRequest,
  ProduceSolutionResponse,
  GetSearchSolutionsResultsRequest,
  GetSearchSolutionsResultsResponse,
  GetScoreSolutionResultsRequest,
  GetScoreSolutionResultsResponse,
  GetFitSolutionResultsRequest,
  GetFitSolutionResultsResponse,
  GetProduceSolutionResultsRequest,
  GetProduceSolutionResultsResponse,
} from 'ta3ta2-api-ts/core_pb';
import {
  SearchSolutionsResponse,
  ScoreSolutionRequest,
  ScoreSolutionResponse,
  DescribeSolutionRequest,
} from 'ta3ta2-api-ts/core_pb';

const cache = new NodeCache();

export enum StreamStatus {
  UPDATE = 'UPDATE',
  END = 'END',
  ERROR = 'ERROR',
}

export interface StreamUpdate<T> {
  status: StreamStatus;
  data?: T;
}

export class TA2Client {

  private _grpcClient: CoreClient;

  constructor() { }

  get grpcClient() {
    if (!this._grpcClient) {
      console.log('\nConnecting to TA2 GRPC API at', GRPC_CONN_URL);
      this._grpcClient = new CoreClient(GRPC_CONN_URL, grpc.credentials.createInsecure());
    }
    return this._grpcClient;
  }

  async createStreamHandler<T>(
    method: string,
    request,
    call: grpc.ClientReadableStream,
    onUpdate: (update: T) => void,
  ) {
    return new Promise((resolve, reject) => {

      console.log(`\n> Calling gRPC stream method ${method}() with params:\n`, toDebugString(request.toObject()));
      usageLogger.logGrpcApi(method, request.toObject());

      call.on('data', (update) => {
        console.log(`\n> got gRPC stream data from ${method}():\n`, toDebugString(update.toObject()));
          usageLogger.logGrpcApi(method, update.toObject());
          onUpdate(update);
      });

      call.on('end', () => {
        console.error(`\n> ${method}() gRPC stream finished.`);
        resolve();
      });

      call.on('error', (error: Error) => {
        console.error(`\n> ${method}() gRPC stream request failed`, error);
        reject(error);
      });
    });
  }

  createResponseHandler<T>(method: string, request, resolve, reject): grpc.requestCallback<T> {
    console.log(`\n> Calling gRPC method ${method}() with params:\n`, toDebugString(request.toObject()));
    usageLogger.logGrpcApi(method, request);
    return (err, response) => {
      usageLogger.logGrpcApi(method, response);
      if (err) {
        // We reject the the promise when the execution fails.
        // Client should always catch the error using the try/catch
        // syntax or the then()/catch() Promise functions.
        console.log(`\n> ${method}() gRPC call failed.`);
        reject(err);
      } else {
        console.log(`\n> got gRPC response from ${method}():\n`, toDebugString(response.toObject()));
        resolve(response);
      }
    };
  }

  async searchSolutions(request: SearchSolutionsRequest): Promise<SearchSolutionsResponse>  {
    return new Promise((resolve, reject) => {
      this.grpcClient.searchSolutions(
        request,
        this.createResponseHandler('SearchSolutions', request, resolve, reject),
      );
    });
  }

  async describeSolution(solutionId: string): Promise<DescribeSolutionResponse> {

    const request = new DescribeSolutionRequest();
    request.setSolutionId(solutionId);

    const cacheKey = `describeSolution_${request.getSolutionId()}`;
    let currValue: DescribeSolutionResponse = cache.get(cacheKey);
    if (!currValue) {
      currValue = await (
        new Promise((resolve, reject) => {
          this.grpcClient.describeSolution(
            request,
            this.createResponseHandler('DescribeSolution', request, resolve, reject),
          );
        })
      );
      cache.set(cacheKey, currValue);
    }
    return currValue;
  }

  async getSearchSolutionsResults(
    searchId: string,
    onUpdate: (update: GetSearchSolutionsResultsResponse) => void,
  ) {
    const request = new GetSearchSolutionsResultsRequest();
    request.setSearchId(searchId);

    const call = this.grpcClient.getSearchSolutionsResults(request);
    return this.createStreamHandler('GetSearchSolutionsResults', request, call, onUpdate);
  }

  async scoreSolution(request: ScoreSolutionRequest): Promise<ScoreSolutionResponse> {
    return new Promise((resolve, reject) => {
      this.grpcClient.scoreSolution(request,
        this.createResponseHandler('ScoreSolution', request, resolve, reject),
      );
    });
  }

  async getScoreSolutionsResults(
    requestId: string,
    onUpdate: (update: GetScoreSolutionResultsResponse) => void,
  ) {
    const request = new GetScoreSolutionResultsRequest();
    request.setRequestId(requestId);

    const call = this.grpcClient.getScoreSolutionResults(request);
    return this.createStreamHandler('GetScoreSolutionResults', request, call, onUpdate);
  }

  async fitSolutionRequest(solutionID: string, datasetUri: string): Promise<FitSolutionResponse> {
    const value = new Value();
    value.setDatasetUri(datasetUri);

    const request = new FitSolutionRequest();
    request.setSolutionId(solutionID);
    request.setInputsList([value]);
    request.setExposeOutputsList([]);
    request.setExposeValueTypesList([ValueType.CSV_URI]);

    const cacheKey = `ta2/client/fitSolutionRequest/${solutionID}/${datasetUri}`;
    return withCache(cacheKey, async () => await this.doFitSolution(request));
  }

  private async doFitSolution(request: FitSolutionRequest): Promise<FitSolutionResponse> {
    return new Promise((resolve, reject) => {
      this.grpcClient.fitSolution(
        request,
        this.createResponseHandler('FitSolution', request, resolve, reject),
      );
    });
  }

  async getFitSolutionResults(
    requestId: string,
    onUpdate?: (update: GetFitSolutionResultsResponse) => void,
  ) {
    const request = new GetFitSolutionResultsRequest();
    request.setRequestId(requestId);

    const cacheKey = `ta2/client/getFitSolutionResults/${requestId}`;
    return withCache(
      cacheKey,
      async () => await this.createStreamHandler(
        'GetFitSolutionResults',
        request,
        this.grpcClient.getFitSolutionResults(request),
        onUpdate,
      ),
    );
  }

  async produceSolution(fittedSolutionID: string, inputs: Array<{ dataset_uri: string }>): Promise<ProduceSolutionResponse> {
    const request = new ProduceSolutionRequest();
    request.setFittedSolutionId(fittedSolutionID);
    request.setInputsList(inputs.map(i => {
      const v = new Value();
      v.setDatasetUri(i.dataset_uri);
      return v;
    }));
    request.setExposeOutputsList(['outputs.0']);
    request.setExposeValueTypesList([ValueType.CSV_URI]);

    const cacheKey = `ta2/client/produceSolutionRequest/${fittedSolutionID}/${inputs[0].dataset_uri}`;
    return withCache(cacheKey, async () => await this.doProduceSolution(request));
  }

  private async doProduceSolution(request: ProduceSolutionRequest): Promise<ProduceSolutionResponse> {
    return new Promise((resolve, reject) => {
      this.grpcClient.produceSolution(
        request,
        this.createResponseHandler('ProduceSolution', request, resolve, reject),
      );
    });
  }

  async stopSolutionSearch(searchId: string): Promise<StopSearchSolutionsResponse> {
    const request = new StopSearchSolutionsRequest();
    request.setSearchId(searchId);
    return new Promise((resolve, reject) => {
      this.grpcClient.stopSearchSolutions(
        request,
        this.createResponseHandler('StopSearchSolutions', request, resolve, reject),
      );
    });
  }

  async solutionExport(solutionId: string, rank: number): Promise<SolutionExportResponse> {
    const request = new SolutionExportRequest();
    request.setSolutionId(solutionId);
    request.setRank(rank);
    return new Promise((resolve, reject) => {
      this.grpcClient.solutionExport(
        request,
        this.createResponseHandler('SolutionExport', request, resolve, reject),
      );
    });
  }

  async getProduceSolutionResults(
    requestId: string,
    onUpdate: (update?: GetProduceSolutionResultsResponse) => void,
  ) {
    const request = new GetProduceSolutionResultsRequest();
    request.setRequestId(requestId);

    const cacheKey = `ta2/client/getProduceSolutionResults/${requestId}`;

    return withCache(
      cacheKey,
      async () => await this.createStreamHandler(
        'GetProduceSolutionResults',
        request,
        this.grpcClient.getProduceSolutionResults(request),
        onUpdate,
      ),
    );
  }
}

export default new TA2Client();
