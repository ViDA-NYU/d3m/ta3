import * as NodeCache from 'node-cache';

const cache = new NodeCache();

export async function withCache<T>(cacheKey: string, loader: () => Promise<T>): Promise<T> {
    const currValue = cache.get<T>(cacheKey);
    if (currValue) {
        console.log('==== Using cache for', cacheKey);
        return currValue;
    } else {
        console.log('==== NO cache for', cacheKey, 'calling loader');
        const result = await loader();
        cache.set<T>(cacheKey, result);
        return result;
    }
}

// class Tape<U> {
//     private subscribers = [];
//     constructor() {

//     }
//     subscribe((update: U) => void) {

//     }
// }

// export async function withTape<R, U>(cacheKey: string, loader: () => Promise<R>, onUpdate: (update: U) => void) {
//     let tape = cache.get<Tape<U>>(cacheKey);
//     if (!tape) {
//         tape = new Tape();
//         tape.subscribe(onUpdate)
//     }
// }

export default cache;
