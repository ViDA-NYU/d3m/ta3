// TODO: prepare to remove this api file

import { Response, Request, NextFunction } from 'express';
import * as fs from 'fs';
import * as path from 'path';
import * as fsapi from '../rest/loader';

interface Dataset {
  id: string;
  size: string;
  description: string;
  nColumns: number;
  datasetdocUri: string;
}

/*
  Function that returns the available datasets to the client.
*/
export let getDatatasetList = (req: Request, res: Response, next: NextFunction) => {
  fsapi.walk((err: NodeJS.ErrnoException, fnames: string[]) => {
    const ret: Dataset[] = [];
    fnames.forEach(fname => {
      const datasetDocPath = path.join(fname, fname + '_dataset', 'datasetDoc.json');
      const problemDocPath = path.join(fname, fname + '_problem', 'problemDoc.json');
      if (fsapi.hasDoc(datasetDocPath)) {
        const datasetDoc = fsapi.readDatasetDoc(datasetDocPath);
        const size = datasetDoc.about.approximateSize;
        const id = fname;
        let description = '';
        if (fsapi.hasDoc(problemDocPath)) {
          const problemDoc = fsapi.readProblemDoc(problemDocPath);
          if (problemDoc.about.problemDescription) {
            description = problemDoc.about.problemDescription;
          }
        }
        const datasetdocUri = datasetDocPath;
        let nColumns = 0;
        for (let i = 0; i < datasetDoc.dataResources.length; ++i) {
          if (datasetDoc.dataResources[i].columns) {
            nColumns += datasetDoc.dataResources[i].columns.length - 1;
          }
        }
        ret.push({ id, size, description, nColumns, datasetdocUri });
      }
    });
    res.json(ret);
  });
};

export let getDatasetResources = (req: Request, res: Response, next: NextFunction) => {
  const datasetId = req.body.datasetId;
  const datasetDocPath = path.join(datasetId, datasetId + '_dataset', 'datasetDoc.json');
  if (fsapi.hasDoc(datasetDocPath)) {
    const datasetDoc = fsapi.readDatasetDoc(datasetDocPath);
    res.json(datasetDoc.dataResources);
  }
};
