import { Response } from 'express';
import { readLearningDataTable, readPredTable, readPredTableObjectDetection, readDatasetDoc, getBase } from '../loader';
import * as d3m from 'd3m/dist';
import { getDataSource } from '../../data/config';
import { ColumnRole } from 'd3m/dist';
import * as _ from 'lodash';
import * as path from 'path';
const sizeOf = require('image-size');

export const computeConfusionMatrix = async (problem: d3m.Problem, predictionURI: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  try {
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTable(predictionURI, table, target);
    const [ tIxs, pIxs ] = table.intersectIndices(predTable);
    const labels = table.getValues(table.getLabelColumn(target), tIxs);
    const preds = predTable.getValues(predTable.getLabelColumn(target), pIxs);
    const conf: { [index: string]: { [index: string]: number } } = {};
    labels.forEach((l, ix) => {
      const p = preds[ix];
      if (!(p in conf)) {
        conf[p] = {};
      }
      if (!(l in conf)) {
        conf[l] = {};
      }
      if (!(l in conf[p])) {
        conf[p][l] = 0;
      }
      conf[p][l] += 1;
    });
    const classDefs = Object.keys(conf);
    classDefs.forEach((p) => {
      classDefs.forEach((l) => {
        if (!(p in conf)) {
          conf[p] = {};
        }
        if (!(l in conf[p])) {
          conf[p][l] = 0;
        }
      });
    });
    res.json({ conf, classDefs });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};

export const computeConfusionScatterplot = async (problem: d3m.Problem, predictionURI: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  try {
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTable(predictionURI, table, target);
    const [ tIxs, pIxs ] = table.intersectIndices(predTable);
    const labels = table.getValues(table.getLabelColumn(target), tIxs);
    const preds = predTable.getValues(predTable.getLabelColumn(target), pIxs);
    const points = preds.map((v, ix) => [ v, labels[ix] ]);
    res.json({ points });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};

export const computeForecastingLineChart = async (problem: d3m.Problem, predictionURI: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  const dataset = getDataSource().getDatasetById(inputData.datasetID);
  const resources = dataset.dataResources;
  const columns = resources[0].config.columns; // TODO: Check which resource to use if there is more than one
  let forecastingDatetimeColumnName: string = undefined;
  for (const column of columns) {
    if (column.role.includes(ColumnRole.timeIndicator)) {
      forecastingDatetimeColumnName = column.colName;
    }
  }
  try {
    if (forecastingDatetimeColumnName === undefined) {
      throw Error('No Datetime variable found');
    }
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTable(predictionURI, table, target);
    const [ tIxs, pIxs ] = table.intersectIndices(predTable);
    const datetime = table.getValues(table.getLabelColumn(forecastingDatetimeColumnName), tIxs);
    const labels = table.getValues(table.getLabelColumn(target), tIxs);
    const preds = predTable.getValues(predTable.getLabelColumn(target), pIxs);
    const points = preds.map((v, ix) => [ v, labels[ix], datetime[ix] ]).slice(1000);

    res.json({ points });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};

export const computePredictedImagesData = async (problem: d3m.Problem, predictionURI: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  const dataset = getDataSource().getDatasetById(inputData.datasetID);
  const resources = dataset.dataResources;
  let columns: d3m.Column[] = undefined;
  for (const resource of resources) {
    if (resource.resourceID === 'learningData') {
      columns = resource.config.columns;
    } else {
      console.error('There is no tabular data.');
    }
  }
  let imageNameFileColumnName: string = undefined;
  for (const column of columns) {
    // TODO: It will not work when there is more than two resources with media data.
    if (!(_.isEmpty(column.refersTo))) { // check if the column refer to some other resource
      if (column.refersTo.resID !== 'learningData') {
        imageNameFileColumnName = column.colName;
      }
    }
  }
  try {
    if (imageNameFileColumnName === undefined) {
      throw Error('No Image Name variable found');
    }
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTable(predictionURI, table, target);
    const [ tIxs, pIxs ] = table.intersectIndices(predTable);
    const imageNames = table.getValues(table.getLabelColumn(imageNameFileColumnName), tIxs);
    const labels = table.getValues(table.getLabelColumn(target), tIxs); // Ground Truth
    const preds = predTable.getValues(predTable.getLabelColumn(target), pIxs);
    const points = preds.map((v, ix) => [ v, labels[ix], imageNames[ix] ]); // [Predicted, grount-truth, image-name]
    res.json({ points });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};

const scalePoint = (point: string, GTSize: number, sizeImage: string): string => {
  const ratio = parseInt(sizeImage) / GTSize;
  const newPoint = Math.floor(ratio * parseInt(point));
  return newPoint.toString();
};

const scaleXYPoints = (arrayBoundings: (string | number)[], sizeImage: string, pathToImageFile: string): string[] => {
  const dimensions = sizeOf(pathToImageFile);
  const scaledPointsArray = [];
  arrayBoundings.forEach((points, idx) => {
    const predArray = (points).toString().split(',');
    predArray.forEach((point, index) => {
      if (index % 2 === 0) {
        predArray[index] = scalePoint(point, dimensions.width, sizeImage);
      } else {
        predArray[index] = scalePoint(point, dimensions.height, sizeImage);
      }
    });
    const scaledPoints = predArray.toString();
    scaledPointsArray[idx] = scaledPoints;
  });
  return scaledPointsArray;
};

export const getPredictedBoundingBoxInfo = async (problem: d3m.Problem, predictionURI: string, sizeImage: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  const dataset = getDataSource().getDatasetById(inputData.datasetID);
  const resources = dataset.dataResources;
  let columns: d3m.Column[] = undefined;

  for (const resource of resources) {
    if (resource.resourceID === 'learningData') {
      columns = resource.config.columns;
    } else {
      console.error('There is no tabular data.');
    }
  }
  let imageNameFileColumnName: string = undefined;
  let resIDImageFile: string = undefined;
  for (const column of columns) {
    // TODO: It will not work when there is more than two resources with media data.
    if (!(_.isEmpty(column.refersTo))) { // check if the column refer to some other resource. It should not refer to learningData resource
      if (column.refersTo.resID !== 'learningData') {
        imageNameFileColumnName = column.colName;
        resIDImageFile = column.refersTo.resID;
      }
    }
  }
  try {
    if (imageNameFileColumnName === undefined) {
      throw Error('No Image Name variable found');
    }
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTableObjectDetection(predictionURI, table);
    const [ tIxs, pIxs ] = table.intersectIndicesMultiIndex(predTable);
    const tIxsUnique = tIxs.map((v) => v[0]);
    const imageNames = table.getValues(table.getLabelColumn(imageNameFileColumnName), tIxsUnique);

    let pathToImageFiles: string = undefined;
    const doc = readDatasetDoc(datasetDocPath);
    //  Handling multiple resources.
    //  Find the index position of the media resource.
    for (const resource of doc.dataResources) {
      if (resource.resID === resIDImageFile) {
        pathToImageFiles = resource.resPath;
        break;
      } else {
        console.error('Error. There is no media data.');
      }
    }
    const labels = table.getValuesMultiIndex(table.getLabelColumn(target), tIxs); // Ground Truth
    const preds = predTable.getValuesMultiIndex(predTable.getLabelColumn(target), pIxs);
    const newPreds = [];
    const newlabels = [];
    imageNames.forEach((imageName , index) => {
      const pathToImageFile = path.join(path.join(datasetDocPath, '..', pathToImageFiles), (imageName).toString());
      newPreds.push(scaleXYPoints(preds[index], sizeImage, pathToImageFile));
      newlabels.push(scaleXYPoints(labels[index], sizeImage, pathToImageFile));
    });
    const points = newPreds.map((v, ix) => [ v, newlabels[ix], imageNames[ix] ]); // [Predicted[], grount-truth[], image-name]
    res.json({ points });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};
