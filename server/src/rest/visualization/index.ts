import { Router } from 'express';
import { evaluateResult } from '../store';
import { computeConfusionMatrix, computeConfusionScatterplot, computeForecastingLineChart, computePredictedImagesData, getPredictedBoundingBoxInfo } from './confusion';
import { computeRuleMatrix } from './rule-matrix';
import * as d3m from 'd3m/dist';
import { computeRegressionPartialPlots } from './partialPlots';
import { usageLogger } from '../../usage-log';

const visualization = Router();

/**
 * Middleware that makes sure that the requested fittedSolutionID is valid.
 */
visualization.post('*', async (req, res, next) => {
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  if (!(fittedSolutionID in evaluateResult)) {
    return res.status(400).send(`"${fittedSolutionID}" is not a valid fitted solution ID`);
  }
  next();
});

/**
 * Requests computation of confusion matrix for a given evaluated solution (classification only).
 */
visualization.post('/confusion-matrix', async (req, res) => {
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  usageLogger.logEvent(d3m.UserEvent.VIEW_CONFUSION_MATRIX, { fittedSolutionID : fittedSolutionID });
  const result = evaluateResult[fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  computeConfusionMatrix(
    problem,
    predictionCsvUri,
    res,
  );
});

/**
 * Requests computation of confusion scatterplot for a given evaluated solution (regression only).
 */
visualization.post('/confusion-scatterplot', async (req, res) => {
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  usageLogger.logEvent(d3m.UserEvent.VIEW_CONFUSION_SCATTER_PLOT, { fittedSolutionID : fittedSolutionID });
  const result = evaluateResult[fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  computeConfusionScatterplot(
    problem,
    predictionCsvUri,
    res,
  );
});

/**
 * Requests computation of time series forecasting line chart for a given evaluated solution (time series forecasting only).
 */
visualization.post('/forecasting-line-chart', async (req, res) => {
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  const result = evaluateResult[fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  await computeForecastingLineChart(
    problem,
    predictionCsvUri,
    res,
  );
});

/**
 * Requests computation of predicted images info for a given evaluated solution (image only).
 */
visualization.post('/predicted-images-data', async (req, res) => {
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  const result = evaluateResult[fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  await computePredictedImagesData(
    problem,
    predictionCsvUri,
    res,
  );
});

export interface PredictedObjectImagesParams {
  fittedSolutionID: string;
  sizeImage: string;
}

/**
 * Requests computation of image chart for a given evaluated solution (image only).
 */
visualization.post('/predicted-object-images-data', async (req, res) => {
  const params: PredictedObjectImagesParams = req.body;
  const result = evaluateResult[params.fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  const sizeImage = params.sizeImage;
  await getPredictedBoundingBoxInfo(
    problem,
    predictionCsvUri,
    sizeImage,
    res,
  );
});

/**
 * Requests computation of rule matrix for an evaluated solution.
 */
visualization.post('/rule-matrix', async (req, res) => {
  const { fittedSolutionID, ruleMatrixConfig } = req.body as { fittedSolutionID: string, ruleMatrixConfig: d3m.RuleMatrixConfig[] };
  usageLogger.logEvent(d3m.UserEvent.VIEW_RULE_MATRIX, { fittedSolutionID : fittedSolutionID });
  const result = evaluateResult[fittedSolutionID];
  /* NOTE(Sonia) featureIndices and targetIndices in RuleMatrixConfig will be
     updated later if the user have defined a problem by himself. */
  computeRuleMatrix(
    fittedSolutionID,
    ruleMatrixConfig,
    result.problem,
    result.predictionCsvUri,
    res,
  );
});

export interface RegressionPartialPlotsParams {
  fittedSolutionID: string;
  problem: d3m.Problem;
}

visualization.post('/_regressionPartialPlots', async (req, res) => {
  const params: RegressionPartialPlotsParams = req.body;
  usageLogger.logEvent(d3m.UserEvent.VIEW_PARTIAL_DEPENDENCE_PLOTS, { fittedSolutionID : params.fittedSolutionID });
  try {
    const result = await computeRegressionPartialPlots(params.fittedSolutionID, params.problem, req.context.dataSource);
    res.json(result);
  } catch (err) {
    res.json(err);
  }
});

export default visualization;
