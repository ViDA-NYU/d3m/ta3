import * as express from 'express';
import * as request from 'supertest';
import datasets from '../datasets';
import { DataSource } from '../../data/DataSource';
import { getDataSourceConfig } from '../../data/config';

function init() {
  const app = express();
  const dataSource = new DataSource(getDataSourceConfig());
  app.use((req, res, next) => {
    req.context = { dataSource };
    next();
  });
  app.use(datasets);
  return app;
}

describe('Datasets GET /', function() {
  it('return the list of datasets', function() {
    const app = init();
    return request(app)
      .get('/')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        return expect(response.body).toHaveLength(12);
      });
  });

  it('returns one dataset given a id', function() {
    const app = init();
    return request(app)
      .get('/185_baseball_dataset')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        return expect(response.body).toMatchObject({
          about: { datasetName: 'baseball' },
        });
      });
  });

  it('returns the problems for a dataset', function() {
    const app = init();
    return request(app)
      .get('/185_baseball_dataset/problems')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        return expect(response.body[0]).toMatchObject({
          about: { problemName: 'baseball_problem' },
        });
      });
  });
});
