import { Router } from 'express';
import resources from './resources';
import { usageLogger } from '../usage-log';
import * as d3m from 'd3m/dist';
import * as fs from 'fs';
import * as path from 'path';
import * as env from '../env';
import { mkdirIfAbsent } from '../utils';
import { spawn } from 'child_process';
import { getDataSource } from '../data/config';
import { RestRequestState } from 'd3m/dist';

const TMP_DATASET_PATH = path.join(env.TMP_DIR, 'dataset_upload');

const LOAD_NEW_DATASET_ERROR = { error: 'err' };
const LOAD_NEW_DATASET_START = { start: true };

const PYTHON_BIN = 'python3';

export const loadNewDatasetResults: {
  [id: string]: d3m.RuleMatrixData | d3m.RuleMatrixStart | d3m.RuleMatrixError;
} = {};
export let tempData: string = '';
const datasets = Router();

// get resources info and data
datasets.use('/:datasetId/resources', async (req, res, next) => {
  req.context.dataset = await req.context.dataSource.getDatasetById(req.params.datasetId);
  resources(req, res, next);
});

// Returns the problems for a dataset
datasets.get('/:datasetId/problems', async (req, res) => {
  usageLogger.logEvent(d3m.UserEvent.LIST_PROBLEMS);
  const problems = await req.context.dataSource.getProblemsForDatasetId(req.params.datasetId);
  res.json(problems.map(p => p.toJSON()));
});

// Returns a dataset description
datasets.get('/:datasetId', async (req, res) => {
  usageLogger.logEvent(d3m.UserEvent.GET_DATA_DESCRIPTION);
  const dataset = await req.context.dataSource.getDatasetById(req.params.datasetId);
  res.json(dataset.toJSON());
});

// Returns a list of datasets
datasets.get('/', async (req, res) => {
  usageLogger.logEvent(d3m.UserEvent.LIST_DATASETS);
  const datasetList = await req.context.dataSource.getDatasets();
  res.json(datasetList.map(d => d.toJSON()));
});

const readJSON = (filePath: string): object => {
  return JSON.parse(fs.readFileSync(filePath, 'utf-8').toString());
};

// Data profiling
export async function writeNewDatasets(
  dataID: string,
  dataArrayBuffer: string,
) {
  return new Promise<d3m.ta3.WriteNewDataResponse>((resolve, reject) => {
    const pathNewDataset = path.join(TMP_DATASET_PATH, dataID + '.csv');
    mkdirIfAbsent(pathNewDataset);

    const dataTemp = dataArrayBuffer.replace('data:text/csv;base64,', '');
    const dataStringify = Buffer.from(dataTemp, 'base64').toString('utf-8');

    const wstream = fs.createWriteStream(pathNewDataset);
    wstream.write(dataStringify);
    wstream.end();
    wstream.on('finish', () => {
      const id = `${dataID}:${dataID}`;
      try {
        loadNewDatasetResults[id] = LOAD_NEW_DATASET_START;
        const callSampleParametersFile = pathNewDataset;

        const profilerArgs = [
          '-u',
          env.PYTHON_ROOT + '/dataset-processes/data_profiler.py',
          TMP_DATASET_PATH,
          dataID,
          '--config',
          callSampleParametersFile,
          '-v',
        ];
        console.log('DataProfiler spawning data_profiler.py script with args:\n ', profilerArgs.join(' '));
        const runDataProfiler = spawn(PYTHON_BIN, profilerArgs);

        // Capture stdout and stderr and print on Node's log
        runDataProfiler.stdout.on('data', function(data) {
          console.log(data.toString());
        });
        runDataProfiler.stderr.on('data', function(data) {
          console.warn(data.toString());
        });

        // Listen to error code and register error status
        runDataProfiler.on('error', function(code0) {
          console.log('DataProfiler data_profiler.py returned error: ', code0);
          loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
          console.error('code --1', code0);
        });

        runDataProfiler
          .on('close', function(code0) {
            const pathJsonDatasetInfo = path.join(TMP_DATASET_PATH, dataID + '.json');
            fs.readFile(pathJsonDatasetInfo, 'utf8', (err, jsonString) => {
              if (err) {
                resolve({
                  statusWriteNewData: RestRequestState.ERROR,
                  dataTypes: '',
                  error: 'File read failed. ' + err,
                });
              } else {
                tempData = jsonString;
                console.log('Finished to write JSON at ' + pathJsonDatasetInfo);
                resolve({
                  statusWriteNewData: RestRequestState.SUCCESS,
                  dataTypes: tempData,
                });
              }
            });
            console.log('data_profiler.py closed with code ', code0);
          })
          .on('error', (err0: Error) => {
            loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
            console.error(err0.stack, err0);
          });
      } catch (err) {
        loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
        console.error(err.stack, err);
        console.log('Error while writing csv file.');
        resolve({
          statusWriteNewData: RestRequestState.ERROR,
          dataTypes: '',
          error: 'Error while profiling CSV file. ' + err,
        });
      }
    });
  });
}

export async function saveDatasetD3MFormat(metadata: string) {
  const jsonMetadata = JSON.parse(metadata);
  const filename = jsonMetadata.filename;
  const dataID = jsonMetadata.datasetID;
  const dataSource = getDataSource();

  return new Promise<d3m.ta3.WriteDataD3MFormatResponse>((resolve, reject) => {
    // This path is pointing out to the loaded csv file
    const tmpDatasetPath = path.join(TMP_DATASET_PATH, dataID + '.csv');

    // Path of the directory where the dataset in D3M format will be saved
    const d3mDatasetDirPath = path.join(env.USER_INPUT_DIR, filename);
    mkdirIfAbsent(env.USER_INPUT_DIR);

    const id = `${dataID}:${dataID}`;
    try {
      loadNewDatasetResults[id] = LOAD_NEW_DATASET_START;

      const dataMaterializerArgs = [
        '-u',
        env.PYTHON_ROOT + '/dataset-processes/data_materialize.py',
        tmpDatasetPath,
        metadata,
        '--config',
        d3mDatasetDirPath,
        '-v',
      ];
      console.log('Spawning data_materialize.py script with args', dataMaterializerArgs.join(' '));
      const runDataMaterializer = spawn(PYTHON_BIN, dataMaterializerArgs);

      // Capture stdout and stderr and print on Node's log
      runDataMaterializer.stdout.on('data', function(data) {
        console.log(data.toString());
      });
      runDataMaterializer.stderr.on('data', function(data) {
        console.warn(data.toString());
      });

      // Listen to error code and register error status
      runDataMaterializer.on('error', function(code0) {
        console.log('data_materialize.py returned error: ', code0);
        loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
        console.error('code', code0);
      });
      runDataMaterializer
        .on('close', function(code0) {
          console.log('DataMaterialize data_materialize.py closed with code ', code0);
          if (code0 !== 0) {
            loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
            console.error('code', code0);
            resolve({
              statusSaveNewData: RestRequestState.ERROR,
              error: 'Error: DataMaterialize data_materialize.py closed with code' + code0,
            });
          } else {
            // Re-scans the data directory which will include the new dataset
            dataSource.loadDirectory([d3mDatasetDirPath]);
            // Finished SAVING new dataset in the D3M format correctly
            resolve({ statusSaveNewData: RestRequestState.SUCCESS });
          }
        })
        .on('error', (err0: Error) => {
          loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
          console.error(err0.stack, err0);
        });
    } catch (err) {
      loadNewDatasetResults[id] = LOAD_NEW_DATASET_ERROR;
      console.error(err.stack, err);
      console.log('Error while saving new dataset in the D3M format.');
      resolve({
        statusSaveNewData: RestRequestState.ERROR,
        error: 'Error while saving new dataset in the D3M format. ' + err,
      });
    }
  });
}

export default datasets;
