import * as d3m from 'd3m/dist';
import * as path from 'path';
import * as fs from 'fs';
import { tempStorageRoot } from '../env';
import { ruleMatrixResults } from './visualization/rule-matrix';
import { partialPlotsResults } from './visualization/partialPlots';

export interface EvaluateResult {
  problem: d3m.Problem;
  solutionID: string; // original searched solution id

  predictionCsvUri: string;
  ruleMatrixPredictionCsvUri?: string; // a second prediction CSV used by rule matrix
}

/**
 * Stores the evaluation result of a fitted solution.
 * The key "id" is the fitted solution ID (not searched solution id).
 * The solution explanation components can utialized the evaluated results here to compute visualization data,
 * e.g. for confusion matrix and rule matrix.
 */
const evaluateResult: { [fittedSolutionID: string]: EvaluateResult } = {};

/**
 * Maps an original solution ID to its fitted solution ID (if it has been fitted before).
 */
const fittedSolutions: { [solutionID: string]: string } = {};

const EVALUATION_CACHE_FILENAME = 'solutionCache.json';
const cacheFilePath = path.join(tempStorageRoot(), EVALUATION_CACHE_FILENAME);

interface EvaluationCache {
  evaluateResult: object;
  fittedSolutions: object;
  ruleMatrixResults: object;
  partialPlotsResults: object;
}

const clearObject = (obj: object) => {
  for (const key in obj) {
    delete obj[key];
  }
};

const clearCache = () => {
  clearObject(evaluateResult);
  clearObject(fittedSolutions);
  clearObject(ruleMatrixResults);
  clearObject(partialPlotsResults);
};

const loadCache = (cached: EvaluationCache) => {
  console.log('cached', cached);
  Object.assign(evaluateResult, cached.evaluateResult);
  Object.assign(fittedSolutions, cached.fittedSolutions);
  Object.assign(ruleMatrixResults, cached.ruleMatrixResults);
  Object.assign(partialPlotsResults, cached.partialPlotsResults);
};

const readCachedEvaluations = () => {
  if (fs.existsSync(cacheFilePath)) {
    const cached = JSON.parse(fs.readFileSync(cacheFilePath).toString());
    clearCache();
    loadCache(cached);
  } else {
    console.log('no evaluation cache found');
  }
};

export const cacheEvaluateResult = () => {
  fs.writeFileSync(cacheFilePath, JSON.stringify({
    evaluateResult,
    fittedSolutions,
    ruleMatrixResults,
    partialPlotsResults,
  }), 'utf-8');
};

export const clearCachedEvaluateResult = () => {
  if (fs.existsSync(cacheFilePath)) {
    fs.unlinkSync(cacheFilePath);
  }
  clearCache();
};

// Read cache on every server (re)start.
readCachedEvaluations();

export { evaluateResult, fittedSolutions };
