import { Router } from 'express';
import { WSRequest } from './index';
import * as d3m from 'd3m/dist';
import { getFitSolutionResults, getProduceSolutionResults } from '../ta2';
import { evaluateResult, fittedSolutions, cacheEvaluateResult, clearCachedEvaluateResult } from './store';
import { formatAsFileURI } from '../api/utils';
import { getDataSource, getDatasetSplitter } from '../data/config';
import { circularStringify } from '../utils';
import { computeRegressionPartialPlots } from './visualization/partialPlots';
import { writeNewDatasets, saveDatasetD3MFormat } from './datasets';
import { toProtobufProblemDescription } from '../ta2/translate';
import { RestRequestState, RestResponse } from 'd3m/dist';
import { GetFitSolutionResultsResponse, GetProduceSolutionResultsResponse, ProgressState } from 'ta3ta2-api-ts/core_pb';
import { Value } from 'ta3ta2-api-ts/value_pb';

const solutions = Router();

// Returns resource stats;
solutions.post('/_searchSolutions', async (req, res) => {
  const result = await req.context.ta2.startSolutionSearch(req.body as d3m.SolutionSearchRequest);
  res.json(result);
});

solutions.post('/describeSolution', async (req, res) => {
  const result = await req.context.ta2.describeSolution(req.body.solutionID);
  res.json(JSON.parse(circularStringify(result)));
});

/**
 * Calls FitSolution and ProduceSolution to prepare for TA3 solution evaluation.
 * The method returns the fitted solution ID, which should be used as a key to request visualization data.
 */
const evaluateSolution = async (params: d3m.ta3.EvaluateSolutionRequest, req: WSRequest) => {
  const { solutionID } = params;
  if (solutionID in fittedSolutions) {
    const fittedSolutionID = fittedSolutions[solutionID];
    console.log(`solution ${solutionID} already evaluated with fitted solution ${fittedSolutionID}`);
    return req.send({ fittedSolutionID, error: false });
  }
  const fitRequest = await req.context.ta2.fitSolution(params);
  const fitRequestID = fitRequest.getRequestId();
  getFitSolutionResults(fitRequestID, async (fitUpdate: GetFitSolutionResultsResponse) => {
    // Wait for FitSolution to complete
    if (fitUpdate.getProgress().getState() === ProgressState.COMPLETED) {
      const fittedSolutionID = fitUpdate.getFittedSolutionId();
      // Use the fitted solution to produce solution.
      const inputs = await Promise.all(params.problem.inputs.data.map(async d => {
        const problem = toProtobufProblemDescription(params.problem);
        const dataset = getDataSource().getDatasetById(d.datasetID);
        const splits = await getDatasetSplitter().getSplitsForDataset(dataset, problem);
        return { dataset_uri: splits.testing_dataset_uri };
      }));
      const produceRequest = await req.context.ta2.produceSolution({
        fittedSolutionID,
        inputs: inputs,
      });
      const produceRequestID = produceRequest.getRequestId();

      getProduceSolutionResults(produceRequestID, (produceUpdate: GetProduceSolutionResultsResponse) => {
        const state = produceUpdate.getProgress().getState();
        if (state === ProgressState.COMPLETED) {
          const value: Value = produceUpdate.getExposedOutputsMap().get('outputs.0');
          const csvUri = formatAsFileURI(value.getCsvUri());
          console.log('Produced solution CSV:', csvUri);
          evaluateResult[fittedSolutionID] = {
            problem: params.problem,
            predictionCsvUri: csvUri,
            solutionID,
          };
          fittedSolutions[solutionID] = fittedSolutionID;
          req.send({ fittedSolutionID, error: false});
        } else if (state === ProgressState.ERRORED) {
            req.send({
              error: true,
              fitRequestID,
            });
          }
        });
      } else if (fitUpdate.getProgress().getState() === ProgressState.ERRORED) {
        req.send({
          error: true,
          fitRequestID,
        });
      }
  });
};

export interface RegressionPartialPlotsParams {
  fittedSolutionID: string;
  problem: d3m.Problem;
}
const computePartialDependencePlots = async (params: RegressionPartialPlotsParams, req: WSRequest) => {
  const result = await computeRegressionPartialPlots(params.fittedSolutionID, params.problem, req.context.dataSource);
  return req.send(result);
};

const writeNewDataset = async (params: d3m.ta3.WriteNewDataRequest, req: WSRequest) => {
  const result = await writeNewDatasets(params.dataID, params.data);
  return req.send(result);
};

const writeDatasetD3MFormat = async (params: d3m.ta3.WriteDataD3MFormatRequest, req: WSRequest) => {
  const result = await saveDatasetD3MFormat(params.metadata);
  return req.send(result);
};

solutions.post('/_stopSearchSolutions', async (req, res) => {
  try {
    await req.context.ta2.stopSearchSolutions(req.body.searchID);
    res.json({
      status: RestRequestState.SUCCESS,
      message: 'Request to stop search successful.',
    } as RestResponse);
  } catch (e) {
    console.error('Failed to request to stop search.', e);
    res.status(500).json({
      status: RestRequestState.ERROR,
      message: 'Failed to request to stop search.',
    } as RestResponse);
  }
});

solutions.post('/_exportSolution', async (req, res) => {
  const { solutionID, rank } = req.body;
  try {
    await req.context.ta2.solutionExport(solutionID, rank);
    res.json({
      status: RestRequestState.SUCCESS,
      message: 'Solution exported successfully.',
    } as RestResponse);
  } catch (e) {
    console.error('Failed to export solution', e);
    res.status(500).json({
      status: RestRequestState.ERROR,
      message: 'Failed to export solution',
    } as RestResponse);
  }
});

solutions.post('/cacheEvaluations', (req, res) => {
  cacheEvaluateResult();
  res.json(true);
});

solutions.post('/clearCachedEvaluations', (req, res) => {
  clearCachedEvaluateResult();
  res.json(true);
});

const getSearchResults = (params: d3m.ta3.GetSearchResultsRequest, req: WSRequest) => {
  req.context.ta2.getSearchResults(params.searchID, params.problem, update => {
    req.send(update);
  });
};

export const wsSolutionResolvers = {
  getSearchResults: getSearchResults,
  evaluateSolution: evaluateSolution,
  computePartialDependencePlots: computePartialDependencePlots,
  writeNewDataset: writeNewDataset,
  writeDatasetD3MFormat: writeDatasetD3MFormat,
};

export default solutions;
