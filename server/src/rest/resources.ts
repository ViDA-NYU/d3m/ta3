import { Router } from 'express';
import { usageLogger } from '../usage-log';
import * as d3m from 'd3m/dist';
import { ColumnProfiler } from '../data/ColumnProfiler';

const resources = Router();
const profiler = new ColumnProfiler();

// Returns resource stats;
resources.get('/:resourceID/stats', async (req, res) => {
  usageLogger.logEvent(d3m.UserEvent.VIEW_RESOURCE_STATS);
  const { dataset } = req.context;
  const resource = dataset.getResource(req.params.resourceID);
  res.json(await profiler.profile(resource));
});

resources.post('/:resourceID/sample', async (req, res) => {
  const { dataset } = req.context;
  const resource = dataset.getResource(req.params.resourceID);
  res.json(await resource.getSample(req.body));
});

resources.get('/:resourceID/file/:filename', async (req, res) => {
  const { dataset } = req.context;
  const resource = dataset.getResource(req.params.resourceID);
  res.sendFile(`${resource.getPath()}/${req.params.filename}`);
});
resources.get('/:resourceID/numRows', async (req, res) => {
  const { dataset } = req.context;
  const resource = dataset.getResource(req.params.resourceID);
  res.json(await resource.getSize());
});

// Returns info of a specific id
resources.get('/:resourceID', async (req, res) => {
  const { dataset } = req.context;
  const resource = dataset.getResource(req.params.resourceID);
  res.json(resource.toJSON());
});

// Returns a list of resources
resources.get('/', async (req, res) => {
  res.json({ a: 1 });
});
export default resources;
