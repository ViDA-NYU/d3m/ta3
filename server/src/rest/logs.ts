import { Router } from 'express';
import { usageLogger } from '../usage-log';
import * as d3m from 'd3m/dist';

const logsRouter = Router();

logsRouter.post('/', async (req, res) => {
  console.log('body: ', req.body);
  console.log('params: ', req.params);
  const feature = d3m.UserEvent[req.body.feature];
  if (feature) {
    usageLogger.logEvent(feature, req.body.metadata);
    res.json({ success: true});
  } else {
    console.log('> Received log request without valid feature.');
    res.json({ success: false});
  }
});

export default logsRouter;
