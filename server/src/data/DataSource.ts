import * as path from 'path';
import * as fs from 'fs';
import { Dataset } from './Dataset';
import { formatAsFileURI, isDirectory } from '../api/utils';
import { DATASETS_INFO, DATAMART_MARK } from '../env';
import { Problem } from './Problem';
import { fileExists } from '../utils';

export interface DataSourceConfig {
  inputPaths: string[];
  problemPath: string;
}

export class DataSource {
  private datasets: Dataset[];
  private problems: Problem[];
  private datasetsIdx: { [datasetID: string]: Dataset };
  private config: DataSourceConfig;

  constructor(config: DataSourceConfig) {
    this.config = config;
    this.scanDataDirectories();
  }

  /**
   * Re-scans all data directories
   */
  updateDataSource() {
    this.scanDataDirectories();
  }

  /**
   * Scans the given directories and adds the discovered datasets and problems
   * to the begin of the array.
   */
  loadDirectory(directories: string[]) {
    const datasets = this.findAllDatasets(directories);
    const problems = this.findAllProblems(directories);
    // push new items to the front of the array
    this.datasets.unshift(...datasets);
    this.problems.unshift(...problems);
    // update dataset index
    this.buildDatasetIndex();
  }

  /**
   * Scans all data directories looking for datasets and builds an dataset index.
   */
  scanDataDirectories() {
    const config = this.config;
    if (config.problemPath) {
      if (fileExists(config.problemPath)) {
        const problem = Problem.readFromProblemDocFile(config.problemPath);
        this.datasets = this.findAllDatasets(config.inputPaths).filter(
          d => d.about.datasetID === problem.getInputDatasetID(),
        );
        this.problems = [problem];
      } else {
        console.error(`File ${config.problemPath} does not exist. `);
      }
    } else {
      this.datasets = this.findAllDatasets(config.inputPaths);
      this.problems = this.findAllProblems(config.inputPaths);
    }
    this.buildDatasetIndex();
    console.log(
      `Found ${this.datasets.length} dataset(s) and `
        + `${this.problems.length} problem(s) at:\n - `
        + config.inputPaths.join('\n - '),
    );
  }

  buildDatasetIndex() {
    this.datasetsIdx = {};
    this.datasets.forEach((d: Dataset) => (this.datasetsIdx[d.datasetID] = d));
  }

  /**
   * Recursively walks the given directory paths searching for datasetDoc.json
   * files and returns an array of Dataset objects.
   *
   * @param rootPaths - the paths to the directories to be searched.
   */
  private findAllDatasets(rootPaths: string[]): Dataset[] {
    const allFiles = this.findAllFilesByName(rootPaths, 'datasetDoc.json');
    return allFiles.map(f => Dataset.readFromDatasetDocFile(f));
  }

  /**
   * Recursively walks the given directory paths searching for problemDoc.json
   * files and returns an array of Problem objects.
   *
   * @param rootPaths - the paths to the directories to be searched.
   */
  private findAllProblems(rootPaths: string[]): Problem[] {
    const allFiles = this.findAllFilesByName(rootPaths, 'problemDoc.json');
    return allFiles.map(f => Problem.readFromProblemDocFile(f));
  }

  /**
   * Recursively walks the given directory paths searching for files
   * with the given name and returns an array of string file paths.
   *
   * @param rootPaths - an array of paths to the directories to be searched.
   * @param filename - the name of the files
   */
  private findAllFilesByName(rootPaths: string[], filename: string): string[] {
    // for all paths that are directories, searches for files recursively
    const allFiles: string[] = [];
    if (rootPaths && rootPaths.length > 0) {
      for (const rootPath of rootPaths) {
        if (isDirectory(rootPath)) {
          const childPaths = fs.readdirSync(rootPath);
          if (childPaths && childPaths.length > 0) {
            for (let i = 0; i < childPaths.length; i++) {
              const childPath = path.join(rootPath, childPaths[i]);
              if (isDirectory(childPath)) {
                const files = this.findAllFilesByName([childPath], filename);
                if (files && files.length > 0) {
                  files.forEach(f => allFiles.push(f));
                }
              }
            }
          }
        }
        const filePath: string = path.join(rootPath, filename);
        if (fs.existsSync(filePath)) {
          allFiles.push(filePath);
        }
      }
    }
    return allFiles;
  }

  /**
   * Produces an path for the given datasetID (relative to the given base path
   * or to the default system configured data path).
   */
  getDatasetDocPath(datasetID: string): string {
    const dataset = this.getDatasetById(datasetID);
    return dataset.datasetDocPath;
  }

  /**
   * Produces an file URI (which starts with file://) for the given datasetID.
   */
  getDatasetDocURI(datasetID: string): string {
    const datasetDocPath = this.getDatasetDocPath(datasetID);
    return formatAsFileURI(datasetDocPath);
  }

  getDatasets(): Dataset[] {
    return this.datasets;
  }

  getProblems(): Problem[] {
    return this.problems;
  }

  getDatasetById(id: string): Dataset {
    if (this.datasetsIdx[id] === undefined) {
      const parts = id.split(DATAMART_MARK);
      if (parts.length > -1) {
        const datasetDocPath: string = path.join(DATASETS_INFO, parts[0], 'augmented', parts[1], 'datasetDoc.json');
        console.log('Loading from', datasetDocPath);
        if (fs.existsSync(datasetDocPath)) {
          return Dataset.readFromDatasetDocFile(datasetDocPath);
        }
      }
    }
    return this.datasetsIdx[id];
  }

  async getProblemsForDatasetId(datasetId: string): Promise<Problem[]> {
    const dataset = this.getDatasetById(datasetId);
    return this.problems.filter(p => p.getInputDatasetID() === dataset.about.datasetID);
  }
}
