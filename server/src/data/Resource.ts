import * as d3m from 'd3m/dist';
import * as path from 'path';
import { clone, cloneDeep } from 'lodash';
import { DataPoint } from './DataPoint';
import { Dataset } from './Dataset';
import { Column } from './Column';
import { DataLoader } from './readers/DataLoader';
import { CSVReader } from './readers/CSVReader';
import { TXTReader } from './readers/TXTReader';
import { FileRefReader } from './readers/FileRefReader';

export enum SampleMethod {
  TOP,
  RANDOM,
  BOTTOM,
}

export class Resource {

  resourceID: string;
  config: d3m.DataResource;
  private _reader: DataLoader;
  dataset: Dataset;
  data: DataPoint[];

  constructor(config: d3m.DataResource, dataset: Dataset) {
    this.resourceID = config.resID;
    this.config = config;
    this.dataset = dataset;
  }

  static fromDataPoints(data: DataPoint[], original: Resource, dataset: Dataset) {
    const dataResource = cloneDeep(original.config);
    dataResource.columns = data[0].columns;
    const resource =  new Resource(dataResource, dataset);
    resource.data = data;
    return resource;
  }

  static getStandAloneResource(
    dataPath: string,
    {columns, format, isCollection, resourceType}:
    {columns?: d3m.Column[], isCollection?: boolean, format?: {[mimeType: string]: string[]}, resourceType?: d3m.ResourceType} = {}) {
    if (format === undefined) {
      if (dataPath.endsWith('.csv')) {
        format = {'text/csv': ['csv']};
        isCollection = false;
        resourceType = d3m.ResourceType.table;
        if (!columns) {
          throw 'Columns are required for CSV';
        }
      } else {
        throw 'Could not identify the format of the resource';
      }
    }
    return new Resource({
      columns: columns,
      isCollection: isCollection,
      resFormat: format,
      resID: 'STANDALONE',
      resPath: dataPath,
      resType: resourceType,
    }, undefined);
  }

  async saveDataAsync() {
    const reader = this.getReader();
    await reader.writeData(this.data);
  }

  streamData(): DataPoint[] {
    const reader = this.getReader();
    return reader.stream();
  }

  async getSample(params: { size?: number; start?: number, method?: SampleMethod }) {
    const { size, method, start } = params;
    const samples: DataPoint[] = [];
    if (start) {
      const data = [...await this.loadData()];
      return data.slice(start, size + start);
    } else if (!method) {
      for await (const d of this.streamData()) {
        samples.push(d);
        if (size && size <= samples.length) {
          break;
        }
      }
    } else if (method === SampleMethod.RANDOM) {
      const data = [...await this.loadData()];
      if (data.length < size) {
        return data;
      }
      for (let i = 0; i <= size; i++) {
        const randomIndex = Math.floor(Math.random() * data.length);
        const selected = data.splice(randomIndex, 1)[0];
        samples.push(selected);
      }
    }
    return samples;
  }

  async loadData() {
    this.data = [];
    for await (const d of this.streamData()) {
      this.data.push(d);
    }
    return this.data;
  }

  /**
   * Returns the base path of this resource. The returned path is resolved
   * based on the datasetDoc.json path and `resPath` path value from
   * datasetDoc.json.
   */
  getPath() {
    if (!this.dataset) {
      return this.config.resPath;
    }
    return path.join(this.dataset.datasetDocPath, '..', this.config.resPath);
  }

  async getSize() {
    const data = await this.loadData();
    return data.length;
  }

  getColumnByName(name: string) {
    if (!this.config.columns) {
      return undefined;
    }
    const columnIndex = this.config.columns.findIndex(c => c.colName === name);
    if (columnIndex === -1) {
      return undefined;
    }
    return { index: columnIndex, column: this.config.columns[columnIndex]};
  }

  getReader() {
    if (!this._reader) {
      const formats = new Set(Object.keys(this.config.resFormat));
      if (formats.has('text/csv') && this.config.resType === 'table') {
        this._reader = new CSVReader({
          path: this.getPath(),
          columns: this.config.columns.map(
            c =>
              new Column({
                colIndex: +c.colIndex,
                colName: c.colName,
                role: c.role,
                colType: d3m.ColumnType[c.colType],
                colDescription: c.colDescription,
                refersTo: c.refersTo,
              }),
          ),
        });
      } else if (formats.has('text/plain')) {
        this._reader = new TXTReader({
          path: this.getPath(),
        });
      } else {
        const basePath = `${this.dataset.datasetID}/resources/${this.config.resID}/file`;
        this._reader = new FileRefReader({
          path: this.getPath(),
          basePath,
        });
      }
    }
    return this._reader;
  }

  toJSON() {
    return clone(this.config);
  }

}
