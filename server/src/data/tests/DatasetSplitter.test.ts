import 'jest';
import { getDataSourceConfig, getDatasetSplitter } from '../config';
import { DataSource } from '../DataSource';
import { Dataset } from '../Dataset';
import { removeFileURLPrefix } from '../../utils';
import { ProblemDescription, Problem, TaskKeyword, PerformanceMetric, ProblemPerformanceMetric, ProblemInput, ProblemTarget } from 'ta3ta2-api-ts/problem_pb';

function init() {
  return new DataSource(getDataSourceConfig());
}

describe('Split datasets into training and testing parts', () => {

  it('should split the 185_baseball_dataset dataset', async () => {
    // given
    const store = init();
    const datasetId = '185_baseball_dataset';
    const splitter = getDatasetSplitter();
    const dataset: Dataset = store.getDatasetById(datasetId);

    const ppm = new ProblemPerformanceMetric();
    ppm.setMetric(PerformanceMetric.F1);

    const p = new Problem();
    p.setTaskKeywordsList([TaskKeyword.CLASSIFICATION, TaskKeyword.MULTICLASS]);
    p.setPerformanceMetricsList([ppm]);

    const pd = new ProblemDescription();
    pd.setId('185_baseball_problem');
    pd.setProblem(p);

    const target = new ProblemTarget();
    target.setTargetIndex(0);
    target.setColumnIndex(18);
    target.setColumnName('Hall_of_Fame');
    target.setResourceId('0');

    const input = new ProblemInput();
    input.setDatasetId(datasetId);
    input.setTargetsList([target]);

    pd.setInputsList([input]);

    // when
    const split = await splitter.getSplitsForDataset(dataset, pd);

    // then
    expect(split).toBeDefined();
    expect(split.training_dataset_uri).toBeDefined();
    expect(split.testing_dataset_uri).toBeDefined();

    const training = Dataset.readFromDatasetDocFile(removeFileURLPrefix(split.training_dataset_uri));
    expect(training.datasetID).toEqual(datasetId);
    expect(training.about.digest).toBeUndefined();

    const testing = Dataset.readFromDatasetDocFile(removeFileURLPrefix(split.training_dataset_uri));
    expect(testing.datasetID).toEqual(datasetId);
    expect(training.about.digest).toBeUndefined();
  });

});
