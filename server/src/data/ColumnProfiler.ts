import * as d3m from 'd3m/dist';
import { each } from 'lodash';
import { histogram } from 'd3-array';
import { ResourceType, DataPointValue } from 'd3m/dist';
import { Dictionary } from './preprocessing/TextProcessing';
import { DataPoint } from './DataPoint';
import { Column } from './Column';
import { Resource } from './Resource';

const TEXT_NUM_TOKENS = 5;
const DEFAULT_NUM_BINS = 20;
const numericTypes = new Set(['integer', 'real']);
const DEFAULT_TICK_PRECISION = 6;

/**
 * Removes small floating point error and keep the numeric value at a fixed precision.
 * This is to ensure that chart ticks are nicely displayed.
 */
const removeSmallError = (v: number): number => {
  return +v.toFixed(DEFAULT_TICK_PRECISION);
};

export type PrimitiveValue = string | number | Date;
export interface ColumnProfile {
  column: Column;
  values: {
    name: PrimitiveValue;
    count: number;
  }[];
  valuesIndex?: { [name: string]: number };
  binSize: number;
  isText?: boolean;
  stats: {
    count: number;
    distinctValueCount?: number;
    sum?: number;
    min?: number;
    max?: number;
    mean?: number;
    stdDev?: number;
  };
}

export class ColumnProfiler {

  async profile(resource: Resource): Promise<ColumnProfile[]> {
    const numBins = DEFAULT_NUM_BINS;

    if (!resource.config.columns) {
      if (resource.config.resType === ResourceType.text) {
        const data = await resource.getSample({ size: 1000 });
        const [numDocuments, summary] = await this.summarizeTextData(data);
        const ColumnConfig: d3m.Column = {
          colIndex: 0,
          colName: 'Text',
          colDescription: '',
          colType: d3m.ColumnType.string,
          role: [d3m.ColumnRole.attribute],
        };
        const columnProfile: ColumnProfile = {
          column: new Column(ColumnConfig),
          values: summary,
          binSize: -1,
          stats: {
            count: numDocuments,
          },
        };
        columnProfile.values = summary;
        return [columnProfile];
      }
    }
    const columnsStats: { [name: string]: ColumnProfile } = resource.config.columns.reduce((prev, c) => {
      prev[c.colIndex] = {
        column: c,
        values: [],
        valuesIndex: {},
        stats: {
          count: 0,
          distinctValueCount: 0,
          sum: 0,
          mean: 0,
          stdDev: 0,
          min: Infinity,
          max: -Infinity,
        },
      };
      return prev;
    }, {});

    const dictionaries: { [name: string]: Dictionary } = {};
    // Compute initial stats
    for await (const record of resource.streamData()) {
      record.columns.forEach((column, idx) => {
        let value = record.values[idx];
        const colIndex = column.config.colIndex;
        const columnInfo = columnsStats[colIndex];
        const stats = columnInfo.stats;
        if (numericTypes.has(column.config.colType)) {
          value = +value;
          if (!isNaN(value)) {
            stats.min = Math.min(stats.min, value);
            stats.max = Math.max(stats.max, value);
            stats.sum += value;
          }
        } else if (column.config.colType === d3m.ColumnType.dateTime) {
          value = new Date(value as string).getTime();
          if (!isNaN(value)) {
            stats.min = Math.min(stats.min, value);
            stats.max = Math.max(stats.max, value);
            stats.sum += value;
          }
        } else if (column.config.colType === d3m.ColumnType.string) {
          let dictionary: Dictionary;
          if (dictionaries[colIndex]) {
            dictionary = dictionaries[colIndex];
          } else {
            dictionary = new Dictionary();
            dictionaries[colIndex] = dictionary;
          }
          dictionary.addDocument(value.toString());
        } else {
          let info = columnInfo.valuesIndex[value.toString()] || 0;
          info += 1;
          columnInfo.valuesIndex[value.toString()] = info;
        }
        stats.count += 1;
      });
    }

    each(columnsStats, column => {
      if (numericTypes.has(column.column.colType) || column.column.colType === d3m.ColumnType.dateTime) {
        const stats = column.stats;
        // Use d3 histogram to generate human-readable bins.
        const bins = histogram()
          .domain([stats.min, stats.max])
          .thresholds(numBins)([]); // pass an empty array just to generate the bin coordinates
        if (!bins.length) {
          console.warn('empty numeric column');
          column.values = [];
          column.binSize = 0;
          stats.mean = NaN;
          return;
        }

        // Avoid using the first or last bin to estimate bin size.
        column.binSize = bins.length >= 3 ? bins[2].x0 - bins[1].x0 : bins[0].x1 - bins[0].x0;
        column.binSize = removeSmallError(column.binSize);

        // The first and last bin may have uneven size.
        // We must make them even otherwise recharts bars may not work properly (bar width extremely thin).
        bins[0].x0 = bins[0].x1 - column.binSize;
        bins[bins.length - 1].x1 = bins[bins.length - 1].x0 + column.binSize;
        column.values = bins.map(b => ({ name: removeSmallError(b.x0), count: 0 }));
        stats.mean = stats.sum / stats.count;
      }
    });

    for await (const record of resource.streamData()) {
      record.columns.forEach((column, idx) => {
        const value =
          column.config.colType === d3m.ColumnType.dateTime
            ? new Date(record.values[idx].toString()).getTime()
            : record.values[idx];
        const colIndex = column.config.colIndex;
        const stats = columnsStats[colIndex];
        stats.stats.stdDev += Math.pow(+value - stats.stats.mean, 2);
        if (numericTypes.has(column.config.colType) || column.config.colType === d3m.ColumnType.dateTime) {
          const binIndex = this.findBinIndex(value, stats.values);
          if (binIndex >= 0) {
            stats.values[binIndex].count++;
          }
        }
      });
    }

    return Object.keys(columnsStats).map(k => {
      const column = columnsStats[k];
      if (numericTypes.has(column.column.colType) || column.column.colType === d3m.ColumnType.dateTime) {
        column.stats.stdDev = Math.sqrt(column.stats.stdDev / column.stats.count);
      } else if (dictionaries[k]) {
        let values = [];
        if (dictionaries[k].maxDocLength > TEXT_NUM_TOKENS) {
          values = dictionaries[k].getTopTokens();
          column.isText = true;
        } else {
          values = dictionaries[k].getTokenList();
        }

        column.values = values.map(t => ({
          name: t.token,
          count: t.docFrequency,
        }));
        column.stats.distinctValueCount = dictionaries[k].numDocuments;
      } else {
        column.values = Object.keys(column.valuesIndex).map(ck => ({
          name: ck,
          count: column.valuesIndex[ck],
        }));
        column.stats.distinctValueCount = column.values.length;
      }
      delete column.valuesIndex;
      return column;
    });
  }

  async summarizeTextData(texts: DataPoint[]): Promise<[number, { name: PrimitiveValue; count: number }[]]> {
    const dictionary = new Dictionary();
    for await (const d of texts) {
      dictionary.addDocument(d.getValue('content').toString());
    }
    const topTerms = dictionary.getTopTokens();
    return [
      dictionary.numDocuments,
      topTerms.map(t => ({
        name: t.token,
        count: t.docFrequency,
      })),
    ];
  }

  /**
   * Executes a binary search to find index of the bin which the given
   * value belongs.
   */
  findBinIndex(
    value: DataPointValue,
    bins: {name: PrimitiveValue, count: number}[],
  ): number {
    let li = 0;
    let ri = bins.length - 1;
    while (li <= ri) {
      const mi = (li + ri) >> 1;
      if (bins[mi].name <= value) {
        li = mi + 1;
      } else {
        ri = mi - 1;
      }
    }
    return ri;
  }
}
