import * as fs from 'fs';
import { clone } from 'lodash';
import * as d3m from 'd3m/dist';
import { DataSource } from './DataSource';
import { Dataset } from './Dataset';

export class Problem {

  private config: d3m.Problem;
  private dataSource: DataSource;
  private _dataset: Dataset;

  constructor(config: d3m.Problem, dataSource?: DataSource) {
    this.config = config;
    this.dataSource = dataSource;
  }

  static readFromProblemDocFile(filePath: string) {
    const problemDoc: string = fs.readFileSync(filePath, 'utf8');
    return new Problem(JSON.parse(problemDoc) as d3m.Problem);
  }

  getInputDatasetID(): string {
    return this.config.inputs.data[0].datasetID;
  }

  get dataset() {
    if (!this._dataset) {
      this._dataset = this.dataSource.getDatasetById(this.config.inputs.data[0].datasetID);
    }
    return this._dataset;
  }

  get target() {
    return this.config.inputs.data[0].targets[0];
  }

  toJSON(): d3m.Problem {
    return clone(this.config);
  }
}
