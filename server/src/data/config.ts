import * as path from 'path';
import { DataSourceConfig, DataSource } from './DataSource';
import { INPUT_DIR, OUTPUT_DIR, D3MPROBLEMPATH, USER_INPUT_DIR } from '../env';
import { DatasetSplitter } from './DatasetSplitter';

export const getDataSourceConfig = (): DataSourceConfig => {
  return {
    inputPaths: [path.resolve(USER_INPUT_DIR), path.resolve(INPUT_DIR)],
    problemPath: D3MPROBLEMPATH,
  };
};

/*
 * A singleton DataSource instance to avoid re-scanning the data directory on
 * every request.
 */
const DATA_SOURCE = new DataSource(getDataSourceConfig());

/**
 * Returns a singleton data source instance using th current data source configuration.
 */
export const getDataSource = (): DataSource => {
  return DATA_SOURCE;
};

/*
 * A singleton DatasetSplitter instance used to create dataset splits and hold
 * references to the created files to cache dataset splits.
 */
const DATASET_SPLITTER = new DatasetSplitter();

/**
 * Returns a singleton dataset splitter instance.
 */
export const getDatasetSplitter = (): DatasetSplitter => {
  return DATASET_SPLITTER;
};

export interface ConfigJson {
  temp_storage_root: string;
  pipeline_logs_root: string;
  executables_root: string;
  dataset_schema?: string;
  problem_schema?: string;
  training_data_root?: string;
  problem_root?: string;
  user_problems_root?: string;
  timeout?: string; // "10"
  cpus?: string; // "2"
  ram?: string; // "5Gi"
}

export function getProblemsDirectory(): string {
  return path.join(OUTPUT_DIR, 'problems');
}
