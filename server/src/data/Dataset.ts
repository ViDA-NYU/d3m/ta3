import * as fs from 'fs';
import * as path from 'path';
import * as d3m from 'd3m/dist';
import { Resource } from './Resource';
import { cloneDeep } from 'lodash';
import { DataPoint } from './DataPoint';
import { mkdirIfAbsent } from '../utils';

export class Dataset {

  datasetDocPath: string;
  private _config: d3m.Dataset;
  private _dataResources: Resource[];

  constructor(config: d3m.Dataset, datasetDocPath: string) {
    this._config = config;
    this.datasetDocPath = datasetDocPath;
  }

  static readFromDatasetDocFile(filePath: string) {
    const datasetDoc = JSON.parse(
      fs.readFileSync(filePath, 'utf8'),
    ) as d3m.Dataset;
    return new Dataset(datasetDoc, filePath);
  }

  get datasetID() {
    return this.config.about.datasetID;
  }

  get about() {
    return this.config.about;
  }

  get qualities() {
    return this.config.qualities;
  }

  get dataResources() {
    if (!this._dataResources) {
      this._dataResources = this.config.dataResources.map(r => new Resource(r, this));
    }
    return this._dataResources;
  }

  get config() {
    if (!this._config) {
      this._config = JSON.parse(
        fs.readFileSync(this.datasetDocPath, 'utf8'),
      ) as d3m.Dataset;
    }
    return this._config;
  }

  set config(value) {
    this._config = value;
  }

  async copyToOverwritingLearningData(data: DataPoint[], newDatasetDirectoryPath: string, copyFiles: boolean = false) {

    const datasetDocPath = path.join(newDatasetDirectoryPath, 'datasetDoc.json');

    const doc = cloneDeep(this.config);
    // we erase digest hash since we are modifying the data
    doc.about.digest = undefined;

    const newDataset = new Dataset(doc, datasetDocPath);
    newDataset.clearResources();

    for (const resource of this.dataResources) {
      if (resource.config.resID === 'learningData') {
        const newResource = Resource.fromDataPoints(data, resource, newDataset);
        newDataset.addResource(newResource);
        await newResource.saveDataAsync();
      } else {
        if (copyFiles) {
          // TODO: We currently don't handle table resources (), we are coping
          // the entire csv files. Other resource resources types will be
          // copied on demand later based on the rows of the learningData.
          // Ideally, for CSV files we would copy only CSV lines related to the
          // learningData as well.
          newDataset.addResource(cloneDeep(resource));
          if (resource.config.resType === d3m.ResourceType.table && resource.config.resFormat['text/csv']) {
            const resPath = resource.config.resPath;
            const srcPath = path.join(this.datasetDocPath, '..', resPath);
            const destPath = path.join(newDataset.datasetDocPath, '..', resPath);
            mkdirIfAbsent(path.join(newDataset.datasetDocPath, '..', resPath));
            console.log(`Copying file from ${srcPath} to ${destPath}`);
            fs.copyFileSync(srcPath, destPath);
          }
        } else {
          const newResource = cloneDeep(resource);
          newResource.config.resPath = resource.getPath();
          console.log(`Resource path: ${resource.getPath()}`);
          newDataset.addResource(newResource);
        }
      }
      if (copyFiles) {
        if (resource.config.resType === d3m.ResourceType.table && resource.config.resFormat['text/csv']) {
          if (resource.config.columns) {
            for (const column of resource.config.columns) {
              if (column.refersTo && column.refersTo.resObject === d3m.ResourceLinkObject.item) {
                console.log(`Found a column that references files: ${column.colName}`);
                // this columns is a reference for file objects from another
                // resource we need to copy the referenced files
                const referencedResId = column.refersTo.resID;
                const referencedResource = this.dataResources
                  .find(r => r.config.resID === referencedResId).config.resPath;
                // copy every referenced file to the new location
                mkdirIfAbsent(path.join(newDataset.datasetDocPath, '..', referencedResource));
                console.log('Copying resource files:');
                for (const entry of data) {
                  const filename = entry.values[column.colIndex] as string;
                  const src = path.join(this.datasetDocPath, '..', referencedResource, filename);
                  const dest = path.join(newDataset.datasetDocPath, '..', referencedResource, filename);
                  console.log(` - Copying ${filename}`);
                  fs.copyFileSync(src, dest);
                }
              }
            }
          }
        }
      }
    }

    console.log(`Writing datasetDoc ${datasetDocPath}...`);
    mkdirIfAbsent(datasetDocPath);
    fs.writeFileSync(datasetDocPath, JSON.stringify(newDataset.config, undefined, 2));

    return newDataset;
  }

  private clearResources() {
    this._dataResources = undefined;
    this.config.dataResources = [];
    return this;
  }

  private addResource(resource: Resource) {
    this._dataResources = this._dataResources || [];
    this._dataResources.push(resource);
    this.config.dataResources.push(resource.toJSON());
  }

  getResource(resourceId: string) {
    return this.dataResources.find(r => r.resourceID === resourceId);
  }

  getMainResource() {
    return this.getResource('learningData') || this.dataResources.find(resource =>
      resource.getPath().endsWith('learningData.csv'),
    );
  }

  toJSON(
    template: { about: boolean; dataResources: boolean; problems: boolean, qualities: boolean } = {
      about: true,
      qualities: true,
      dataResources: true,
      problems: false,
    },
  ) {
    const result = {};
    for (const key in template) {
      if (template[key]) {
        result[key] = this[key];
      }
    }
    return result;
  }

}
