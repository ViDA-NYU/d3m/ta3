import * as path from 'path';
import * as d3m from 'd3m/dist';
import { OUTPUT_DIR } from '../env';
import { Dataset } from './Dataset';
import { DataPoint } from './DataPoint';
import { Resource } from './Resource';
import { shuffle } from 'lodash';
import { formatAsFileURI } from '../api/utils';
import { ProblemDescription, TaskKeyword } from 'ta3ta2-api-ts/problem_pb';
import { ColumnRole } from 'd3m/dist';

const DEFAULT_DATASET_SPLITS_PATH = path.join(OUTPUT_DIR, 'dataset_splits');
export const DEFAULT_TEST_SPLIT_PROPORTION = 0.25;

interface DatasetSplits {
  training_dataset_uri: string;
  testing_dataset_uri: string;
}

class DatasetSplitter {
  splitsIndex: { [datasetID: string]: Promise<DatasetSplits> } = {};
  splitsPath: string;

  constructor(splitsPath?: string) {
    this.splitsPath = splitsPath || DEFAULT_DATASET_SPLITS_PATH;
  }

  async getSplitsForDataset(dataset: Dataset, problem: ProblemDescription) {
    const datasetID = dataset.datasetID;
    if (!this.splitsIndex[datasetID]) {
      this.splitsIndex[datasetID] = this.generateSplits(dataset, problem);
    }
    return this.splitsIndex[datasetID];
  }

  private generateSplits(dataset: Dataset, problem: ProblemDescription): Promise<DatasetSplits> {
    return new Promise(async (resolve, reject) => {
      console.log('\n> Generating data splits for ' + dataset.datasetDocPath);
      const dr = dataset.dataResources.find(d => d.config.resID === 'learningData');
      if (!dr) {
        throw new Error('No learningData resource found in the dataset');
      }

      const taskKeywords = problem.getProblem().getTaskKeywordsList();
      if (
        (taskKeywords.includes(TaskKeyword.CLASSIFICATION) ||
          taskKeywords.includes(TaskKeyword.REGRESSION) ||
          taskKeywords.includes(TaskKeyword.FORECASTING) ||
          taskKeywords.includes(TaskKeyword.COLLABORATIVE_FILTERING)) &&
        dr.config.resType === d3m.ResourceType.table
      ) {
        try {
          resolve(await this.splitDataset(dataset, problem));
        } catch (error) {
          console.error(`Failed to split dataset: ${dataset.datasetID}.`, error);
          reject(error);
        }
      } else {
        // When the dataset has more than one resource or the resource is not a table
        // we return the original dataset as training and testing.
        resolve({
          training_dataset_uri: formatAsFileURI(dataset.datasetDocPath),
          testing_dataset_uri: formatAsFileURI(dataset.datasetDocPath),
        });
      }
    });
  }

  private async splitDataset(dataset: Dataset, problem: ProblemDescription) {
    // Load original data
    const mainResource = dataset.getMainResource();
    const data = [...await mainResource.loadData()];
    const {testingData, trainingData} = this.splitDataPoints(data, mainResource, problem);

    const trainingPath = path.join(this.splitsPath, dataset.datasetID, 'TRAIN', 'dataset_TRAIN');
    const testingPath = path.join(this.splitsPath, dataset.datasetID, 'TEST', 'dataset_TEST');

    // Copy training data datasetDoc.json to its path
    const trainDataset = await dataset.copyToOverwritingLearningData(trainingData, trainingPath);

    // Copy test data datasetDoc.json to its path
    const testDataset = await dataset.copyToOverwritingLearningData(testingData, testingPath);

    console.log(
      `Split input data:\n` +
        `- Train split size: ${trainingData.length} (${trainDataset.datasetDocPath})\n` +
        `- Test split size: ${testingData.length} (${testDataset.datasetDocPath})`,
    );

    return {
      training_dataset_uri: formatAsFileURI(trainDataset.datasetDocPath),
      testing_dataset_uri: formatAsFileURI(testDataset.datasetDocPath),
    };
  }

  private splitDataPoints(data: DataPoint[], mainResource: Resource, problem: ProblemDescription) {
    const taskKeywords = problem.getProblem().getTaskKeywordsList();
    if (taskKeywords.includes(TaskKeyword.CLASSIFICATION)) {
      console.info('Spliting dataset using Stratified Random Sampling...');
      const target = problem
        .getInputsList()[0]
        .getTargetsList()[0]
        .getColumnName();
      return this.stratifiedRandomSampling(data, target);
    }
    if (taskKeywords.includes(TaskKeyword.FORECASTING)) {
      console.info('Spliting dataset using temporal information...');
      return this.temporalCutoffSplit(data, mainResource, problem);
    } else {
      console.info('Spliting dataset using Random Sampling...');
      return this.simpleRandomSampling(data);
    }
  }

  /**
   * Randomly splits the data into train/test with sizes proportional to TEST_SPLIT_PROPORTION.
   */
  private simpleRandomSampling(data: DataPoint[]) {
    const testingData = [];
    let trainingData = [];
    const testingCount = Math.ceil(data.length * DEFAULT_TEST_SPLIT_PROPORTION);
    while (testingData.length < testingCount) {
      const randomIndex = Math.floor(Math.random() * data.length);
      const selected = data.splice(randomIndex, 1)[0];
      testingData.push(selected);
    }
    trainingData = data;
    return { trainingData, testingData };
  }

  /**
   * Randomly splits the data into train/test with sizes proportional to TEST_SPLIT_PROPORTION
   * while still keeping similar proportion of classes in each data split.
   */
  private stratifiedRandomSampling(data: DataPoint[], target: string) {
    const targetIndex = data[0].columns.findIndex(c => c.colName === target);
    const indexTarget: number = targetIndex;
    const trainingData: DataPoint[] = [];
    const testingData: DataPoint[] = [];
    const groups = {};
    data.forEach((d, idx) => {
      const label = d.values[indexTarget].toString();
      (groups[label] = groups[label] || []).push(idx);
    });
    Object.keys(groups).forEach(label => {
      const shuffledGroupIndex: number[] = shuffle(groups[label]);
      const testingCountGroup = Math.ceil(shuffledGroupIndex.length * DEFAULT_TEST_SPLIT_PROPORTION);
      for (let i = 0; i < testingCountGroup; i++) {
        testingData.push(data[shuffledGroupIndex[i]]);
      }
      for (let i = testingCountGroup; i < shuffledGroupIndex.length; i++) {
        trainingData.push(data[shuffledGroupIndex[i]]);
      }
    });
    return { trainingData, testingData };
  }

  private temporalCutoffSplit(data: DataPoint[], mainResource: Resource, problem: ProblemDescription) {
    const columns = mainResource.config.columns;
    let forecastingColumnIdx: number = undefined;
    for (const column of columns) {
      if (column.role.includes(ColumnRole.timeIndicator)) {
        forecastingColumnIdx = column.colIndex;
      }
    }
    data.sort((a, b) => (a.values[forecastingColumnIdx] as number) - (b.values[forecastingColumnIdx] as number));
    const testingCount = Math.ceil(data.length * DEFAULT_TEST_SPLIT_PROPORTION);
    const trainingCount = data.length - testingCount;
    const trainingData = data.splice(0, trainingCount);
    const testingData = data;
    return { trainingData, testingData };
  }

}

export { DatasetSplitter };
