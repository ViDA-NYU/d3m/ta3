import { DataLoader } from './DataLoader';
import { DataPoint } from '../DataPoint';
import * as fs from 'fs';
import * as path from 'path';
import { Column } from '../Column';
import * as d3m from 'd3m/dist';

export class TXTReader extends DataLoader {
  path: string;
  columns: Column[];
  constructor(config: { path: string }) {
    super();
    this.path = config.path;
    this.columns = [
        new Column({
            colIndex: 0,
            colName: 'name',
            role: [],
            colType: d3m.ColumnType.categorical,
            colDescription: 'File Name',
        }),
        new Column({
            colIndex: 1,
            colName: 'content',
            role: [],
            colType: d3m.ColumnType.string,
            colDescription: 'Text file contents',
        }),
    ];
  }
  writeData(data: DataPoint[]) {

  }
  *stream() {
    const files = fs.readdirSync(this.path);
    for (const file of files) {
        const fullPath = path.join(this.path, file);
        const content = fs.readFileSync(fullPath, {encoding: 'utf-8'});
        yield new DataPoint([file, content], this.columns);
    }
  }
}
