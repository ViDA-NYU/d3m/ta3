import { WordTokenizer, Tokenizer } from 'natural';
import stopwords from './stopwrods';
interface TokenInfo {
  docFrequency: number;
  termFrequency: number;
}

interface DictionaryDict {
  [token: string]: TokenInfo;
}

export class Dictionary {
  private dictionary: DictionaryDict;
  private tokenizer: Tokenizer;
  private _numdocuments: number;
  private _maxDocLength: number;
  constructor() {
    this.dictionary = {};
    this.tokenizer = new WordTokenizer();
    this._numdocuments = 0;
    this._maxDocLength = 0;
  }

  get numDocuments() {
    return this._numdocuments;
  }

  get maxDocLength() {
    return this._maxDocLength;
  }

  addDocument(text: string) {
    this._numdocuments += 1;
    const tokens = this.tokenizer.tokenize(text.toLowerCase())
      .filter(t => !stopwords.has(t));
    this._maxDocLength = Math.max(this._maxDocLength, tokens.length);
    const tokensCount = tokens.reduce((prev, curr) => {
      prev[curr] = prev[curr] ? prev[curr] + 1 : 1;
      return prev;
    }, {});

    for (const token of Object.keys(tokensCount)) {
      const tokenState: TokenInfo = this.dictionary[token] || {
        docFrequency: 0,
        termFrequency: 0,
      };
      tokenState.docFrequency += 1;
      tokenState.termFrequency = tokensCount[token];
      this.dictionary[token] = tokenState;
    }
  }

  getTokenList(): Array<{ token: string } & TokenInfo> {
    return Object.keys(this.dictionary).map(k => ({
      token: k,
      ...this.dictionary[k],
    }));
  }

  getTopTokens(n = 50) {
    const tokenList = this.getTokenList();
    return tokenList.sort((a, b) =>  b.docFrequency - a.docFrequency).slice(0, n);
  }
}
