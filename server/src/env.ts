import * as path from 'path';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { normalizeUrl } from './utils';

dotenv.config({ path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env' });

export const NODE_ENV = process.env.NODE_ENV || 'development';

export const D3MPROBLEMPATH = process.env.D3MPROBLEMPATH;

export const OUTPUT_DIR = process.env.OUTPUT_DIR || process.env.D3MOUTPUTDIR || '/output';

export const INPUT_DIR = process.env.D3M_DATA_PATH || process.env.D3MINPUTDIR || '/input';

export const USER_INPUT_DIR = process.env.USER_INPUT_DIR || `${OUTPUT_DIR}/user_datasets`;

export const TA3_STORAGE = `${OUTPUT_DIR}/ta3`;

export const TMP_DIR = `${TA3_STORAGE}/tmp`;

export const PORT = process.env.PORT || '3000';

// When this is enabled, the system will use the scores returned in the SearchSolutions as final
// scores instead of dispatching another call to ScoreSolutions for computing actual scores.
export const USE_SCORES_FROM_SEARCH_SOLUTIONS = process.env.USE_SCORES_FROM_SEARCH_SOLUTIONS || false;

export const ENABLE_RECORDER = process.env.ENABLE_RECORDER;

export const GRPC_PORT = process.env.GRPC_PORT || '';
export const GRPC_HOST = process.env.GRPC_HOST;
export const GRPC_CONN_URL = GRPC_HOST + ':' + GRPC_PORT;

export const PYTHON_ROOT = process.env.PYTHON_ROOT || process.env.RULEMATRIX_ROOT || '/ta3/rulematrix';

export const DATAMART_URL =
  normalizeUrl(process.env.DATAMART_URL) ||
  normalizeUrl(process.env.DATAMART_NYU_URL) ||
  normalizeUrl(process.env.DATAMART_URL_NYU) ||
  'https://datamart.d3m.vida-nyu.org';

export const DATASETS_INFO = path.join(OUTPUT_DIR, 'datasets_info');
export const DATAMART_MARK = '__TA3DMKTAUG__';

const DEFAULT_ALLOW_ORIGINS = NODE_ENV === 'development' ? 'http://localhost:5000' : '';

export const ALLOW_ORIGINS = (process.env.ALLOW_ORIGINS || DEFAULT_ALLOW_ORIGINS).split(';');

export const checkResources = (): boolean => {
  let passCheck = true;
  if (!OUTPUT_DIR || !fs.existsSync(OUTPUT_DIR)) {
    console.error(
      `Environment variable $OUTPUT_DIR=["${OUTPUT_DIR}"] is not set or the output directory does not exist.`,
    );
    passCheck = false;
  }
  const outputDirStats = fs.lstatSync(OUTPUT_DIR);
  if (!outputDirStats.isDirectory()) {
    console.error(`Environment variable $OUTPUT_DIR=["${OUTPUT_DIR}"] points to a file, not a directory.`);
    passCheck = false;
  }
  return passCheck;
};

export const tempStorageRoot = () => {
  return OUTPUT_DIR + '/storage';
};
