import * as path from 'path';
import * as fs from 'fs';
import * as util from 'util';

const FILE_PREFIX_LENGTH = 'file://'.length;

export function fileExists(filePath: string) {
  return fs.existsSync(filePath) && fs.lstatSync(filePath).isFile();
}

export function directoryExists(dirPath: string) {
  return fs.existsSync(dirPath) && fs.lstatSync(dirPath).isDirectory();
}

export function hasFileExtension(filePath: string) {
  return filePath.split('/').pop().indexOf('.') > -1;
}

export function mkdirIfAbsent(filePath: string) {
  const dir = hasFileExtension(filePath) ? path.dirname(filePath) : filePath;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }
}

export function removeFileURLPrefix(fileName: string) {
  return fileName.startsWith('file://') ? fileName.slice(FILE_PREFIX_LENGTH) : fileName;
}

export function normalizeUrl(url: string) {
  return (url && url.endsWith('/')) ? url.slice(0, url.length - 1) : url;
}

export let circularStringify =  (obj: Object) => {
  const cache: object[] = [];
  return JSON.stringify(obj, function(key, value) {
    if (typeof value === 'object' && value !== null) {
      if (cache.indexOf(value) !== -1) {
        // Duplicate reference found
        try {
          // If this value does not reference a parent it can be deduped
          return JSON.parse(JSON.stringify(value));
        } catch (error) {
          // discard key if value cannot be deduped
          return;
        }
      }
      // Store value in our collection
      cache.push(value);
    }
    return value;
  });
};

export function toDebugString(obj: object) {
  return util.inspect(obj, {showHidden: false, depth: Infinity, colors: true});
}

export function padInt(value: number, width: number) {
  const fillChar = '0';
  const valueAsString = value + '';
  if (valueAsString.length >= width) {
    return valueAsString;
  } else {
    return new Array(width - valueAsString.length + 1).join(fillChar) + valueAsString;
  }
}

export class SeqIdCreator {

  solutionsSeq = {};

  /**
   * Creates a sequential number for the given ID.
   * @param id a unique ID
   */
  getOrCreateNumberId(id: string): number {
    if (!this.solutionsSeq[id]) {
      this.solutionsSeq[id] = Object.keys(this.solutionsSeq).length + 1;
    }
    return this.solutionsSeq[id];
  }

  /**
   * Creates a sequential string ID for the given unique ID.
   * @param id a unique ID
   */
  getOrCreateSeqId(id: string, width: number) {
    return `#${padInt(this.getOrCreateNumberId(id), width)}`;
  }

}
