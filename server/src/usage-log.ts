import * as fs from 'fs';
import * as path from 'path';
import * as stringify from 'csv-stringify/lib/sync';
import { OUTPUT_DIR } from './env';
import { mkdirIfAbsent } from './utils';
import * as d3m from 'd3m/dist';

const LOG_PATH = path.join(OUTPUT_DIR, './usage-logs.log');

const eventMapping: {
  [event: string]: {
    type: string,
    L1: string,
    L2: string,
  },
} = {};

// Data Preparation
eventMapping[d3m.UserEvent.LIST_DATASETS] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_OPEN,
};
eventMapping[d3m.UserEvent.LIST_PROBLEMS] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_OPEN,
};
eventMapping[d3m.UserEvent.GET_DATA_DESCRIPTION] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_OPEN,
};
eventMapping[d3m.UserEvent.VIEW_RESOURCE_STATS] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_EXPLORATION,
};
eventMapping[d3m.UserEvent.CHANGE_VIEW] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_EXPLORATION,
};
eventMapping[d3m.UserEvent.EXPLORE_SPREADSHEET] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_EXPLORATION,
};
eventMapping[d3m.UserEvent.DATAMART_SEARCH] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_SEARCH,
};
eventMapping[d3m.UserEvent.DATAMART_AUGMENT] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.DATA_PREPARATION,
  L2: d3m.ActivityTypeL2.DATA_AUGMENTATION,
};
// Problem Definition
eventMapping[d3m.UserEvent.SELECT_TARGET] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.PROBLEM_DEFINITION,
  L2: d3m.ActivityTypeL2.PROBLEM_SPECIFICATION,
};
eventMapping[d3m.UserEvent.SELECT_PROBLEM_TYPE] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.PROBLEM_DEFINITION,
  L2: d3m.ActivityTypeL2.PROBLEM_SPECIFICATION,
};
eventMapping[d3m.UserEvent.SELECT_PROBLEM_SUBTYPE] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.PROBLEM_DEFINITION,
  L2: d3m.ActivityTypeL2.PROBLEM_SPECIFICATION,
};
eventMapping[d3m.UserEvent.SETUP_SEARCH_CONFIGURATION] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.PROBLEM_DEFINITION,
  L2: d3m.ActivityTypeL2.PROBLEM_SPECIFICATION,
};
// Model Selection
eventMapping[d3m.UserEvent.RANK_UP_SOLUTION] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_COMPARISON,
};
eventMapping[d3m.UserEvent.RANK_DOWN_SOLUTION] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_COMPARISON,
};
eventMapping[d3m.UserEvent.SHOW_ONLY_FAVORITE] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_COMPARISON,
};
eventMapping[d3m.UserEvent.RESET_SOLUTIONS] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_COMPARISON,
};
eventMapping[d3m.UserEvent.SELECT_MODEL_GRAPH_VIEW] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_COMPARISON,
};
// Explain Solutions
eventMapping[d3m.UserEvent.GO_EXPLAIN_SOLUTIONS_VIEW] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[d3m.UserEvent.VIEW_CONFUSION_MATRIX] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[d3m.UserEvent.VIEW_RULE_MATRIX] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};

eventMapping[d3m.UserEvent.VIEW_CONFUSION_SCATTER_PLOT] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[d3m.UserEvent.VIEW_PARTIAL_DEPENDENCE_PLOTS] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[d3m.UserEvent.RANK_SOLUTION] = {
  type: d3m.UsageLogType.SYSTEM,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPORT,
};

// System Activity
eventMapping['SearchSolutions'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_SEARCH,
};
eventMapping['GetSearchSolutionsResults'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_SEARCH,
};
eventMapping['DescribeSolution'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_SUMMARIZATION,
};
eventMapping['ScoreSolution'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_SUMMARIZATION,
};
eventMapping['GetScoreSolutionResults'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_SUMMARIZATION,
};
eventMapping['FitSolution'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['GetFitSolutionResults'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['ProduceSolution'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['GetProduceSolutionResults'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['SolutionExport'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.MODEL_EXPORT,
};
eventMapping['Hello'] = {
  type: d3m.UsageLogType.TA23API,
  L1: d3m.ActivityType.MODEL_SELECTION,
  L2: d3m.ActivityTypeL2.LAUNCH_TA3,
};

class UsageLogger {

  logFile: fs.WriteStream;

  constructor(file: string) {
    mkdirIfAbsent(LOG_PATH);
    this.logFile = fs.createWriteStream(file);
    // Write column headers
    this.logFile.write('timestamp,feature_id,type,activity_l1,activity_l2,other\n');
  }

  logEvent(featureName: string, metadata?: object) {
    this.log(d3m.UsageLogType.SYSTEM, featureName, metadata);
  }

  logGrpcApi(method: string, metadata?: object) {
    this.log(d3m.UsageLogType.TA23API, method, metadata);
  }

  log(type: d3m.UsageLogType, featureId: string, metadata?: object) {
    const line = stringify([[
      new Date().toISOString(),
      featureId,
      type.toString(),
      eventMapping[featureId] ? eventMapping[featureId].L1 : 'OTHER_L1',
      eventMapping[featureId] ? eventMapping[featureId].L2 : 'OTHER_L2',
      JSON.stringify(metadata ? metadata : {}),
    ]]);
    this.logFile.write(line);
    process.nextTick(() => this.logFile.uncork()); // flush buffer
  }

}

export const usageLogger = new UsageLogger(LOG_PATH);
