export const LIGHT_NYU_PURPLE = '#c09bd8';
export const LIGHT_NYU_PURPLE_ALPHA_50 = '#c09bd880';
export const LIGHT_NYU_PURPLE_ALPHA_25 = '#c09bd840';

export const VIDA_GREEN = '#84be10';
