import React, { PureComponent } from 'react';
import { omit } from 'lodash';
import Loadable from './Loadable';
import { downloadObj, loadFile } from './utils';

interface BaseStoreState {}
export class DataStoreBase<T extends BaseStoreState> {
  persistOnChange: boolean;
  ignoreKeysOnPersist: string[];
  initialState: T;
  loadableKeys: Set<string>;

  protected state: T;
  private subscribers: ((store: DataStoreBase<T>) => unknown)[];

  constructor(initialState: T) {
    this.subscribers = [];
    this.initialState = initialState;
    this.state = initialState;
    this.persistOnChange = false;
    this.ignoreKeysOnPersist = [];
  }

  connectPropsToDatastore = <DP>(selector: (store: this, ownProps: Partial<DP> ) => Partial<DP>) => {
    const self = this;

    return <O>(SourceComponent: React.ComponentType) => {
      class StoreComponent extends PureComponent<Partial<DP>> {
        _reactInternalFiber: Object;
        handleUpdate = () => {
          this.forceUpdate();
        };
        componentDidMount() {
          self.subscribe(this.handleUpdate);
        }
        componentWillUnmount() {
          self.unsubscribe(this.handleUpdate);
        }

        // tslint:disable-next-line:no-any
        render(): React.ReactElement<any> {
          const ownProps: {} = this.props;
          const props = selector(self, ownProps);
          const keyProp = 'key';
          return React.createElement(
            SourceComponent,
            Object.assign(
              { key: this._reactInternalFiber[keyProp]},
              ownProps,
              props));
        }
      }
      return StoreComponent;
    };
  };

  setState = (state: Partial<T>) => {
    this.state = Object.assign({}, this.state, state);
    if (this.persistOnChange) {
      try {
        this.persist();
      } catch (err) {
        // TODO: try/catch is not a fix!
        // Temporarily just avoid breaking the system. See issue #91:
        // https://gitlab.com/ViDA-NYU/d3m/ta3/issues/91
        console.error('DataStoreBase.persist() failed. Veryfin', err);
      }
    }
    this.notify();
  };

  replaceState = (state: T) => {
    this.state = state;
    this.notify();
  };

  persist = (key = 'lastState') => {
    try {
    const lastState = JSON.stringify(omit(this.state, this.ignoreKeysOnPersist));
    sessionStorage.setItem(key, lastState);
    } catch (err) {
      console.warn('Error saving state', err);
    }
  };

  load = (key = 'lastState') => {
    let storedValue = sessionStorage.getItem(key);
    if (!storedValue) {
      console.warn(`State ${key} not found, ignoring command`);
    } else {
      this.deserializeState(storedValue);
    }
  };

  deserializeState = (storedValue: string) => {
    let state: T = JSON.parse(storedValue);
    for (let stateKey in state) {
      if (state.hasOwnProperty(stateKey)) {
          if (this.loadableKeys.has(stateKey)) {
          const value = Loadable.fromJSON(state[stateKey]);
          if (value.hasValue || value.error) {
            state[stateKey as string] = Loadable.fromJSON(state[stateKey]);
          } else {
            state[stateKey as string] = undefined;
          }
        }
      }
    }
    state = Object.assign({}, this.initialState, state);
    this.replaceState(state);
  }

  downloadState = () => {
    downloadObj(this.state);
  }

  loadStateFromFile = () => {
    loadFile((data) => {
      this.deserializeState(data);
    });
  }

  reset = () => {
    this.replaceState(this.initialState);
    this.persist();
  }

  resetValue = (key: keyof T) => {
    if (key) {
      const newState: Partial<T> = {};
      newState[key] = this.initialState[key];
      this.setState(newState);
    } else {
      this.replaceState(this.initialState);
    }
    this.persist();
  }

  subscribe(func: (store: DataStoreBase<T>) => unknown) {
    const exist = this.subscribers.find(d => func === d);
    if (!exist) {
      this.subscribers.push(func);
    }
  }
  unsubscribe(func: (store: DataStoreBase<T>) => unknown) {
    const index = this.subscribers.findIndex(d => func === d);
    if (index > -1) {
      this.subscribers.splice(index, 1);
    }
  }

  private notify() {
    for (let s of this.subscribers) {
      s(this);
    }
  }
}
