/**
 * Browser (chrome) has native WebSocket available with "WebSocket" or "window.WebSocket".
 * However in tests (jest) WebSocket is undefined, and we have to provide an alternative.
 * We use "typeof WebSocket !== 'undefined'" to check if the context has native WebSocket.
 * We have to tell the system to use this file to provide "ws". We added a "browser" keyword in package.json for that.
 * In tests, the original "ws" is used directly in socket.ts.
 * This solution is taken from https://github.com/websockets/ws/issues/1093.
 *
 * Other notes:
 *
 * - "ws" package has to run in nodejs environment and does not work in browsers.
 * Importing "ws" alone and using it directly in browsers will give mysterious http error "426 upgrade required".
 * The request is processed as http and the socket handshake fails.
 * https://github.com/websockets/ws/issues/790
 *
 * - We cannot use "ws" and the native WebSocket in a same module, which leads to a nasty error:
 * "Critical dependencies: the request of a dependency is an expression".
 */

let ws;

if (typeof WebSocket !== 'undefined') {
  ws = WebSocket;
} else {
  console.error('browser does not have native WebSocket');
}

module.exports = ws;
