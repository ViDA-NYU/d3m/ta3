import { DataStore, StoreState } from './DataStore';
import { ConfigurationType } from 'd3m/dist';

const storeInitialState: StoreState = {
  favoriteSolutions: [],
  removedSolutions: [],
  orderedSolutions: [],
  isDoneDefineProblem: false,
  problemDefinition: {},
  selectedConfigDefineProblem: ConfigurationType.DEFAULT,
  defaultProblemDefinition: {},
};

export const store = new DataStore(storeInitialState);
export const connectPropsToDatastore = store.connectPropsToDatastore;
export const downloadState = store.downloadState;
export const loadStateFromFile = store.loadStateFromFile;

// For debugging purposes ------------------------------

declare global {
  interface Window {
    __TA3STORE__: DataStore;
  }
}
store.load();
store.persistOnChange = true;
window.__TA3STORE__ = store;
