import React from 'react';
import styled from 'styled-components';
import { connectPropsToDatastore } from 'src/data/store';

const Container = styled.div`
    margin: auto auto;
    padding: 50px;
`;

interface ErrorBoundaryProps {
    fallback?: React.PureComponent;
    resetStore: () => void;
}

class ErrorBoundary extends React.Component<ErrorBoundaryProps, { hasError: boolean }> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error: Error, info: React.ErrorInfo) {
    // You can also log the error to an error reporting service
    console.log(error, info);
  }

  render() {
    if (this.state.hasError) {
      return (
        this.props.fallback || (
          <Container>
            <div>An unknow error happened.</div>
            <a href="#" onClick={() => {
              this.props.resetStore();
              document.location!.reload();
            }}>Reset application and try again.</a>
          </Container>
        )
      );
    }
    return this.props.children;
  }
}

export default connectPropsToDatastore((store) => ({resetStore: store.reset}))(ErrorBoundary);
