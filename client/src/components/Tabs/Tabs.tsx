import * as React from 'react';

class Tabs extends React.PureComponent {
  render() {
    return (
      <ul className={'nav nav-tabs'}>
        {this.props.children}
      </ul>
    );
  }
}

interface TabProps {
  onClick: ((event: React.MouseEvent<HTMLAnchorElement>) => void) | undefined;
  selected: boolean;
}

class Tab extends React.PureComponent<TabProps> {
  render() {
    const tabClassName = this.props.selected ? 'nav-link active' : 'nav-link';
    return (
      <li className="nav-item">
        <a className={tabClassName} onClick={this.props.onClick}>
          {this.props.children}
        </a>
      </li>
    );
  }
}

export { Tabs, Tab };
