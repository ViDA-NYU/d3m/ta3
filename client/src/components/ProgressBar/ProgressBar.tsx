import * as React from 'react';

interface ProgressBarProps {
  progress: number;
}

class ProgressBar extends React.PureComponent<ProgressBarProps> {
  render() {
    return (
      <div className="progress mb-2">
        <div
          className="progress-bar progress-bar-striped progress-bar-animated"
          role="progressbar"
          style={{ width: this.props.progress + '%' }}
        >
          {this.props.progress}%
        </div>
      </div>
    );
  }
}

export { ProgressBar };
