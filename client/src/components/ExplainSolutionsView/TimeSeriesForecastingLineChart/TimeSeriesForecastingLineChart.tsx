import React, { PureComponent } from 'react';
import { LineChart, Line, XAxis, YAxis, Legend, Tooltip, Brush, ResponsiveContainer } from 'recharts';
import { timeFormat } from 'd3-time-format';
import moment from 'moment';
import { extent } from 'd3-array';
import * as d3m from 'd3m/dist';

interface TimeSeriesForecastingLineChartProps {
  dataLoader: () => d3m.TimeSeriesForecastingLineChartData;
}

enum TSViewConfigurationType {
  SINGLE_CHART = 'SINGLE_CHART',
  DUAL_CHART = 'DUAL_CHART',
}

interface TimeSeriesForecastingLineChartState {
  tsMode: TSViewConfigurationType;
}

export default class TimeSeriesForecastingLineChart extends
  PureComponent<TimeSeriesForecastingLineChartProps, TimeSeriesForecastingLineChartState> {

  constructor(props: TimeSeriesForecastingLineChartProps) {
    super(props);
    this.state = {
      tsMode: TSViewConfigurationType.SINGLE_CHART,
    };
  }

  render() {
    const data = this.props.dataLoader();

    // recharts expects as data an array of objects
    const points = data.points.map(x => ({ 0: x[0], 1: x[1], 2: x[2] }));

    points.sort((a, b) =>
      moment(a[2]).valueOf() - moment(b[2]).valueOf());

    const domainPred = extent(points, x => x[0]);
    const domainTrue = extent(points, x => x[1]);
    const domainTotal: [number, number] = [Math.min(domainPred[0] || 0, domainTrue[0] || 0),
    Math.max(domainPred[1] || 0, domainTrue[1] || 0)];

    return (
      <div>
        <div className="form-check form-check-inline mb-4">
          <span className="mr-2 font-weight-bold">Chart Mode: </span>
          <label
            className="form-check-input"
            key={TSViewConfigurationType.SINGLE_CHART}
            style={{ padding: 0, marginBottom: 0 }}
          >
            <input
              type="radio"
              className="mr-md-1"
              value={TSViewConfigurationType.SINGLE_CHART}
              onChange={e => { this.setState({ tsMode: TSViewConfigurationType.SINGLE_CHART }); }}
              checked={this.state.tsMode === TSViewConfigurationType.SINGLE_CHART}
            />
            {'Single View'}
          </label>
          <label
            className="form-check-input"
            key={TSViewConfigurationType.DUAL_CHART}
            style={{ padding: 0, marginBottom: 0 }}
          >
            <input
              type="radio"
              className="mr-md-1"
              value={TSViewConfigurationType.DUAL_CHART}
              onChange={e => { this.setState({ tsMode: TSViewConfigurationType.DUAL_CHART }); }}
              checked={this.state.tsMode === TSViewConfigurationType.DUAL_CHART}
            />
            {'Dual View'}
          </label>
        </div>
        {this.state.tsMode === TSViewConfigurationType.SINGLE_CHART ?
          <ResponsiveContainer width={'100%'} height={600} >
            <LineChart data={points} syncId={'timeSeries'}>
              <Line type="monotone" dataKey={x => x[0]} stroke="#AA5A39" dot={false} name={'Predicted'} />
              <Line type="monotone" dataKey={x => x[1]} stroke="#2A4F6E" dot={false} name={'True'} />
              <Legend />
              <YAxis />
              <XAxis dataKey={x => x[2]} tickFormatter={timeFormat} />
              <Tooltip isAnimationActive={false} />
              <Brush dataKey={x => x[2]} height={30} stroke="#8884d8" />
            </LineChart>
          </ResponsiveContainer> :
          <>
            <ResponsiveContainer width={'100%'} height={300} >
              <LineChart data={points} syncId={'timeSeries'}>
                <Line type="monotone" dataKey={x => x[1]} stroke="#2A4F6E" dot={false} name={'True'} />
                <Legend />
                <YAxis domain={domainTotal} />
                <XAxis dataKey={x => x[2]} tickFormatter={timeFormat} />
                <Tooltip isAnimationActive={false} />
                <Brush dataKey={x => x[2]} height={30} stroke="#8884d8" />
              </LineChart>
            </ResponsiveContainer>
            <ResponsiveContainer width={'100%'} height={300} >
              <LineChart data={points} syncId={'timeSeries'}>
                <Line type="monotone" dataKey={x => x[0]} stroke="#AA5A39" dot={false} name={'Predicted'} />
                <Legend />
                <YAxis domain={domainTotal} />
                <XAxis dataKey={x => x[2]} tickFormatter={timeFormat} />
                <Tooltip isAnimationActive={false} />
                <Brush dataKey={x => x[2]} height={30} stroke="#8884d8" />
              </LineChart>
            </ResponsiveContainer>
          </>
        }
      </div>
    );
  }
}
