import * as React from 'react';
import * as Icon from 'react-feather';
import { PageHeader } from '../PageHeader/PageHeader';
import { HelpText } from '../HelpText/HelpText';
import * as d3m from 'd3m/dist';
import * as api from 'src/data/Api';
import ConfusionMatrix from './ConfusionMatrix/ConfusionMatrix';
import TimeSeriesForecastingLineChart from './TimeSeriesForecastingLineChart/TimeSeriesForecastingLineChart';
import ConfusionScatterplot from './ConfusionScatterplot/ConfusionScatterplot';
import PredictedImagesGallery from './PredictedImagesGallery/PredictedImagesGallery';
import RuleMatrix from './RuleMatrix/RuleMatrix';
import SolutionTable from '../SolutionTable/SolutionTable';
import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';
import { connectPropsToDatastore } from 'src/data/store';
import { Card } from '../Card/Card';
import PartialPlots from './PartialPlots/PartialPlots';
import Loading from '../Loading/Loading';
import { startCase } from 'lodash';
import { getTaskTypeFromKeywords, TaskKeyword } from 'd3m/dist';
import produce from 'immer';

enum ExplainSolutionsVisualizationType {
  UNSET = 'unset',
  NONE = 'none',
  CONFUSION_MATRIX = 'confusion-matrix',
  CONFUSION_SCATTERPLOT = 'confusion-scatterplot',
  RULE_MATRIX = 'rule-matrix',
  PARTIAL_PLOTS = 'partial-plots',
  TIME_SERIES_LINE_CHART = 'time-series-line-chart',
  PREDICTED_IMAGES_GALLERY = 'predicted-images-gallery',
  PREDICTED_OBJECT_IMAGES_GALLERY = 'predicted-object-images-gallery',
}

enum SolutionState {
  EVALUATING,
  SUCCESS,
  ERRORED,
}

interface ExplainSolutionsViewProps {
  solutions: d3m.Solution[];
  selectedProblem: d3m.Problem;
  selectedDataset: d3m.Dataset;
  favoriteSolutions: d3m.Solution[];
  exportSolutions: (solutions: d3m.Solution[], callback?: (message: string, done?: boolean) => void) => void;
}

interface ExplainSolutionsViewState {
  selectedSolutions?: string[]; // solution ids
  selectedVisualizationOption: { value: ExplainSolutionsVisualizationType, label: string };
  solutionStates: {
    [solutionID: string]: {
      solutionState: SolutionState;
      fittedSolutionID?: string; // currently selected fitted solution
    },
  };
  message: string; // message displayed to the user
  isExporting: boolean;
}

class ExplainSolutionsView extends React.PureComponent<ExplainSolutionsViewProps, ExplainSolutionsViewState> {
  constructor(props: ExplainSolutionsViewProps) {
    super(props);
    this.state = {
      selectedVisualizationOption: this.visualizationOptions[0],
      solutionStates: {},
      message: '',
      isExporting: false,
    };
  }

  onVisualizationTypeChange = (selected: { value: ExplainSolutionsVisualizationType, label: string }) => {
    this.setState({ selectedVisualizationOption: selected });
  }

  get visualizationOptions() {
    const taskKeywords = this.props.selectedProblem.about.taskKeywords;
    const taskType = getTaskTypeFromKeywords(taskKeywords);
    const options = [];

    options.push({ value: ExplainSolutionsVisualizationType.NONE, label: 'Select a visualization' }); // fallback

    if (taskType === d3m.TaskKeyword.classification) {
      options.push({
        value: ExplainSolutionsVisualizationType.CONFUSION_MATRIX,
        label: 'Confusion Matrix',
      });
      if (taskKeywords.includes(TaskKeyword.tabular)) {
        options.push({
          value: ExplainSolutionsVisualizationType.RULE_MATRIX,
          label: 'Rule Matrix',
        });
      }
    } else if (taskType === d3m.TaskKeyword.regression) {
      options.push({
        value: ExplainSolutionsVisualizationType.CONFUSION_SCATTERPLOT,
        label: 'Confusion Scatterplot',
      });
      if (this.isImageProblem(this.props.selectedDataset.dataResources)) {
        options.push({
          value: ExplainSolutionsVisualizationType.PREDICTED_IMAGES_GALLERY,
          label: 'Predicted Images Gallery - Sample',
        });
      } else {
              // if (taskKeywords.includes(TaskKeyword.tabular)) {
        options.push({
          value: ExplainSolutionsVisualizationType.PARTIAL_PLOTS,
          label: 'Partial Dependence Plots',
        });
      // }
      }

    } else if (taskType === d3m.TaskKeyword.forecasting) {
      options.push({
        value: ExplainSolutionsVisualizationType.CONFUSION_SCATTERPLOT,
        label: 'Confusion Scatterplot',
      });
      options.push({
        value: ExplainSolutionsVisualizationType.TIME_SERIES_LINE_CHART,
        label: 'Time Series Forecasting Line Chart',
      });
    } else if (taskType === d3m.TaskKeyword.objectDetection) {
      options.push({
        value: ExplainSolutionsVisualizationType.PREDICTED_OBJECT_IMAGES_GALLERY,
        label: 'Predicted Object Images Gallery - Samples',
      });
    }

    return options;
  }

  isImageProblem(resources: d3m.DataResource[]): Boolean {
    return this.getImageResource(resources) !== undefined;
  }

  getImageResource(resources: d3m.DataResource[]): d3m.DataResource | undefined {
    let imageResource: d3m.DataResource | undefined = undefined;
    resources.map( (resource: d3m.DataResource) => {
      if (resource.resType === d3m.ResourceType.image) {
        imageResource = resource;
      }
    });
    return imageResource;
  }

  solutionState(solutionID: string) {
    if (this.state.solutionStates && this.state.solutionStates[solutionID]) {
      return this.state.solutionStates[solutionID];
    } else {
      return { solutionState: SolutionState.EVALUATING };
    }
  }

  renderVisualization(solutionID: string) {
    const visualizationType = this.state.selectedVisualizationOption.value;
    if (visualizationType === ExplainSolutionsVisualizationType.NONE) {
      return <div>Please select a visualization option.</div>;
    }

    const { solutionState, fittedSolutionID } = this.solutionState(solutionID);
    if (solutionState === SolutionState.ERRORED) {
      return <div>Failed to generate predictions for the selected model.</div>;
    }

    if (solutionState === SolutionState.EVALUATING) {
      return <Loading message="Evaluating selected solution..." />;
    }

    if (!fittedSolutionID) {
      return <div>Unexpected error: solutions already evaluated but no fittedSolutionID available.</div>;
    }

    let visualization;
    switch (this.state.selectedVisualizationOption.value) {
      case ExplainSolutionsVisualizationType.CONFUSION_MATRIX:
        visualization =
          <ConfusionMatrix
            dataLoader={() => api.suspense.loadConfusionMatrixData(fittedSolutionID)}
          />;
        break;
      case ExplainSolutionsVisualizationType.CONFUSION_SCATTERPLOT:
        visualization =
          <ConfusionScatterplot
            dataLoader={() => api.suspense.loadConfusionScatterplotData(fittedSolutionID)}
          />;
        break;
      case ExplainSolutionsVisualizationType.RULE_MATRIX:
        visualization = <RuleMatrix
          fittedSolutionID={fittedSolutionID}
          resources={this.props.selectedDataset.dataResources}
        />;
        break;
      case ExplainSolutionsVisualizationType.PARTIAL_PLOTS:
        visualization =
          <PartialPlots
            problem={this.props.selectedProblem}
            dataLoader={() => api.suspense.loadPartialPlotsData(fittedSolutionID, this.props.selectedProblem)}
          />;
        break;
      case ExplainSolutionsVisualizationType.TIME_SERIES_LINE_CHART:
        visualization =
          <TimeSeriesForecastingLineChart
            dataLoader={() => api.suspense.loadTimeSeriesPlotData(fittedSolutionID)}
          />;
        break;
      case ExplainSolutionsVisualizationType.PREDICTED_IMAGES_GALLERY:
        visualization =
          <PredictedImagesGallery
            dataLoader={() => api.suspense.loadPredictedImagesData(fittedSolutionID)}
            resource={this.getImageResource(this.props.selectedDataset.dataResources)}
          />;
        break;
      case ExplainSolutionsVisualizationType.PREDICTED_OBJECT_IMAGES_GALLERY:
        visualization =
          <PredictedImagesGallery
            dataLoader={() => api.suspense.loadPredictedObjectImagesData(fittedSolutionID, '200')}
            resource={this.getImageResource(this.props.selectedDataset.dataResources)}
            boundingBox={true}
          />;
        break;
      default:
        visualization = null;
    }
    return <>
      <Row>
        <Col md={12}>
          <React.Suspense fallback={<Loading message={`Loading ${this.state.selectedVisualizationOption.label}...`} />}>
            { visualization }
          </React.Suspense>
        </Col>
      </Row>
    </>;
  }

  loadVisualization = async (solutionID: string) => {
    let nextSolutionsStates;
    try {
      const state = this.state.solutionStates[solutionID];
      if (state && state.solutionState !== SolutionState.SUCCESS) {
        // Request evaluation of the solution using TA3-side train/test split.
        nextSolutionsStates = produce(this.state.solutionStates, draftSolutionsStates => {
          draftSolutionsStates[solutionID] = {
            solutionState: SolutionState.EVALUATING,
            fittedSolutionID: undefined,
          };
        });
        this.setState({ solutionStates: nextSolutionsStates});
      }

      const { fittedSolutionID, error } = await api.evaluateSolution(solutionID, this.props.selectedProblem);

      nextSolutionsStates = produce(this.state.solutionStates, draftSolutionsStates => {
        draftSolutionsStates[solutionID] = {
          solutionState: error ? SolutionState.ERRORED : SolutionState.SUCCESS,
          fittedSolutionID: fittedSolutionID,
        };
      });
      this.setState({ solutionStates: nextSolutionsStates});

    } catch (error) {
      nextSolutionsStates = produce(this.state.solutionStates, draftSolutionsStates => {
        draftSolutionsStates[solutionID] = {
          solutionState: SolutionState.ERRORED,
          fittedSolutionID: undefined,
        };
      });
      this.setState({ solutionStates: nextSolutionsStates});
    }
  }

  onSelectSolutions(solutionIDs: string[]) {
    this.setState({ selectedSolutions: solutionIDs });
    solutionIDs.forEach(solutionID => this.loadVisualization(solutionID));
  }

  renderColumns() {

    const taskType = getTaskTypeFromKeywords(this.props.selectedProblem.about.taskKeywords);
    if (
      taskType === d3m.TaskKeyword.vertexClassification ||
      taskType === d3m.TaskKeyword.vertexNomination
    ) {
      return (
        <div>
          There are no visualizations available for{' '}
          <mark className="badge-pill badge-secondary">{startCase(taskType)}</mark> problem.
        </div>
      );
    }

    if (!this.state.selectedSolutions || this.state.selectedSolutions.length <= 0) {
      return <div className="col-sm-12">Please select a solution to visualize.</div>;
    }
    const allSolutions = this.props.solutions;
    const solutions = this.state.selectedSolutions
      .map(solutionID => allSolutions.find(s => s.id === solutionID))
      .filter(solution => solution); // make sure to filter undefined solutions

    const columnWidth = 100 / solutions.length;
    return solutions
      .map((solution: d3m.Solution) => {
        let title = `${solution.name}`;
        if (solution.description) {
          title += ` (${solution.description.modelName})`;
        }
        return (
          <div key={`explain-column-${solution.id}`} style={{ width: `${columnWidth}%`}}>
            <Col md={12}>
              <Card title={title} className="mt-4">
                { this.renderVisualization(solution.id) }
              </Card>
            </Col>
          </div>
        );
      });
  }

  render() {
    return (
      <>
        <PageHeader title="Explain Solutions">
          <button className="btn btn-sm btn-outline-primary"
            disabled={!this.props.favoriteSolutions.length || this.state.isExporting}
            onClick={() => {
              this.props.exportSolutions(
              this.props.favoriteSolutions,
              (message, done) => {
                const isExporting = !done;
                this.setState({ message, isExporting });
              },
              );
          }
          }
          >
            <Icon.ChevronsRight className="feather" />
            Export Favorite Solutions & Finish
          </button>
        </PageHeader>
        <Row>
          <Col md={12}>
            <HelpText
              text="Select solutions to see explanatory visualizations
              bellow. Use the arrows to re-rank the best solutions."
            />
            {
              this.state.message !== '' ?
                <div className="alert alert-primary">
                  {
                    this.state.isExporting ?
                    <Loading message={this.state.message}/> :
                    <div>{ this.state.message }</div>
                  }
                </div> : null
            }
          </Col>
        </Row>

        <Row>
          <Col md={12}>
            <SolutionTable
              onSelectedSolutionsChange={ids => this.onSelectSolutions(ids)}
              multiSelect={true}
            />
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col sm={12} md={12} lg={12}>
            <Select
              options={this.visualizationOptions}
              value={this.state.selectedVisualizationOption}
              onChange={this.onVisualizationTypeChange}
            />
          </Col>
        </Row>
        <Row>
          <div className="d-flex justify-content-between" style={{ width: '100%' }}>
            {
              this.state.selectedSolutions &&
              this.renderColumns()
            }
          </div>
        </Row>
      </>
    );
  }
}

export default connectPropsToDatastore(store => ({
  selectedProblem: store.selectedProblem,
  selectedDataset: store.selectedDataset,
  favoriteSolutions: store.favoriteSolutions,
  exportSolutions: store.exportSolutions,
  solutions: store.solutions,
}))(ExplainSolutionsView);
