import React from 'react';
import { ComposedChart, XAxis, YAxis, CartesianGrid, Line, Area, Label, Bar } from 'recharts';
import * as d3m from 'd3m/dist';
import PersistentComponent from 'src/components/PersistentComponent';
import styled from 'styled-components';
import { format } from 'd3-format';

const yTickFormatter = format(',.4g');

interface PartialPlotsProps {
  problem: d3m.Problem;
  dataLoader: () => d3m.PartialPlotData;
}

const LegendBox = styled.div`
  height: 10px;
  width: 10px;
  background-color:${(props: { color: string }) => props.color};
  display: inline-block;
  margin-left: 10px;
`;

const ChartContainer = styled.div`
  & .recharts-line-dots {
    display: none;
  }
`;

// tslint:disable-next-line:no-any
function computeFeatureImportanceDepPlot(values: Array<any>, accessor: Function) {
  let absSumMaxDiff = 0;
  let absSumPosDiff = 0;
  let absSumNegDiff = 0;
  for (let i = 0; i < values.length - 1; i++) {
    const diff = accessor(values[i]) - accessor(values[i + 1]);
    if (diff > 0) {
      absSumPosDiff += diff;
      absSumNegDiff = 0;
      if (absSumPosDiff > absSumMaxDiff) {
        absSumMaxDiff = absSumPosDiff;
      }
    } else {
      absSumNegDiff += Math.abs(diff);
      absSumPosDiff = 0;
      if (absSumNegDiff > absSumMaxDiff) {
        absSumMaxDiff = absSumNegDiff;
      }
    }
  }
  return absSumMaxDiff;
}

export class PartialPlots extends PersistentComponent<PartialPlotsProps> {

  constructor(props: PartialPlotsProps) {
    super(props);
    this.state = {};
  }

  render() {
    const {problem} = this.props;
    const data = this.props.dataLoader();
    if (!data) {
      return <div>No Data </div>;
    }
    try {
      const partials = data.partials.map(p => ({
        ...p,
        values: p.values.map(v => {
          const hist = p.histogram.find(h => h.value === v.value)!;
          if (!hist || !hist.count) {
            return {...v, std: 0, base: 0, trueBand: 0, trueMean: 0};
          }
          return {
            ...v,
            std: [v.mean - v.std, v.mean + v.std],
            base: [data.baseStats.mean - data.baseStats.std, data.baseStats.mean + data.baseStats.std],
            trueBand: [hist.mean - (hist.std || 0), hist.mean + (hist.std || 0)],
            trueMean: hist.mean,
          };
        }).filter(v => v.trueMean || v.std),
      }));

      // Sorting partial dependence plots by feature importance
      // tslint:disable-next-line:no-any
      let importances = partials.map((p: any) => computeFeatureImportanceDepPlot(p.values, (x: any) => x.mean));
      let importancesIdx = importances.map((x, idx) => [x, idx]);
      importancesIdx.sort((a, b) => b[0] - a[0]);
      const partialsSorted = importancesIdx.map(([x, idx]) => partials[idx]);

      return (
        <div>
          <div className="d-flex" style={{justifyContent: 'center'}}>
            <div><LegendBox color="#8884d8"/> Predicted</div>
            <div><LegendBox color="#EEBBBB"/> Ground Truth</div>
          </div>
          <ChartContainer style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-evenly'}}>
            {partialsSorted.map(p => {
              const min: number = Math.min(...p.values.map(v => Math.min(v.trueBand[0], v.std[0])));
              const max: number = Math.max(...p.values.map(v => Math.max(v.trueBand[1], v.std[1])));
              return (
                <div key={p.column.colName} style={{ margin: '10px', padding: '10px' }}>
                  <ComposedChart width={300} height={200} margin={{ top: 25, right: 5, bottom: 5, left: 5 }}
                  data={p.values}>
                    <YAxis type="number" domain={[min, max]} tickFormatter={yTickFormatter}>
                      <Label
                        value={`${problem!.inputs.data[0].targets[0].colName }`}
                        x={0} y={125}
                        textAnchor="middle"
                        transform="rotate(270, 100, 125) translate(70, -80)" />
                    </YAxis>
                    <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
                    <Line dataKey="mean" stroke="#8884d8" animationDuration={0} />
                    <Line dataKey="trueMean" stroke="#EEBBBB"  animationDuration={0} />
                    <Area dataKey="std" stroke="#0088FE" fillOpacity="0.1" strokeOpacity="0.2"  animationDuration={0} />
                    <Area dataKey="trueBand" fill="#FFCCCC" stroke="#FFCCCC" strokeOpacity="0.2"
                          animationDuration={0} fillOpacity="0.2"  />
                </ComposedChart>
                <ComposedChart
                  width={300}
                  height={100}
                  margin={{ top: 0, right: 5, bottom: 20, left: 5 }}
                  data={p.histogram}>
                  <YAxis dataKey="count">
                    <Label
                      value="count" x={0} y={0}
                      textAnchor="middle"
                      transform="rotate(270, 25, 35) translate(0, -10)"
                    />
                  </YAxis>
                  <Bar dataKey="count" fill="#ccc" />
                  <XAxis
                    dataKey="value"
                    tickFormatter={v =>
                      p.column.colType === d3m.ColumnType.dateTime ? new Date(v).toISOString().slice(0, 10) : v
                    }
                  >
                    <Label value={p.column.colName} position="bottom" />
                  </XAxis>
                </ComposedChart>
              </div>
            );
          })
        }
        </ChartContainer>
        </div>
      );
    } catch (error) {
      console.log(error);
      return <div>Failed while creating partial plots. </div>;
    }
  }
}

export default PartialPlots;
