import * as React from 'react';
import ConfusionTableContainer from './ConfusionTableContainer';
import * as d3m from 'd3m/dist';

interface ConfusionMatrixProps {
  dataLoader: () => d3m.ConfusionMatrixData;
}

class ConfusionMatrix extends React.PureComponent<ConfusionMatrixProps, {}> {

  constructor(props: ConfusionMatrixProps) {
    super(props);
  }

  render() {
    const { conf, classDefs } = this.props.dataLoader();
    return <ConfusionTableContainer conf={conf} classDefs={classDefs}/>;
  }
}

export default ConfusionMatrix;
