import React, { PureComponent } from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import ImageViewer from '../../DataExploration/Image/ImageViewer';
import { Row, Col } from 'react-bootstrap';
import styled from 'styled-components';

const LegendBox = styled.div`
  height: 10px;
  width: 10px;
  background-color:${(props: { color: string }) => props.color};
  display: inline-block;
  margin-left: 10px;
`;

interface PredictedImagesGalleryProps {
  dataLoader: () => d3m.PredictedImagesData | d3m.PredictedObjectData;
  dataImageResource: (resourceI: d3m.DataResource) => d3m.DataForResource | undefined;
  resource?: d3m.DataResource;
  boundingBox?: boolean;
}

interface InfoAboutPrediction {
  imageName?: string;
  truth?: string | string[];
  predicted?: string | string[];
}

interface PredictedImagesGalleryState {
  resourceFiles?: d3m.ResourceFileData[];
}

class PredictedImagesGallery extends
  PureComponent<PredictedImagesGalleryProps, PredictedImagesGalleryState> {

  constructor(props: PredictedImagesGalleryProps) {
    super(props);
    if (props.resource) {
      const resourceFiles = this.props.dataImageResource(props.resource) as d3m.ResourceFileData[];
      this.state = {
        resourceFiles: resourceFiles,
      };
    }
  }
  getPredictionInfo (d: [string, string, string] | [string[], string[], string]) {
    return {
      imageName: d[2],
      truth: d[1],
      predicted: d[0],
    };
  }
  render() {
    const data = this.props.dataLoader();
    const infoAboutPrediction: InfoAboutPrediction[] = this.props.boundingBox ?
    (data as d3m.PredictedObjectData).points.map((d)  => this.getPredictionInfo(d))
    :
    (data as d3m.PredictedImagesData).points.map((d) => this.getPredictionInfo(d));

    return <>
      <div>
        <div className="d-flex" style={{ justifyContent: 'center', marginBottom: '10px' }}>
          <div><LegendBox color="#8884d8" /> Predicted</div>
          <div><LegendBox color="#EEBBBB" /> Ground Truth</div>
        </div>
      </div>
      <br />
      <Row>
        {
          this.state.resourceFiles && this.state.resourceFiles.map((d, index) => {
            let res = infoAboutPrediction.find(el => el.imageName === d.name);
            if (res) {
              return (
                <Col md={3} key={`predicted-image-${index}`}>
                  {
                    this.props.boundingBox
                    ?
                    <>
                    <ImageViewer data={d} truthBoundingBox={res.truth as string[]}
                    predBoundingBox={res.predicted  as string[]}/>
                    <ul>
                      <li style={{ color: '#8884d8' }}>Number of objects: {(res.predicted  as string[]).length}</li>
                      <li style={{ color: '#EEBBBB' }}>Number of objects: {(res.truth  as string[]).length}</li>
                    </ul>
                    </>
                    :
                    <>
                    <ImageViewer data={d} />
                    <ul>
                      <li style={{ color: '#8884d8' }}>{res.predicted}</li>
                      <li style={{ color: '#EEBBBB' }}>{res.truth}</li>
                    </ul>
                    </>
                  }
                </Col>
              );
            } else {
              return (<div key={`predicted-image-${index}`} />);
            }
          })
        }
      </Row>
    </>;
  }
}

export default connectPropsToDatastore<PredictedImagesGalleryProps>((store, ownProps) => ({
  dataImageResource: store.dataForResource,
}))(PredictedImagesGallery);
