import * as React from 'react';
import * as Icon from 'react-feather';
import { Link } from 'react-router-dom';
import VisusLogoPng from '../../images/visus-logo.png';

export class HomePageView extends React.PureComponent {
  componentWillMount() {
    // Applying a class here seems odd, but it is the only way to apply a style
    // to the body tag which is outside of the scope managed by React.
    document.body.className = 'datasets-view';
  }

  componentWillUnmount() {
    document.body.className = '';
  }

  render() {
    return (
    <div>
        <div className="bg-purple" style={{marginLeft: 0, width: '100%', textAlign: 'center', padding: 90}}>
            <div>
                <div className="d-flex justify-content-center">
                  <img src={VisusLogoPng} className="ml-2 mr-2" height="70" width="70" />
                  <h1
                    className="mb-2 mt-2 text-white text-oswald"
                    style={{verticalAlign: 'middle'}}
                  >
                    Visus
                  </h1>
                </div>
                <div className="mt-5">
                  <h5 className="mb-3 text-white text-merriweather">Welcome!</h5>
                  <p className="mb-0 text-white small">
                    What would you like to know? The price of a 780 ft<sup>2</sup>, 1 bedroom
                    apartment in your neighborhood?
                    Your dog's life expectancy?
                  </p>
                  <p className="mb-0 text-white small">
                    Visus helps you to answer your questions using
                    existing datasets
                    and state-of-the-art machine learning techniques.
                  </p>
                  <p className="mb-0 text-white small">
                    Visus presents step-by-step guidance which enables
                    you to build your own Machine Learning model without
                    knowing how machine learning works.
                  </p>
                </div>
            </div>
        </div>
        <Link to={'/select-dataset'}>
        <div
          className="d-flex align-items-center"
          style={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 30, marginBottom: 30}}
        >
          <button className="btn btn-outline-primary" data-cy="use-visus">
            <Icon.ChevronsRight className="feather" /> USE VISUS
          </button>
        </div>
        </Link>
    </div>
    );
  }
}

export default {HomePageView};
