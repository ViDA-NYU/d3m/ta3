import * as React from 'react';
import * as Icon from 'react-feather';
import * as d3m from 'd3m/dist';
import './select-dataset-view.css';
import { CardShadow } from '../Card/Card';
import ReactTable, { Column as ReactTableColumn } from 'react-table';
import Loading from '../Loading/Loading';
import { writeNewDatasetWS, writeDataD3MFormatWS } from 'src/data/Api';

interface LoadNewDatasetProps {
  onSaveDatasetSuccessfully: () => void;
  onSaveDatasetFailure: () => void;
  onCancelSave: () => void;
  infoNewData: d3m.InfoDataset;
  datasetNames: string [];
}

interface LoadNewDatasetState {
  infoNewData?: d3m.InfoDataset;
  profilingStatus?: d3m.ta3.WriteNewDataResponse;
  isDuplicatedDataset: boolean;
  disableSaveButton: boolean;
}

class LoadNewDataset extends React.PureComponent<LoadNewDatasetProps, LoadNewDatasetState> {
  constructor(props: LoadNewDatasetProps) {
    super(props);
    this.state = {
      isDuplicatedDataset: false,
      disableSaveButton: false,
    };
  }

  componentDidMount() {
    const { datasetID, file } = this.props.infoNewData;
    const reader = new FileReader();
    reader.onabort = () => console.log('file reading was aborted');
    reader.onerror = () => console.log('file reading has failed');
    reader.onload = () => {
      var csv = reader.result as string;
      this.uploadCsvFile(datasetID, csv);
    };
    reader.readAsDataURL(file);
  }

  async uploadCsvFile(datasetID: string, data: string) {
    // upload and profile data
    const profilingStatus = await writeNewDatasetWS(datasetID, data);

    if (profilingStatus.statusWriteNewData === d3m.RestRequestState.SUCCESS) {
      // profiling has concluded successfully
      this.setState({
        profilingStatus: profilingStatus,
        infoNewData: this.props.infoNewData,
      });
    } else {
      this.setState({ profilingStatus: profilingStatus });
    }
  }

  getDataType(type: string) {
    let stringType = type.slice(type.lastIndexOf('/') + 1);
    switch (stringType) {
      case 'Enumeration':
        return 'Categorical';
      default:
        return stringType;
    }
  }

  stringToJson(text: string) {
    var lines = text.split('\n');
    var result = [];
    var headers = lines[0].split(',');
    for (var i = 1; i < lines.length; i++) {
      var obj = {};
      var currentLine = lines[i].split(',');
      for (var j = 0; j < headers.length; j++) {
        obj[headers[j]] = currentLine[j];
      }
      result.push(obj);
    }
    return result;
  }

  /**
   * Update data types.
   */
  updateDataTypes = (featureName: string, newType: string) => {
    if (!this.state.profilingStatus) {
      return;
    }
    const dataInfo = JSON.parse(this.state.profilingStatus.dataTypes);
    const columns = dataInfo.columns;
    // Find index of specific object using findIndex method.
    const objIndex = columns.findIndex((obj: d3m.ColumnData) => obj.name === featureName);
    // Update type property.
    const tempDataType = columns[objIndex].structural_type;
    switch (newType) {
      case 'Categorical':
        newType = 'Enumeration';
        break;
      default:
        newType = newType;
    }
    const temp = tempDataType.substring(0, tempDataType.lastIndexOf('/') + 1) + newType;
    dataInfo.columns[objIndex].structural_type = temp;
    dataInfo.columns[objIndex].semantic_types = [temp];
    this.setState({
      profilingStatus: { ...this.state.profilingStatus, dataTypes: JSON.stringify(dataInfo) },
    });
  };

  getHeader(element: d3m.ColumnData): JSX.Element {
    let typeData = ['String', 'Integer', 'Float', 'Categorical', 'DateTime', 'Text', 'Boolean', 'Real'];
    const featureType = this.getDataType(element.structural_type);
    return (
      <div style={{ overflow: 'auto', marginBottom: 0 }}>
        <div key={'i.toString()' + element.name} style={{ overflow: 'auto', marginBottom: 0 }}>
          {element.name}
        </div>
        <div>
          <div className="d-flex align-items-center">
            <select
              className="custom-select mr-sm-4 ml-4"
              id="settings-max-time-unit"
              style={{ maxWidth: '200px' }}
              value={featureType}
              onChange={e => {
                this.updateDataTypes(element.name, e.target.value);
              }}
            >
              {typeData.map(unit => (
                <option key={unit} value={unit}>
                  {unit}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    );
  }

  saveDataset() {
    if (this.state.infoNewData && this.state.profilingStatus) {
      const metadata = {
        filename: this.state.infoNewData.datasetName,
        datasetID: this.state.infoNewData.datasetID,
        metadata: JSON.parse(this.state.profilingStatus.dataTypes),
      };

      if (!this.props.datasetNames.includes(this.state.infoNewData.datasetName)) {
        const stringifyMetadata = JSON.stringify(metadata);
        this.setState({isDuplicatedDataset: false, disableSaveButton: true});
        this.writeDataD3MFormat(stringifyMetadata);
      } else {
        this.setState({isDuplicatedDataset: true});
      }
    }
  }

  // Write info of the new data (json format) in a D3M format
  async writeDataD3MFormat(metadata: string) {
    const result: d3m.ta3.WriteDataD3MFormatResponse = await writeDataD3MFormatWS(metadata);

    if (result.statusSaveNewData === d3m.RestRequestState.SUCCESS) {
      this.props.onSaveDatasetSuccessfully();
    } else if (result.statusSaveNewData === d3m.RestRequestState.ERROR) {
      this.props.onSaveDatasetFailure();
    }
    this.setState({isDuplicatedDataset: false});
  }

  renderErrorMessage(error?: string) {
    return (
      <>
        <div className="text-danger mt-2 mb-4">Unexpected error while profiling data {error && `(${error})`}</div>
        <button
          className="btn btn-outline-primary float-left"
          onClick={() => {
            this.props.onCancelSave();
          }}
        >
          <Icon.XCircle className="feather" /> Try a different CSV file.
        </button>
      </>
    );
  }
  renderErrorMessageDuplicateNameDataset(error?: string) {
    return (
      <>
        <div className="alert alert-danger" role="alert">
          <span>
            The dataset has already been uploaded. If you still want to upload it, please rename the dataset.
          </span>
        </div>
      </>
    );
  }

  handleDatasetNameChange(name: string) {
    if (this.state.infoNewData) {
      this.setState({
        infoNewData: {
          ...this.state.infoNewData,
          datasetName: name,
        },
      });
    }
  }

  renderDatasetProfilerForm(profilingStatus: d3m.ta3.WriteNewDataResponse) {
    let dataJson = JSON.parse(profilingStatus.dataTypes);

    const columnsData = dataJson.columns.map(
      (column: d3m.ColumnData): ReactTableColumn<Object> => ({
        id: `column-id-${column.name}`,
        Header: this.getHeader(column),
        accessor: v => (v ? v[column.name] : ''),
        minWidth: 150,
        sortable: false,
      }),
    );

    const sampleNewData = JSON.stringify(this.stringToJson(dataJson.sample));
    return (
      <>
      {
        this.state.isDuplicatedDataset && this.renderErrorMessageDuplicateNameDataset()
      }
        <h6>Dataset Info</h6>
        <div className="row">
          <div className="col-lg-6 mb-6">
            <div className="form-group row">
              <label className="col-sm-3 col-form-label small">Name:</label>
              <div className="col-sm-9">
                <input
                  className="form-control"
                  id="inputPassword"
                  onChange={e => this.handleDatasetNameChange(e.target.value)}
                  placeholder={this.props.infoNewData.datasetName}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-lg-12 mb-2">
            <h6>Dataset Sample</h6>
            <ReactTable
              data={JSON.parse(sampleNewData)}
              columns={columnsData}
              pageSize={9}
              showPageSizeOptions={false}
            />
          </div>
        </div>
        <div className="d-flex justify-content-end">
          <button className="btn btn-outline-primary" onClick={() => this.props.onCancelSave()}>
            <Icon.Trash2 className="feather" /> Discard Dataset
          </button>
          <button className="btn btn-primary ml-2"
          onClick={() => this.saveDataset()}
          disabled = {this.state.disableSaveButton}
          >
            <Icon.Save className="feather" /> Save Dataset
          </button>
        </div>
      </>
    );
  }

  render() {
    const { profilingStatus } = this.state;
    return (
      <div className="col-lg-12 mb-4">
        <CardShadow>
          <h5>Load Dataset</h5>
          {!profilingStatus ? (
            <>
              <div className="mt-3 mb-3">
                <Loading message={`Profiling CSV file: ${this.props.infoNewData.filename}...`} />
              </div>
              <button className="btn btn-outline-primary float-left" onClick={() => this.props.onCancelSave()}>
                <Icon.XCircle className="feather" /> Cancel
              </button>
            </>
          ) : (
            <>
              {profilingStatus.statusWriteNewData === d3m.RestRequestState.ERROR && (
                <>{this.renderErrorMessage(profilingStatus.error)}</>
              )}
              {profilingStatus.statusWriteNewData === d3m.RestRequestState.SUCCESS && (
                <>{this.renderDatasetProfilerForm(profilingStatus)}</>
              )}
            </>
          )}
        </CardShadow>
      </div>
    );
  }
}

export { LoadNewDataset };
