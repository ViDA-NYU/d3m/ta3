import * as React from 'react';
import * as Icon from 'react-feather';
import Loadable from '../../data/Loadable';
import { Link } from 'react-router-dom';
import * as d3m from 'd3m/dist';
import './select-dataset-view.css';
import '../common.css';
import { connectPropsToDatastore } from 'src/data/store';
import { PageHeader } from '../PageHeader/PageHeader';
import { CardShadow, CardButton } from '../Card/Card';
import { HelpText } from '../HelpText/HelpText';
import ReactMarkdown from 'react-markdown';
import Dropzone from 'react-dropzone';
import { LoadNewDataset } from './LoadNewDataset';
import { Row, Col } from 'react-bootstrap';

const MAX_FILE_SIZE = 100 * 1024 * 1024; // maximum file size

interface DatasetLinkProps {
  dataset: d3m.Dataset;
  selectDataset: (dataset: d3m.Dataset) => void;
}

class DatasetLink extends React.PureComponent<DatasetLinkProps> {
  render() {
    const { about, dataResources } = this.props.dataset;
    let description = about.description ? about.description : '';
    return (
      <CardShadow height="250px">
        <div className="mb-2" style={{ height: '165px' }}>
          <h6 className="mb-2">{about.datasetID}</h6>
          <div className="mb-2">
            {dataResources.map((dr, i) => (
              <span key={'data-resource_' + about.datasetID + i} className="badge badge-pill badge-primary mr-1">
                {dr.resType}
              </span>
            ))}
          </div>
          <div
            className="text-merriweather text-muted small hidden-scrollbar"
            style={{ height: '85px', overflow: 'scroll' }}
          >
            <ReactMarkdown className="code-non-language" source={description} />
          </div>
        </div>
        <Link
          data-cy={'datasetID:' + this.props.dataset.about.datasetID}
          to={'/select-task'}
          className={'dataset-link'}
          onClick={() => {
            this.props.selectDataset(this.props.dataset);
          }}
        >
          <button className="btn btn-outline-primary">
            <Icon.ChevronsRight className="feather" /> Select
          </button>
        </Link>
      </CardShadow>
    );
  }
}

interface SelectDatasetViewProps {
  datasets: Loadable<d3m.Dataset[]>;
  selectDataset: (dataset: d3m.Dataset) => void;
  resetValue: (key: string) => void;
  loadDataStatus: boolean;
}

interface SelectDatasetViewState {
  isFileTooLarge: boolean;
  message?: string;
  error: boolean;
  isLoadingData: boolean;
  newDatasetInfo?: d3m.InfoDataset;
  profilingStatus?: d3m.ta3.WriteNewDataResponse;
}

export class SelectDatasetView extends React.PureComponent<SelectDatasetViewProps, SelectDatasetViewState> {
  constructor(props: SelectDatasetViewProps) {
    super(props);
    this.handleOnDropFile = this.handleOnDropFile.bind(this);
    this.onSaveDatasetFailure = this.onSaveDatasetFailure.bind(this);
    this.onSaveDatasetSuccessfully = this.onSaveDatasetSuccessfully.bind(this);
    this.onCancelLoad = this.onCancelLoad.bind(this);
    this.state = {
      isFileTooLarge: false,
      error: false,
      isLoadingData: false,
    };
  }

  randomString = (len: number): string => {
    return Math.random()
      .toString(36)
      .substr(2, len);
  };

  cleanFileName(filename: string): string {
    const tempStr = filename.replace(/\s+/g, '');
    const newFilename = tempStr.substring(0, tempStr.lastIndexOf('.'));
    return newFilename;
  }

  loadFile(file: File) {
    // this.props.setLoadDataStatus(true);
    const datasetID: string = this.randomString(6);
    this.setState({
      newDatasetInfo: {
        filename: file.name,
        datasetName: this.cleanFileName(file.name),
        datasetID: datasetID,
        file: file,
      },
      isLoadingData: true,
    });
  }

  onSaveDatasetSuccessfully() {
    this.props.resetValue('datasets'); // reload the datasets
    this.setState({
      isLoadingData: false,
      error: false,
      message: 'Successfully uploaded CSV file.',
    });
  }

  onSaveDatasetFailure(error?: string) {
    this.setState({
      isLoadingData: false,
      error: true,
      message: error
      ? error
      : 'Unexpected error: failed to save dataset.',
    });
  }

  onCancelLoad() {
    this.setState({ isLoadingData: false});
  }

  handleOnDropFile(acceptedFiles: File[], rejectedFiles: File[]) {
    const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > MAX_FILE_SIZE;
    if (acceptedFiles.length > 0 && !isFileTooLarge) {
      this.setState({ isFileTooLarge: false, message: undefined, error: false });
      this.loadFile(acceptedFiles[0]);
    } else {
      this.setState({
        isFileTooLarge: isFileTooLarge,
        isLoadingData: false,
      });
    }
  }

  availableDatasets (datasets: Loadable<d3m.Dataset[]>): string [] {
    return datasets.value ? datasets.value.map((dataset) => dataset.about.datasetID) : [];
  }

  renderDropFileCard(): JSX.Element {
    return (
      <>
        <CardShadow height="250px">
          <Dropzone
            multiple={false}
            accept="text/csv"
            minSize={0}
            maxSize={MAX_FILE_SIZE}
            onDrop={this.handleOnDropFile}
          >
            {({ getRootProps, getInputProps, isDragActive, isDragReject, rejectedFiles }) => {
              return (
                <CardButton {...getRootProps()}>
                  <div className="dropzone dropzone-container text-center">
                    <input {...getInputProps()} />
                    {!isDragActive && (
                      <>
                        <h6>Import a new dataset</h6>
                        <span>Click here or drop a file to upload!</span>
                      </>
                    )}
                    {isDragActive && !isDragReject && 'Drop it here!'}
                    {isDragReject && (
                      <div className="text-danger mt-2">File type not accepted. Only CSV files are supported.</div>
                    )}
                    {this.state.isFileTooLarge && !isDragActive && (
                      <div className="text-danger mt-2">
                        File is too large. The maximum file size is capped at 100MB.
                      </div>
                    )}
                  </div>
                </CardButton>
              );
            }}
          </Dropzone>
        </CardShadow>
      </>
    );
  }

  render() {
    const { datasets, selectDataset, resetValue } = this.props;
    return (
      <div>
        <PageHeader title="Select Dataset" />
        <HelpText>
          Please load a tabular dataset or select an existing dataset you want to use for your problem. Your dataset
          would include a series of
          <span style={{ color: '#12a912' }}>
            <b> data instances </b>
          </span>
          (e.g., Dog A, Dog B, Dog C, ...) which contain the same
          <span style={{ color: '#12a912' }}>
            <b> features </b>
          </span>
          (i.e., the properties of a dog such a weight, height, breed, age).
        </HelpText>
        <br />
        <Row>
          <Col md={12}>
            {this.state.message && (
              <div className={`alert alert-${this.state.error ? 'danger' : 'success'}`}>
                {this.state.message}
              </div>
            )}
          </Col>
        </Row>

        <div className="mt-3 mb-3">
          <div className="row">
            {// Error while loading the list of available datasets
            datasets.error ? (
              <div className="alert alert-danger" role="alert">
                <span>
                  <b>Error:</b> {datasets.error.originalError && datasets.error.message}
                </span>{' '}
                -{' '}
                <a href="#" onClick={() => resetValue('datasets')}>
                  Try again
                </a>
              </div>
            ) : // If there is no error while loading the list of available datasets
            // THEN Verify if a new dataset is being loading
            (this.state.isLoadingData && this.state.newDatasetInfo) ? (
              <LoadNewDataset
                onSaveDatasetFailure={this.onSaveDatasetFailure}
                onSaveDatasetSuccessfully={this.onSaveDatasetSuccessfully}
                onCancelSave={this.onCancelLoad}
                infoNewData={this.state.newDatasetInfo}
                datasetNames={this.availableDatasets(datasets)}
              />
            ) : (
              // There is no new dataset being loaded
              <div className="col-lg-4 mb-4">{this.renderDropFileCard()}</div>
            )}
            {datasets.value!.map((dataset, i) => (
              <div key={'dataset-' + i} className="col-lg-4 mb-4">
                <DatasetLink dataset={dataset} selectDataset={selectDataset} />
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<SelectDatasetViewProps>(store => ({
  datasets: store.datasets,
  selectDataset: store.selectDataset,
  resetValue: store.resetValue,
}))(SelectDatasetView);
