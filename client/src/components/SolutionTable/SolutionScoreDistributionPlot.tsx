import * as React from 'react';
import { histogram, range } from 'd3-array';
import { format } from 'd3-format';
import { scaleLinear } from 'd3-scale';
import * as d3m from 'd3m/dist';
import { Bar, BarChart, XAxis, Tooltip, ResponsiveContainer, Cell } from 'recharts';
import { LIGHT_NYU_PURPLE } from 'src/common';

const NUM_BINS = 10;
const DEFAULT_CHART_HEIGHT_PX = 100;
const DEFAULT_PADDING_PX = 10;
const X_AXIS_TICKS_FORMATTER = format(',.3g');
const TOOLTIP_FORMATTER = format(',.3g') as (v: number) => string;

interface SolutionScoreDistributionProps {
  solutions: d3m.Solution[];
  scoreAccessor: (solution: d3m.Solution) => number; // specifies how to access a score
  highlightSolution?: string; // id
  width?: number;
  height?: number;
  padding?: number;
}

class SolutionScoreDistributionPlot extends React.PureComponent<SolutionScoreDistributionProps> {
  render() {
    // Filters valid scores and sort them
    const scores = this.props.solutions
      .map(solution => this.props.scoreAccessor(solution))
      .filter(score => score !== undefined && score !== NaN)
      .sort((a, b) => a - b);

    // Modifies the domain so that it starts and ends on nice round values
    const [ minScore, maxScore ] = [scores[0], scores[scores.length - 1]];
    const xScale = scaleLinear().domain([minScore, maxScore]).nice(NUM_BINS);
    const domain: [number, number] = xScale.domain() as [number, number];

    // "Manually" compute bins so that it contains excactly NUM_BINS bins with
    // uniform size. This is needed because d3 histogram does not guarantee that
    // the final number of bins is equal to the number requested.
    const minDomain = domain[0];
    const maxDomain = domain[1];
    const bins = range(minDomain, maxDomain, (maxDomain - minDomain) / NUM_BINS);

    // compute the histogram data and format it properly
    const data = histogram()
      .domain(domain)
      .thresholds(bins)(scores)
      .map((bin, index) => {
        return {
          index,
          value: bin.x0 as number,
          nextValue: bin.x1 as number,
          count: bin.length,
        };
      });

    let highlightBinIndex: number;
    if (this.props.highlightSolution) {
      const highlightSolution = this.props.solutions
        .find(solution => solution.id === this.props.highlightSolution);
      if (highlightSolution) {
        // highlightSolution may not exist at the moment of user removing this solution
        const score = this.props.scoreAccessor(highlightSolution) as number;
        const highlightBin = data.find((bin, index) => bin.value <= score && score < bin.nextValue ||
          // handle the maxValue equaling the last bin's right end
          index === data.length - 1 && score === bin.nextValue);
        if (highlightBin) {
          highlightBinIndex = highlightBin.index;
        }
      }
    }

    // Use only min and max bin values as ticks
    const minValue = bins[0];
    const maxValue = bins[bins.length - 1];

    return (
      <ResponsiveContainer height={this.props.height !== undefined ? this.props.height : DEFAULT_CHART_HEIGHT_PX}>
        <BarChart data={data}>
          <XAxis
            dataKey="value"
            ticks={[minValue, maxValue]}
            tickFormatter={X_AXIS_TICKS_FORMATTER}
            mirror={false}
            padding={{
              left: this.props.padding !== undefined ? this.props.padding : DEFAULT_PADDING_PX,
              right: this.props.padding !== undefined ? this.props.padding : DEFAULT_PADDING_PX,
            }}
          />
          <Tooltip labelFormatter={TOOLTIP_FORMATTER} />
          <Bar dataKey="count" fill="steelblue">
            {
              data.map((d, index) =>
              <Cell key={index} fill={ index === highlightBinIndex ? LIGHT_NYU_PURPLE : 'steelblue' }/>)
            }
          </Bar>
        </BarChart>
      </ResponsiveContainer>
    );
  }
}

export default SolutionScoreDistributionPlot;
