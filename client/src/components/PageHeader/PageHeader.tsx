import * as React from 'react';

interface PageHeaderProps {
  title: string;
}

class PageHeader extends React.PureComponent<PageHeaderProps> {
  render() {
    const { title } = this.props;
    return (
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h1 className="h2">{title}</h1>
        {this.props.children ? (
          <div className="btn-toolbar mb-2 mb-md-0">{this.props.children}</div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export { PageHeader };
