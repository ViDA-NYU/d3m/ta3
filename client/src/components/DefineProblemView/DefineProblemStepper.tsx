import * as React from 'react';
import * as Icon from 'react-feather';
import DefineProblemHorizontalFlow from './DefineProblemHorizontalFlow';
import { Steps, NavWorkflowSteps } from './stepper-commons';

class DefineProblemStepper extends React.PureComponent {
  render() {
    const workflowSteps: NavWorkflowSteps[] = [
      {
        toStep: Steps.S1_WHAT_TO_PREDICT,
        labelStep: 'What to predict',
        iconComponent: Icon.Target,
      },
      {
        toStep: Steps.S2_HOW_TO_PREDICT,
        labelStep: 'How to predict',
        iconComponent: Icon.HelpCircle,
      },
      {
        toStep: Steps.S3_ADVANCED_CONFIGURATION,
        labelStep: 'Advanced Configuration',
        iconComponent: Icon.Settings,
      },
    ];
    return (
      <div
        className="card card-attributes mb-3"
        style={{
          boxShadow: '1px 1px 5px grey',
          minWidth: 250,
          padding: 0,
        }}
      >
        <div className="card-body">
          <DefineProblemHorizontalFlow workflow={workflowSteps} />
        </div>
      </div>
    );
  }
}

export default DefineProblemStepper;
