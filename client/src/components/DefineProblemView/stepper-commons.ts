import * as Icon from 'react-feather';

export interface NavWorkflowSteps {
  toStep: Steps;
  labelStep: string;
  iconComponent: React.ComponentType<Icon.Props>;
}

export enum Steps {
  S1_WHAT_TO_PREDICT = 1,
  S2_HOW_TO_PREDICT = 2,
  S3_ADVANCED_CONFIGURATION = 3,
}
