import * as React from 'react';
import * as Icon from 'react-feather';
import * as d3m from 'd3m/dist';
import * as api from '../../data/Api';
import { startCase } from 'lodash';
import { connectPropsToDatastore } from 'src/data/store';
import { NavWorkflowSteps, Steps } from './stepper-commons';
import StepperHeader from './StepperHeader';
import DefineProblemHorizontalFlowContent from './DefineProblemHorizontalFlowContent';
import { ConfigurationType } from 'd3m/dist';

/*
 * Define Problem main navigation through step-by-step layout. It allows navigation between the
 * three required steps to define a problem (define: target, type, and subtype).
 */

interface DefineProblemHorizontalFlowProps {
  target: d3m.InputsTarget;
  workflow: NavWorkflowSteps[];
  defineProblemStatus: boolean;
  setDefineProblemStatus: (defineProblemStatus: boolean) => void;
  problemDefinition: d3m.UserProblemDefinition;
  setUserProblemDefinition: (problemDefinition: d3m.UserProblemDefinition) => void;
  createNewProblem: (problemDefinition: d3m.UserProblemDefinition) => void;
  setDefaultProblemDefinition: () => void;
  selectedConfig: ConfigurationType;
  defaultProblemDefinition: d3m.UserProblemDefinition;
}

interface NavWorkflowState {
  selectedStep: Steps;
}

class DefineProblemHorizontalFlow extends React.Component<DefineProblemHorizontalFlowProps, NavWorkflowState> {
  constructor(props: DefineProblemHorizontalFlowProps) {
    super(props);
    this.state = {
      /**
       * if the define problem status is true that means that the problem was already defined.
       * Then the selectedStep should be updated to 3 (last step).
       */
      selectedStep: this.props.defineProblemStatus ? 3 : 1,
    };
  }

  /***
   * Selected value by the user. This value could be either
   * the target, type of problem or sub-type of problem.
   * It will be updated automatically after user selection.
   */
  selectedValueStep(currentStep: number) {
    const { target, taskType: type } = this.props.problemDefinition;
    let isThereMetrics =
      this.props.defaultProblemDefinition.metrics &&
      Object.keys(this.props.defaultProblemDefinition.metrics!).length > 0
        ? true
        : false;
    switch (currentStep) {
      case Steps.S1_WHAT_TO_PREDICT:
        return !target ? undefined : target;
      case Steps.S2_HOW_TO_PREDICT:
        return (type && target) || this.props.defineProblemStatus ? startCase(type) : undefined;
      case Steps.S3_ADVANCED_CONFIGURATION:
        return type && target && this.props.selectedConfig === ConfigurationType.DEFAULT && isThereMetrics
          ? 'Default'
          : type && target && isThereMetrics
          ? 'Custom'
          : undefined;
      default:
        return undefined;
    }
  }

  doneClickHandler() {
    const { problemDefinition } = this.props;
    this.props.setDefineProblemStatus(true);
    this.props.setUserProblemDefinition(problemDefinition);
    this.props.createNewProblem(problemDefinition);
    api.log(d3m.UserEvent.SETUP_SEARCH_CONFIGURATION, {
      typeSearchConfig: this.props.selectedConfig === '1' ? 'Default' : 'Custom',
      problemDefinition,
    });
  }

  selectButtonsStep() {
    const { selectedStep } = this.state;
    switch (selectedStep) {
      case Steps.S1_WHAT_TO_PREDICT:
        return (
          <div>
            <button
              data-cy="define-problem-stepper:target:next"
              className="btn btn-primary float-right"
              onClick={() => {
                this.setState({ selectedStep: selectedStep + 1 });
                api.log(d3m.UserEvent.SELECT_TARGET);
              }}
              disabled={this.props.problemDefinition.target !== '' ? false : true}
            >
              <Icon.ChevronsRight className="feather" /> Next
            </button>
          </div>
        );
      case Steps.S2_HOW_TO_PREDICT:
        if (this.props.defineProblemStatus) {
          return <></>;
        } else {
          return (
            <div>
              <button
                className="btn btn-primary float-right"
                data-cy="define-problem-stepper:type:next"
                onClick={() => {
                  this.setState({ selectedStep: selectedStep + 1 });
                  this.props.setDefaultProblemDefinition();
                  api.log(d3m.UserEvent.SELECT_PROBLEM_TYPE);
                }}
                disabled={this.props.problemDefinition.taskType ? false : true}
              >
                <Icon.ChevronsRight className="feather" /> Next
              </button>
              <button
                className="btn btn-outline-primary float-right mr-3"
                onClick={() => this.setState({ selectedStep: selectedStep - 1 })}
              >
                <Icon.ChevronsLeft className="feather" /> Back
              </button>
            </div>
          );
        }
      case Steps.S3_ADVANCED_CONFIGURATION:
        if (this.props.defineProblemStatus) {
          return <></>;
        } else {
          return (
            <div>
              <button
                className="btn btn-primary float-right"
                data-cy="define-problem-stepper:config:done"
                onClick={() => {
                  this.doneClickHandler();
                }}
                disabled={this.props.problemDefinition.taskType ? false : true}
              >
                <Icon.ChevronsRight className="feather" />
                Done
              </button>
              <button
                className="btn btn-outline-primary float-right mr-3"
                onClick={() => this.setState({ selectedStep: selectedStep - 1 })}
              >
                <Icon.ChevronsLeft className="feather" /> Back
              </button>
            </div>
          );
        }
      default:
        return <div>Unknown Tab</div>;
    }
  }

  render() {
    const { workflow, setDefineProblemStatus } = this.props;
    return (
      <div>
        <div className="d-flex">
          <div className="w-100">
            <nav className="d-flex" style={{ marginLeft: 0, textAlign: 'center', width: '100%' }}>
              <ul className="nav row" style={{ marginLeft: 0, textAlign: 'center', width: '100%' }}>
                {workflow.map((d, index) => {
                  const selectedStep = this.state.selectedStep;
                  const selectedValueStep = this.selectedValueStep(d.toStep);
                  return (
                    <StepperHeader
                      thisStep={d}
                      inactive={d.toStep <= selectedStep}
                      key={index}
                      showBorder={d.toStep === selectedStep && !this.props.defineProblemStatus}
                      selectedValue={selectedValueStep}
                    />
                  );
                })}
              </ul>
            </nav>
            {this.props.defineProblemStatus ? (
              <></>
            ) : (
              <div className="card mt-2 mb-2" style={{ marginBottom: 20 }}>
                <DefineProblemHorizontalFlowContent step={this.state.selectedStep} />
              </div>
            )}
            {this.selectButtonsStep()}
          </div>
          <div className="flex-shrink-1 align-self-center">
            {this.props.defineProblemStatus && (
              <div className="">
                <button
                  className="btn btn-sm btn-outline-primary float-right"
                  onClick={() => setDefineProblemStatus(false)}
                >
                  <Icon.Edit className="feather" /> Edit
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<DefineProblemHorizontalFlowProps>((store, ownProps) => ({
  defineProblemStatus: store.defineProblemStatus,
  setDefineProblemStatus: store.setDefineProblemStatus,
  problemDefinition: store.problemDefinition,
  setUserProblemDefinition: store.setUserProblemDefinition,
  createNewProblem: store.createNewProblem,
  setDefaultProblemDefinition: store.setDefaultProblemDefinition,
  selectedConfig: store.selectedConfig,
  defaultProblemDefinition: store.defaultProblemDefinition,
}))(DefineProblemHorizontalFlow);
