import * as React from 'react';
import * as d3m from 'd3m/dist';
import './define-problem-view.css';
import { startCase } from 'lodash';
import { connectPropsToDatastore } from 'src/data/store';
import ConfigureSearchView from './ConfigureSearchView';
import { Steps } from './stepper-commons';
import { TaskKeyword } from 'd3m/dist';

/**
 * Content for each Define Problem Sub Step (target, type, and subtype). For each sub step, all the
 * possible target, type and subtype available to define a problem are displayed using radio buttons.
 * Radio buttons allow the user to choose only one of a predefined set of targets, types or subtypes.
 */

interface DefineProblemHorizontalFlowContentProps {
  step: number;
  columns: d3m.Feature[];
  problemDefinition: d3m.UserProblemDefinition;
  setUserProblemDefinition: (problemDefinition: d3m.UserProblemDefinition) => void;
}

function getFeatureID(c: d3m.Feature): string {
  return `${c.resID}:${c.colName}`;
}

class DefineProblemHorizontalFlowContent extends React.Component<DefineProblemHorizontalFlowContentProps> {
  constructor(props: DefineProblemHorizontalFlowContentProps) {
    super(props);
    // TODO: The main data resource is always named "learningData". We might want to
    // make it always show up first, before other remaining secondary resources.
    // this.state = {
    // };
  }

  render() {
    const {
      columns,
      problemDefinition,
      setUserProblemDefinition,
    } = this.props;

    const { target, taskType, taskSubType } = problemDefinition;

    switch (this.props.step) {
      case Steps.S1_WHAT_TO_PREDICT:
        return (
          <div>
            <p className={'style-paragraph small'}>
              Which attribute would you like your model to predict?
          </p>
            <form>
              <div className="sub-steps-define-problem">
                {
                  columns.map((column, i) => {
                    const featureID = getFeatureID(column);
                    return (
                      <label key={i} className="col-lg-4" style={{ padding: 0 }}>
                        <input
                          data-cy={'radio:' + column.colName}
                          className="mr-md-1"
                          type="radio"
                          value={featureID}
                          onChange={e => {
                            setUserProblemDefinition({
                              ...problemDefinition,
                              target: e.target.value,
                            });
                          }}
                          checked={target === featureID ? true : false}
                        />
                        {`${column.colName}`}
                      </label>
                    );
                  })
                }
              </div>
            </form>
          </div>
        );
      case Steps.S2_HOW_TO_PREDICT:
        return (
          <div className="define-problem">
            {/* Defining problem type */}
            <p className={'style-paragraph small'}>
              Among the problem <b>types</b> bellow, which one
              best characterizes your problem?
            </p>
            <form>
              <div className="sub-steps-define-problem">
                {
                  d3m.taskTypes
                    .map(k =>
                    <label className="col-md-4" key={k} style={{ padding: 0 }}>
                      <input type="radio" className="mr-md-1"
                        value={k}
                        onChange={e => {
                          setUserProblemDefinition({
                            ...problemDefinition,
                            taskType: e.target.value as TaskKeyword,
                          });
                        }}
                        checked={taskType === k ? true : false}
                      />
                      {startCase(k)}
                    </label>)}
              </div>
            </form>
            {
              /* Defining problem subtype */
              taskType && d3m.problemType[taskType].length > 0
                ?
                <>
                  <p className={'style-paragraph small'}>
                    Among the problem <b>sub-types</b> bellow, which one
                    best characterizes your problem?
                  </p>
                  <form>
                    <div className="sub-steps-define-problem">
                      {
                        d3m.taskSubtypes
                          .filter(subtype => d3m.problemType[taskType].includes(subtype))
                          .map(k =>
                            <label className="col-md-4" key={k} style={{ padding: 0 }}>
                              <input type="radio" className="mr-md-1"
                                value={k}
                                onChange={e => {
                                  setUserProblemDefinition({
                                    ...problemDefinition,
                                    taskSubType: e.target.value as TaskKeyword,
                                  });
                                }}
                                checked={taskSubType === k ? true : false}
                              />
                              {startCase(k)}
                            </label>,
                          )
                      }
                    </div>
                  </form>
                </>
                :
                <>
                </>
            }
          </div>
        );
      case Steps.S3_ADVANCED_CONFIGURATION:
        return (<ConfigureSearchView />);
      default:
        return <div>Unknown Tab</div>;
    }
  }
}

export default connectPropsToDatastore<DefineProblemHorizontalFlowContentProps>(store => ({
  columns: store.datasetFeatures,
  problemDefinition: store.problemDefinition,
  setUserProblemDefinition: store.setUserProblemDefinition,
}))(DefineProblemHorizontalFlowContent);
