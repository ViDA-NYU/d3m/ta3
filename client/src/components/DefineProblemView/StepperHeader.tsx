import * as React from 'react';
import { NavWorkflowSteps } from './stepper-commons';

interface StepperHeaderProps {
  thisStep: NavWorkflowSteps;
  selectedValue?: string;
  showBorder: boolean;
  inactive: boolean;
}

class StepperHeader extends React.Component<StepperHeaderProps> {
  render() {
    const { thisStep, showBorder, selectedValue, inactive } = this.props;
    const { iconComponent: IconComponent } = this.props.thisStep;
    return (
      <div
        className="col-md-4 align-items-center"
        style={showBorder ? { borderBottom: '3px solid #63508b', height: '81px' } : { height: '81px' }}
      >
        <li className="nav-item">
          <a
            className={inactive ? 'nav-link-text' : 'active'}
            style={inactive ? { color: '#63508b' } : { color: 'silver' }}
          >
            <IconComponent className="feather" style={{width: 28, height: 28, strokeWidth: 2}} />
            <br />
            <span>{thisStep.labelStep}</span>
            {selectedValue && (
              <p>
                <small>
                  <mark
                    style={inactive ? { color: 'black' } : { color: 'silver' }}
                    className="badge-pill badge-secondary"
                  >
                    {selectedValue}
                  </mark>
                </small>
              </p>
            )}
          </a>
        </li>
      </div>
    );
  }
}

export default StepperHeader;
