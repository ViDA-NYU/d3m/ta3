import * as React from 'react';
import ReactMarkdown from 'react-markdown';
import styled from 'styled-components';
import './markdowncard.css';

const ScrollDiv = styled.div`
  max-height: 200px;
  overflow: auto;
  background-color: #fefefe;
  padding: 5px 10px;
  font-size: 0.75rem;
  p:last-child {
    margin-bottom: 0;
  }
`;

interface MarkdownCardProps {
  source?: string;
  className?: string;
}

class MarkdownCard extends React.PureComponent<MarkdownCardProps> {
  render() {
    const cardClassName = this.props.className ? ('card ' + this.props.className) : 'card';
    return (
      <ScrollDiv className={cardClassName}>
        { this.props.source && <ReactMarkdown className="code-non-language" source={this.props.source} />}
      </ScrollDiv>
    );
  }
}

export { MarkdownCard };
