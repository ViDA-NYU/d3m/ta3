import * as React from 'react';
import * as Icon from 'react-feather';

interface HelpTextProps {
  text?: string;
  className?: string;
}

class HelpText extends React.PureComponent<HelpTextProps> {
  render() {
    const className = this.props.className ?
      `d-flex ${this.props.className}` : 'd-flex';
    return (
      <div className={className}>
        <div className="mb-auto mt-auto text-primary" >
          <Icon.Info
            strokeWidth="2"
            className="mr-2"
            style={{width: '16px', height: '16px'}}
          />
        </div>
        <span
          className="flex-grow-1 text-merriweather small pl-2 mb-auto mt-auto"
          style={{borderLeft: '1px solid #63508b'}}
        >
          {this.props.children ? this.props.children : this.props.text}
        </span>
      </div>
    );
  }
}

export { HelpText };
