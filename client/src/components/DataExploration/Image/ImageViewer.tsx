import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import './ImageViewer.css';

interface ImageViewerProps {
  data: d3m.ResourceFileData;
  getURLToFile: (filePath: string) => string;
  predBoundingBox?: string[];
  truthBoundingBox?: string[];
}

class ImageViewer extends React.PureComponent<ImageViewerProps, {}> {

  drawBounding (boundingBoxes: string[], color: string) {
    return boundingBoxes
           .map((box, index) => <polygon key={'boxes_' + index} points={box} fill={'none'} stroke={color} />);
  }

  render() {
    const imageData = this.props.data;
    const imageSize = 200;
    return (
      <div className="image-viewer">
        <div className="mb-1">
          <b>{ imageData.name }</b>
        </div>
        <svg x={0} y={0} width={imageSize} height={imageSize} viewBox={`0 0 ${imageSize} ${imageSize}`}>
          <image href={this.props.getURLToFile(imageData.src)} x={0} y={0}  width={imageSize} height={imageSize}
          preserveAspectRatio={'none'}
          />
          {
            this.props.predBoundingBox
            ?
            this.drawBounding(this.props.predBoundingBox, 'blue')
            :
            <></>
          }
          {
            this.props.truthBoundingBox
            ?
            this.drawBounding(this.props.truthBoundingBox, 'red')
            :
            <></>
          }
        </svg>
      </div>
    );
  }
}

export default connectPropsToDatastore<ImageViewerProps>(store => ({
  getURLToFile: store.getURLToFile,
}))(ImageViewer);
