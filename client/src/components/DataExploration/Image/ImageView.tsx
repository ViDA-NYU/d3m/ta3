import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import ImageViewer from './ImageViewer';
import DataSelector from '../DataSelector';
import { Row, Col } from 'react-bootstrap';

const DEFAULT_IMAGE_COUNT = 4;

export interface ImageData {
  name: string;
  src: string;
}

interface ImageViewProps {
  resource: d3m.DataResource;
  data: d3m.ResourceFileData[];
}

interface ImageViewState {
  selected: string[];
}

class ImageView extends React.Component<ImageViewProps, ImageViewState> {
  constructor(props: ImageViewProps) {
    super(props);
    this.state = {
      selected: this.props.data.slice(0, DEFAULT_IMAGE_COUNT).map(d => d.name),
    };
  }

  onSelectedChange = (selected: string[]) => {
    this.setState({ selected });
  }

  render() {
    const selected = new Set(this.state.selected);
    const data = this.props.data.filter(d => selected.has(d.name));
    return (
      <>
        <div className="mb-3">
          <DataSelector
            data={this.props.data}
            selected={this.state.selected}
            onSelectedChange={this.onSelectedChange}
          />
        </div>
        <Row>
          {
            data.map((d, index) => (
              <Col md={3} key={d.name}>
                <ImageViewer data={d}/>
              </Col>
            ))
          }
        </Row>
      </>
    );
  }
}

export default connectPropsToDatastore<ImageViewProps>((store, ownProps) => ({
  data: store.dataForResource(ownProps.resource!) as d3m.ResourceFileData[],
}))(ImageView);
