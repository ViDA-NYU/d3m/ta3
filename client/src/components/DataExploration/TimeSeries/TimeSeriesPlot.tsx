
import * as React from 'react';
import {
  Area,
  AreaChart,
  CartesianGrid,
  ReferenceLine,
  ResponsiveContainer,
  Tooltip,
  YAxis,
} from 'recharts';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import './TimeSeriesPlot.css';
import { parse } from 'papaparse';

export interface TimeSeriesData {
  name: string;
  minValue: number;
  maxValue: number;
  seriesKey: string;
  dataKey: string;
  data: Array<{
    [anyKey: string]: number;
  } | {
      [anyKey: number]: number;
  }>;
}

interface TimeSeriesPlotProps {
  data: d3m.ResourceFileData;
  getURLToFile: (filePath: string) => string;
}

interface TimeSeriesPlotState {
  series: TimeSeriesData;
}

class TimeSeriesPlot extends React.PureComponent<TimeSeriesPlotProps, TimeSeriesPlotState> {
  constructor(props: TimeSeriesPlotProps) {
    super(props);
    this.state = {
      series: {
        name: '',
        minValue: 0,
        maxValue: 1,
        seriesKey: '' + '-',
        dataKey: '',
        data: [],
      },
    };
    this.getData = this.getData.bind(this);
  }

  componentWillMount() {
    const rawSeries = this.props.data;
    this.getCsvData(this.props.getURLToFile(rawSeries.src), rawSeries.name);
  }

  yAxisLabel(axisName: string, cxT: string): JSX.Element {
    const cx = cxT;
    const cy = 40;
    return (
      <svg>
        <text x={cx} y={cy} transform="rotate(-90deg)"
          textAnchor="middle" fontFamily="sans-serif" fontSize="10">
          {axisName}
        </text>
      </svg>
    );
  }
  getData(result: TimeSeriesData) {
    this.setState({series: result});
  }

  fetchCsv(src: string) {
    return fetch(src).then(response => {
        if (response.status !== 200 || !response.ok) {
          throw new Error(`server returned ${response.status}${response.ok ? ' ok' : ''}`);
        }
        if (response.body !== null) {
          try {
            let reader = response.body.getReader();
            let decoder = new TextDecoder('utf-8');
            return reader.read().then(result => {
                return decoder.decode(result.value);
            });
          } catch ( e ) {
            console.log('Error');
          }
        }
        throw new Error(`server returned ${response.status}${response.ok ? ' ok' : ''}`);
    });
}

  async getCsvData(src: string, name: string) {
    let csvData = await this.fetchCsv(src);
    parse(csvData, {
      complete: (result) => {
        var dataSerie = [];
        var columnSize = result.data[0].length;
        for (var i = 1; i < result.data.length; i++) {
          var serieOne: {
            [anyKey: string]: number;
          } | {
              [anyKey: number]: number;
          } = {};
          for (var j = 0; j < columnSize; j++) {
            serieOne[result.data[0][j]] = j === columnSize - 1 ? parseFloat(result.data[i][j]).toFixed(2) :
             result.data[i][j];
          }
          dataSerie.push(serieOne);
        }
        var seriesKey = result.data[0][0];
        var dataKey = result.data[0][1];
        var minValue = dataSerie.reduce((min, b) => Math.min(min, b[dataKey]), dataSerie[0][dataKey]);
        var maxValue = dataSerie.reduce((max, b) => Math.max(max, b[dataKey]), dataSerie[0][dataKey]);
        var serie: TimeSeriesData = {
                    name: name,
                    minValue: minValue,
                    maxValue: maxValue,
                    seriesKey: seriesKey,
                    dataKey: dataKey,
                    data: dataSerie,
                  };
        this.getData(serie);
      },
    });
  }

  render() {
    const series = this.state.series;
    return (
      <div className="time-series-plot">
        <ResponsiveContainer width="100%" height={100}>
        <ResponsiveContainer width="100%" height={100}>
          <AreaChart
            data={series.data}
          >
            <CartesianGrid strokeDasharray="3 3"/>
            <YAxis
              domain={[series.minValue, series.maxValue]}
              label={{ value: series.name, angle: -90, position: 'insideLeft' }}
            />
            <Tooltip/>
            <Area
              type="monotone"
              dataKey={series.dataKey}
            />
            <ReferenceLine y={series.minValue} stroke="#000"/>
          </AreaChart>
        </ResponsiveContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}

export default connectPropsToDatastore<TimeSeriesPlotProps>(store => ({
  getURLToFile: store.getURLToFile,
}))(TimeSeriesPlot);
