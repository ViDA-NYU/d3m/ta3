import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';

interface TextPlotProps {
  data: d3m.ResourceFileData;
}

class TextPlot extends React.PureComponent<TextPlotProps, {}> {
  render() {
    const textData = this.props.data;
    return (
      <div>
        <hr/>
        <div className="mb-2">
          <b>{ textData.name }</b>
        </div>
        <div title={textData.content}>
          { textData.content && `${textData.content.slice(0, 1000)}${textData.content.length > 1000 ? '...' : ''}` }
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<TextPlotProps>(store => ({
}))(TextPlot);
