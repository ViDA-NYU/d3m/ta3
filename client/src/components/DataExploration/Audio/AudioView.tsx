import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import AudioViewer from './AudioViewer';
import DataSelector from '../DataSelector';
import { Row, Col } from 'react-bootstrap';
import './AudioViewer.css';

const DEFAULT_AUDIO_COUNT = 3;

interface AudioViewProps {
  resource: d3m.DataResource;
  data: d3m.ResourceFileData[];
}

interface AudioViewState {
  selected: string[];
}

class AudioView extends React.Component<AudioViewProps, AudioViewState> {
  constructor(props: AudioViewProps) {
    super(props);
    this.state = {
      selected: this.props.data.slice(0, DEFAULT_AUDIO_COUNT).map(d => d.name),
    };
  }

  onSelectedChange = (selected: string[]) => {
    this.setState({ selected });
  }

  render() {
    const selected = new Set(this.state.selected);
    const data = this.props.data.filter(d => selected.has(d.name));
    return (
      <>
        <div className="mb-3">
          <DataSelector
            data={this.props.data}
            selected={this.state.selected}
            onSelectedChange={this.onSelectedChange}
          />
        </div>
        <Row>
          {
            data.map(d => (
              <Col md={12} key={d.name}>
                <AudioViewer data={d}/>
              </Col>
            ))
          }
        </Row>
      </>
    );
  }
}

export default connectPropsToDatastore<AudioViewProps>((store, ownProps) => ({
  data: store.dataForResource(ownProps.resource!) as d3m.ResourceFileData[],
}))(AudioView);
