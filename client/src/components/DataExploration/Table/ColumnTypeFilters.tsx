import * as React from 'react';
import * as _ from 'lodash';
import { capitalize } from 'src/data/utils';

interface ColumnTypeFiltersProps {
  columnTypes: string[];
  columnTypeFilter: string[];
  onColumnTypeFilterChange: (value: string[]) => void;
}

export default class ColumnTypeFilters extends React.PureComponent<ColumnTypeFiltersProps> {

  onColumnTypeFilterChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const checked = evt.target.checked;
    this.props.onColumnTypeFilterChange(checked ?
      _.uniq(this.props.columnTypeFilter.concat([evt.target.value]).sort()) :
      _.pull(this.props.columnTypeFilter, evt.target.value),
    );
  }

  /**
   * Renders a checkbox.
   */
  renderCheckbox(name: string, value: string, text: string,
                 onChange: (evt: React.ChangeEvent<HTMLInputElement>) => void,
                 checked?: boolean): JSX.Element {
    return (
      <label className="form-check-inline" key={value}>
        <input type="checkbox" className="form-check-input"
          name={name}
          value={value}
          onChange={onChange}
          checked={checked}
        />
        {text}
    </label>
    );
  }

  render() {
    return (
      <div className="mt-1">
        <span className="font-weight-bold mr-2">Display only: </span>
        {
          this.props.columnTypes.map(type => this.renderCheckbox(
            'typeFilter', type, capitalize(type), this.onColumnTypeFilterChange,
            this.props.columnTypeFilter.indexOf(type) !== -1))
        }
      </div>
    );
  }
}
