#!/bin/bash
if [ $# -ne 2 ]
then
    SCRIPT_NAME=`basename "$0"`
    echo "Usage:"
    echo "    $SCRIPT_NAME <old-version-number> <new-version-number>"
    echo "Example:"
    echo "    $SCRIPT_NAME 0.0.1 0.0.2-SNAPSHOT"
    exit 1
fi
OLD_VERSION=$1
NEW_VERSION=$2
echo "Changing Visus version from $OLD_VERSION to $NEW_VERSION..."

OS=`uname`
if [[ "$OS" == 'Linux' ]]; then
  # "version": "0.0.1",
  sed -i "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" client/package.json
  sed -i "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" server/package.json
  sed -i "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" d3m/package.json
  sed -i "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" integration-tests/package.json
elif [[ "$OS" == 'Darwin' ]]; then
  sed -i ".bkp" "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" client/package.json
  sed -i ".bkp" "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" server/package.json
  sed -i ".bkp" "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" d3m/package.json
  sed -i ".bkp" "s/version\": \"$OLD_VERSION\",/version\": \"$NEW_VERSION\",/" integration-tests/package.json
  rm client/package.json.bkp
  rm server/package.json.bkp
  rm d3m/package.json.bkp
  rm integration-tests/package.json.bkp
fi

echo "done."