# Visus Integration Tests

This directory contains integration tests for Visus, which are
implemented using the [Cypress](https://www.cypress.io/) testing toolkit.

#### Running Locally

In order to develop new tests, you might want to install
and run Cypress locally.

To install Cypress locally, enter this directory (`/integration-tests`)
 and install npm packages by running:
```
cd integration-tests
npm install
```

To execute Cypress, you can use the command `npx cypress open`.
But before running Cypress integration tests, you will need to
first run Visus (e.g. at `http://localhost:5000`) and only then
you will be able to run tests successfully.

In order to inform Cypress the address where Visus is running,
you will need to configure the base URL using the environment
variable `CYPRESS_baseUrl`. For example:
```
CYPRESS_baseUrl=http://localhost:5000 npx cypress open
```

If you just want to run the existing tests, use:
```
CYPRESS_baseUrl=http://localhost:5000 npx cypress run
```

The code of integration tests are generally stored under the
`cypress/integration` folder.

For more info on how to run or develop tests, and the
directory structure, refer to the Cypress documentation
at https://docs.cypress.io/.

#### Running Using Docker

We also provide a docker image to run the tests. To build it, use:

```
docker build -t visus-integration-tests .
```

To run the tests using the image:
```
docker run -e "CYPRESS_baseUrl=http://dockercompose_ta3_1" -i -t visus-integration-tests
```
where `dockercompose_ta3_1` is the domain name of the server/container
running Visus (you can also use the IP address instead of domain name).

You may also want to include following arguments in the docker run command:
- `-v $PWD/results/:/tests/cypress/results/` - to mount an external directory
  as a docker volume and make the folder where the test results are stored
  available outside the container.
- `--network dockercompose_default` - To allow the test container to join
  another Docker network where Visus might be running.

For example:
```
docker run -v $PWD/results/:/tests/cypress/results/ -e "CYPRESS_baseUrl=http://dockercompose_ta3_1" --network dockercompose_default -i -t visus-integration-tests
```

*Note*: If you are using Docker and you want to find the container IP address, you can use using `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockercompose_ta3_1`, where `dockercompose_ta3_1` is the container name.
